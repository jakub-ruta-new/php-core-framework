<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\router;

/**
 * Description of WebRouter
 *
 * @author jakub
 */
class WebRouter {

    /**
     *
     * @var \JR\CORE\router\WebRouter
     */
    protected $router;

    public function __construct(\JR\CORE\router\WebRouter $router) {
        $this->router = $router;
        $this->register();
    }

    private function register() {
        $this->router->addRoute(["dashBoard", "", "dashboard"],
                \JR\CORE\tests\controlers\main\DashBoard::class);
    }

}
