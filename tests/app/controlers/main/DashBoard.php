<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\tests\controlers\main;

/**
 * Description of DashBoard
 *
 * @author jakub
 */
class DashBoard extends \JR\CORE\controler\WebControler
{

    public $view = "app.main.dashboard";

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = -1;

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = null;

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute()
    {
        bdump($this->request->getBasePath(), "basePath");
        $this->addCounter('Admin', 'Annoucements', 2, 'primary');
    }

}
