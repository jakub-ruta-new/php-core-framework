<?php

namespace JR\CORE\midleware\options;

use JR\CORE\midleware\Midleware;

class Options extends Midleware
{
    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "options";

    public function getFeatureFlags()
    {
        return $this->db->where('is_feature_flag', 1)
            ->get($this->tableName);
    }

    public function getOptions()
    {
        return $this->db->where('is_feature_flag', 0)
            ->get($this->tableName);
    }

    public function getOptionsByCategory($cat)
    {
        return $this->db->where('is_feature_flag', 0)
            ->where('category', $cat)
            ->where('is_global', 0)
            ->get($this->tableName);
    }

    public function getAll($limit = 1000, $offset = 0)
    {
        $data = $this->db->orderBy('category')->orderBy('\'key\'')->get($this->tableName, array($offset, $limit));
        $count = $this->db->getValue($this->tableName, "COUNT(*)");
        return array($data, $count);
    }


}