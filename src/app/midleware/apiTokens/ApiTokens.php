<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midlewares\apiTokens;

/**
 * Description of ApiTokens
 *
 * @author jakub
 */
class ApiTokens extends \JR\CORE\midleware\Midleware {

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "api_tokens";
    protected $allowedTypes = ['get', 'set', 'send', 'setup'];
    protected $allowedApps = ['ldap'];
    protected $url;

    public function __construct(\MysqliDb $db) {
        parent::__construct($db);
        $this->url['ldap'] = $_ENV['LDAP_URL'] . "api/v1/ldap/app/token";
    }

    public function renew($type, $app, \Curl\Curl $curl) {
        if (!in_array($type, $this->allowedTypes) || !in_array($app, $this->allowedApps)) {
            throw new WrongRenewParams();
        }
        if ($type == 'setup') {
            $this->renewSetup($app, $curl);
        } else {
            $this->renewNormal($type, $app, $curl);
        }
    }

    private function renewSetup($app, \Curl\Curl $curl) {
        $token = $this->getToken('recovery', $app);
        $data = [
            'app_code' => $_ENV['APP_CODE'],
            'token' => $token['token'],
            'type' => 'setup',
        ];
        $response = $curl->post($this->url[$app], $data);
        if (isset($response->msg_error)) {
            throw new \Exception(json_encode($data));
        }
        if (!isset($response->data->tokens->token)) {
            throw new \Exception("Missing token");
        }
        $this->addToken($app, $response->data->tokens->token, 'setup');
    }

    public function getToken($type, $app) {
        $token = $this->db->where('app_code', $app)
                ->where('type', $type)
                ->getOne($this->tableName);
        if (!$token) {
            throw new \Exception("Missing token in db");
        }
        return $token;
    }

    private function renewNormal($type, $app, \Curl\Curl $curl) {
        $token = $this->getToken('setup', $app);
        $data = [
            'app_code' => $_ENV['APP_CODE'],
            'token' => $token['token'],
            'type' => $type,
        ];
        $response = $curl->post($this->url[$app], $data);
        if (isset($response->msg_error)) {
            throw new \Exception(json_encode($data));
        }
        if (!isset($response->data->tokens->token)) {
            throw new \Exception("Missing token");
        }
        $this->addToken($app, $response->data->tokens->token, $type);
    }

    public function addToken($app, $tokens, $type, $auto_renew = 1) {
        $data = [
            'app_code' => $app,
            'token' => $tokens->token,
            'expire' => $tokens->expire,
            'auto_renew' => $auto_renew,
            'type' => $type,
        ];
        parent::insertOrUpdate($data);
    }

    public function getForRenew() {
        return $this->db->where("expire", date("Y-m-d H:i:s", time() + 60 * 60), "<")
                        ->get($this->tableName);
    }

    public function checkToken($app, $token, $type = 'get') {
        $token_data = $this->db->where("app_code", $app)
                ->where('token', $token)
                ->where('type', $type)
                ->where('expire', date("Y-m-d H:i:s"), '>')
                ->getOne($this->tableName);
        if (!isset($token_data['id'])) {
            throw new WrongTokenType();
        }
    }

}
