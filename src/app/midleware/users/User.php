<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\users;

/**
 * Description of User
 *
 * @author jakub
 */
class User extends \JR\CORE\midleware\Midleware
{

    /**
     *
     * @var \JR\CORE\midleware\rights\Rights
     */
    public $rights;
    /**
     *
     * @var \JR\CORE\midleware\users\Password
     */
    public $password;
    /**
     *
     * @var \JR\CORE\midleware\sessions\Session
     */
    public $userSession;
    /**
     *
     * @var \JR\CORE\midleware\users\PersonalData
     */
    public $personalInfo;
    /**
     *
     * @var \JR\CORE\midleware\departments\Departments
     */
    public $departments;
    /**
     *
     * @var array representation in string of int roles
     */
    public $admin_levels = array(
        "-10" => "system",
        "-1" => "system guest",
        "0" => "guest",
        "1" => "member lvl 0",
        "2" => "member lvl 1",
        "3" => "member lvl 2",
        "4" => "admin",
        "5" => "head",
        "6" => "supervisor",
        "7" => "director",
        "8" => "CEO",
        "9" => "developer",
        "10" => "sysadmin"
    );
    public $currentSessionId = null;
    protected $primaryKey = "internal_id";
    protected $vissible = null;
    protected $editable = array("nickname", "mail", "locale", "avatar");
    protected $tableName = "users";
    protected $loginName = "Test";
    protected $nickName = "Guest";
    protected $role = "guest";
    protected $InternalId = "0";
    protected $cache = null;
    /**
     *
     * @var \JR\CORE\midleware\tokens\Tokens
     */
    protected $tokens = null;

    public function __construct($InternalId, \MysqliDb $db)
    {
        parent::__construct($db);
        $this->InternalId = $InternalId;
        $this->cache = $this->getOne($InternalId);
        if ($this->cache == null || !isset($this->cache['login_name'])) {
            throw new UserNotExist();
        }
        $this->loginName = $this->cache['login_name'];
        $this->nickName = $this->cache['nickname'];
        $this->role = $this->cache['main_role'];
        $this->mainRole = $this->cache['main_role'];
        $this->portalId = $this->cache['portal_id'];
        $this->rights = new \JR\CORE\midleware\rights\Rights($InternalId, $db);
        $this->userSession = new \JR\CORE\midleware\sessions\Session($db, $InternalId);
        $this->password = new Password($db, $InternalId);
        if ($this->cache['origin'] == 'local') {
            $this->personalInfo = new \JR\CORE\midleware\users\PersonalData($db, $this->InternalId);
        }
        $this->departments = new \JR\CORE\midleware\departments\Departments($db, $InternalId);
        \Tracy\Debugger::getBar()->addPanel(
            new \JR\CORE\bridges\tracy\UserPanel($this));
    }

    public static function registerUserViaCLI($name, $email, $pass, $dev, \MysqliDb $db)
    {
        $db->insert("users", ["login_name" => $name,
            "mail" => $email, 'nickname' => ucfirst($name),
            'main_role' => $dev ? "10" : "0",
            'avatar' => '',
        ]);
        $err = $db->getLastError();
        if ($err) {
            if (str_starts_with($err, "Duplicate entry")) {
                throw new \JR\CORE\midleware\DuplicateRowException($err);
            }
            throw new \Exception($db->getLastError());
        }
        $id = $db->getInsertId();
        $password = new Password($db, $id);
        $password->change($pass);
        $rights = new \JR\CORE\midleware\rights\Rights($id, $db);
        $rights->repairRights();
        if ($dev) {
            $u = array();
            for ($i = 1; $i < 10; $i++) {
                $u[] = array('right_id' => $i);
            }
            $rights->setRights($u, []);
        }
        $deps = new \JR\CORE\midleware\departments\Departments($db, $id);
        $deps->addDepartment(-1, $dev ? "10" : "0");
    }

    public static function getUserByLoginName($login_name, \MysqliDb $db)
    {
        $id = $db->where("login_name", $login_name)
            ->getValue("users", "internal_id");
        if ($id > 9) {
            return new User($id, $db);
        }
        throw new UserNotExist();
    }

    public static function getUserByPortalId($id, $db)
    {
        $id = $db->where("portal_id", $id)
            ->getValue("users", "internal_id");
        if ($id > 9) {
            return new User($id, $db);
        }
        throw new UserNotExist();
    }

    public static function getUserByGoogleId($id, $db)
    {
        $id = $db->where("google_id", $id)
            ->getValue("users", "internal_id");
        if ($id > 9) {
            return new User($id, $db);
        }
        throw new UserNotExist();
    }

    public static function getPortalIdByInternalId($id, $db)
    {
        $id = $db->where("internal_id", $id)
            ->getValue("users", "portal_id");
        if ($id > 9) {
            return $id;
        }
        throw new UserNotExist();
    }

    public function getLoginName()
    {
        return $this->loginName;
    }

    public function getNickName()
    {
        return $this->nickName;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        return parent::update($this->InternalId, ['main_role' => $role]);
    }

    public function setRoleSession($role)
    {
        $this->role = $role;
    }

    public function getPortalId()
    {
        return $this->portalId;
    }

    public function getAvatar()
    {
        return $this->cache['avatar'];
    }

    public function getMail()
    {
        return $this->cache['mail'];
    }

    public function isLoggedIn()
    {
        return $this->InternalId < 10 ? false : true;
    }

    public function setPortalId($id)
    {
        return parent::update($this->InternalId, ['portal_id' => $id]);
    }

    public function setOrigin($type)
    {
        return parent::update($this->InternalId, ['origin' => $type]);
    }

    public function getMainRole()
    {
        return $this->mainRole;
    }

    /**
     * Get user data from cache
     * @return array
     */
    public function get()
    {
        return $this->cache;
    }

    /**
     * Function for get users in departements by department id
     * @param array of ints $dep_ids array of departements id
     * @return array of users
     */
    public function getUserInDepartements($dep_ids = array())
    {
        if (count($dep_ids) > 0) {
            return $this->db->where('d.dep_id', $dep_ids, 'IN')
                ->orWhere('u.internal_id', $this->InternalId)
                ->join('users u', 'd.user_id=u.internal_id')
                ->groupBy('u.internal_id')
                ->get('users_departments d');
        } else {
            return array();
        }
    }

    public function getTokens(): \JR\CORE\midleware\tokens\Tokens
    {
        if (isset($this->tokens))
            return $this->tokens;
        return $this->tokens = new \JR\CORE\midleware\tokens\Tokens($this->db, $this->InternalId);
    }

    public function updateUser($data)
    {
        return parent::update($this->InternalId,
            array_intersect_key($data, array_flip($this->editable)));
    }

    public function disable()
    {
        return parent::update($this->InternalId, ['disabled' => 1]);
    }

    public function enable()
    {
        return parent::update($this->InternalId, ['disabled' => 0]);
    }

    public function ban()
    {
        return parent::update($this->InternalId, ['banned' => 1]);
    }

    public function unBan()
    {
        return parent::update($this->InternalId, ['banned' => 0]);
    }

    public function removeAvatar()
    {
        return parent::update($this->InternalId, ['avatar' => '']);
    }

    /**
     * return user prefered Lang
     */
    public function getLang()
    {
        return $this->cache['locale'];
    }

    public function setRoleAuto()
    {
        $this->setRole($this->departments->getMyMaxRole());
    }

    public function checkLoginState()
    {
        if ($this->cache['banned'] == 1) {
            throw new AccountBannedException();
        }
        if ($this->cache['disabled'] == 1) {
            throw new AccountDissabledException();
        }
    }

    public function registerGoogle($id)
    {
        return parent::update($this->getInternalId(), ['google_id' => $id]);
    }

    public function getInternalId()
    {
        return $this->InternalId;
    }

    public function get2FASettings()
    {
        return $this->cache['2fa_settings'];
    }

    public function set2FASettings($type)
    {
        $this->db->where('internal_id', $this->InternalId)->update($this->tableName, ['2fa_settings' => $type]);
    }


}
