<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\rights;

/**
 * Description of Rights
 *
 * @author jakub
 */
class Rights
{

    protected $cache = array();
    protected $rights = array();
    protected $userInternalId;

    /**
     *
     * @var \JR\CORE\midleware\rights\RightsGroups
     */
    protected $rightsGroups = null;

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    public function __construct($userInternalId, \MysqliDb $db)
    {
        $this->userInternalId = $userInternalId;
        $this->db = $db;
        $this->load();
    }

    protected function load()
    {
        $data = $this->db->join("users_rights r", "l.right_id=r.right_id", "LEFT")
            ->where("r.user_internal_id", $this->userInternalId, "=", "OR")
            ->where("r.user_internal_id", null, "IS", "OR")
            ->get("rights l");
        foreach ($data as $key => $value) {
            $this->rights[$value['right_name']][$value['right_action']] = $value['right_value'];
            $this->cache[$value['right_name']][$value['right_action']] = $value;
        }
    }

    public function getRightsGroups(): \JR\CORE\midleware\rights\RightsGroups
    {
        if (isset($this->rightsGroups))
            return $this->rightsGroups;
        return $this->rightsGroups = new \JR\CORE\midleware\rights\RightsGroups($this->InternalId, $this->db);
    }

    public function can($right, $action = "view", $right_violations = true)
    {
        if (!isset($this->rights[$right][$action])) {
            bdump($right . " - " . $action, "MISSING RIGHT");

            if ($right_violations) {
                throw new RightViolationException($right . " - " . $action);
            }
            return false;
        }
        if ($right_violations && $this->rights[$right][$action] == 0) {
            throw new RightViolationException($right . " - " . $action);
        }
        return $this->rights[$right][$action];
    }

    /**
     *
     * @param int $role (0-10) filter rights that can be displaied by role
     */
    public function getUserRights($role)
    {
        $data = array();
        foreach ($this->cache as $cat) {
            foreach ($cat as $rig) {
                if ($rig['visible_from'] < $role + 1 || $rig['right_value'] == 1) {
                    $data[$rig['right_name']][$rig['right_action']] = $rig;
                }
            }
        }
        ksort($data);
        return $data;
    }

    public function getUsersWithRight($right, $action)
    {
        return $this->db->where('l.right_value', 1)
            ->where('r.right_name', $right)
            ->where('r.right_action', $action)
            ->join('rights r', 'l.right_id=r.right_id')
            ->join('users u', 'l.user_internal_id=u.internal_id')
            ->get('users_rights l', null, 'u.login_name, u.nickname, u.internal_id, u.main_role');
    }

    public function setRights($set, $unset)
    {
        if (is_array($set) && count($set) > 0) {
            foreach ($set as $u) {
                $this->db->orWhere("(right_id=? && user_internal_id=?)",
                    [$u['right_id'], $this->userInternalId]);
            }
            $this->db->update("users_rights", ['right_value' => 1]);
        }
        if (is_array($unset) && count($unset) > 0) {
            foreach ($unset as $u) {
                $this->db->orWhere("(right_id=? && user_internal_id=?)",
                    [$u['right_id'], $this->userInternalId]);
            }
            $this->db->update("users_rights", ['right_value' => 0]);
        }
    }

    public function setRight($right_id, $value)
    {
        $this->db->where("(right_id=? && user_internal_id=?)",
            [$right_id, $this->userInternalId]);
        $this->db->update("users_rights", ['right_value' => $value]);
    }

    /**
     * To return all rights in database
     */
    public function getAll()
    {
        return $this->db->get("rights");
    }

    public function createRight(
        $right,
        $action,
        $description = "",
        $granteable_by = 10,
        $visible_from = 10
    )
    {
        $this->db->insert("rights",
            [
                'right_name' => $right,
                'right_action' => $action,
                'right_description' => $description,
                'right_granteable_by' => $granteable_by,
                'visible_from' => $visible_from,
            ]);
        $this->repairRights(null);
    }

    public function repairRights($id = null)
    {
        if ($id != null) {
            $this->db->where("internal_id", $id);
        }

        $users = $this->db->get("users");
        $rights = $this->db->get("rights");
        foreach ($users as $usr) {
            foreach ($rights as $right) {
                $this->db->onDuplicate(array("user_internal_id"))
                    ->insert("users_rights",
                        ['user_internal_id' => $usr['internal_id'],
                            'right_id' => $right['right_id'],
                            'right_value' => 0]);
            }
        }
    }

    public function getRightByName($name, $action)
    {
        return $this->cache[$name][$action];
    }

}
