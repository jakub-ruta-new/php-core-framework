<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\users;

use PragmaRX\Google2FA\Google2FA;

/**
 * Description of Password
 *
 * @author jakub
 */
class Password extends \JR\CORE\midleware\Midleware
{

    protected $primaryKey = "user_internal_id";
    protected $vissible = null;
    protected $editable = null;
    protected $tableName = "users_creditals";
    protected $userID = null;

    public function __construct(\MysqliDb $db, $userID)
    {
        parent::__construct($db);
        $this->userID = $userID;
    }

    public function getAll($limit = 0, $offset = 0)
    {
        throw new \Exception("This method is not allowed!");
    }

    public function change($password)
    {
        $data = ['user_internal_id' => $this->userID,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'salt1' => 'aher',
            'salt2' => '94a6bv',
            'type' => 'jr_hash'];
        return parent::insertOrUpdate($data);
    }

    public function is2FASecretExists()
    {
        $data = $this->db->where('user_internal_id', $this->userID)
            ->getValue($this->tableName, '2fa_secret');
        if ($data) {
            return true;
        }
        return false;
    }

    public function set2FASecret(?string $secret)
    {
        $this->db->where('user_internal_id', $this->userID)->update($this->tableName, ['2fa_secret' => $secret]);
    }

    public function get2FaSecret()
    {
        return $this->db->where('user_internal_id', $this->userID)
            ->getValue($this->tableName, '2fa_secret');

    }

    public function remove2FASecret()
    {
        $this->set2FASecret(null);
    }

    public function verify2FA($code)
    {
        $twofa = new Google2FA();
        $secret = $this->get2FaSecret();
        if ($twofa->verify($code, $secret)) {
            return true;
        }
        return false;
    }

}
