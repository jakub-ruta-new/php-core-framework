<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\departments;

use JR\CORE\midleware\DuplicateRowException;

/**
 * Description of Departments
 *
 * @author jakub
 */
class Departments extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $tableName = "users_departments";
    protected $userId;

    public function __construct(\MysqliDb $db, $user_id) {
        parent::__construct($db);
        $this->userId = $user_id;
    }

    public function get($user_id = null) {
        if ($user_id == null) {
            $user_id = $this->userId;
        }
        return $this->db->where('l.user_id', $user_id)
                        ->join('departments r', "l.dep_id=r.id", 'LEFT')
                        ->get('users_departments l');
    }

    /**
     * Used by view heads on profile page
     * @return array of array with users
     */
    public function getMyAdmins() {
        $my_departments = $this->get();
        $deps = array();
        $admins = array("heads" => [], "admins" => [], "devs_head" => [], "devs" => []);
        foreach ($my_departments as $value) {
            $deps[] = $value['dep_id'];
        }
        if (count($deps) == 0) {
            return $admins;
        }
        $data = $this->db->where("l.dep_id", $deps, 'IN')
                ->where('l.role', 3, '>')
                ->join('users r', 'l.user_id=r.internal_id')
                ->join('departments d', 'l.dep_id=d.id')
                ->get('users_departments l', null, 'r.login_name,'
                . ' r.nickname, l.role, d.name, d.code');
        foreach ($data as $user) {
            if (in_array($user['role'], [5, 6, 7])) {
                $admins['heads'][] = $user;
            }
            if ($user['role'] == 4) {
                $admins['admins'][] = $user;
            }
            if (in_array($user['role'], [9])) {
                $admins['devs'][] = $user;
            }
            if (in_array($user['role'], [10])) {
                $admins['devs_head'][] = $user;
            }
        }
        return $admins;
    }

    /**
     * Fucntion for get my departements id where I have level higher than param
     * @param int from 0-10 $from_level
     * @return array of departements id
     */
    public function getMyDepartementsIds($from_level) {
        $departements = array();
        $data = $this->getMyDepartments($from_level);
        foreach ($data as $dep) {
            $departements[] = $dep['dep_id'];
        }
        return $departements;
    }

    public function getMyDepartments($from_level = -1) {
        return $this->db->where('l.user_id', $this->userId)
                        ->join('departments d', 'l.dep_id=d.id')
                        ->where('l.role', $from_level, ">=")
                        ->get($this->tableName . ' l');
    }

    public function getAllDepartments() {
        return $this->db->get('departments');
    }

    /**
     * Is user in one of given department id return true
     * @param array $deps_ids
     * @return boolean
     */
    public function isIn($deps_ids) {
        $deps = $this->getMyDepartementsIds(-1);
        foreach ($deps as $dep) {
            if (in_array($dep, $deps_ids)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return my role in department identified by givven id
     * @param int $id department_id
     */
    public function getMyRoleInDepartment($id) {
        return $this->db->where("dep_id", $id)
                        ->where('user_id', $this->userId)
                        ->getOne($this->tableName)['role'];
    }

    public function updateRole($dep_id, $new_tole) {
        return $this->db->where("dep_id", $dep_id)
                        ->where("user_id", $this->userId)
                        ->update($this->tableName, ['role' => $new_tole]);
    }

    public function addDepartment($dep_id, $new_role) {
        return $this->insertOrUpdate(['user_id' => $this->userId,
                    'dep_id' => $dep_id, 'role' => $new_role]);
    }

    public function removeDepartment($dep_id) {
        return $this->db->where("user_id", $this->userId)
                        ->where("dep_id", $dep_id)
                        ->delete($this->tableName, 1);
    }

    public function getOneDepartment($id) {
        return $this->db->where("id", $id)->getOne("departments");
    }

    /**
     * Returns user in department/s
     * @param int|array $id
     * @return array of users
     */
    public function getUsersIn($id) {
        $ids = [];
        if (!is_array($id)) {
            $ids[] = $id;
        } else {
            $ids = $id;
        }
        return $this->db->where("d.dep_id", $ids, "IN")
                        ->join("users u", "d.user_id=u.internal_id")
                        ->get($this->tableName . " d");
    }

    public function createDepartment($data) {
        try {
            $this->db->insert("departments",
                    array_intersect_key($data, array_flip(['name', 'code'])));
        } catch (\RuntimeException $ex) {
            if (str_starts_with($ex->getMessage(), "Duplicate entry")) {
                $this->db->reset();
                throw new DuplicateRowException($ex->getMessage());
            }
            throw new \Exception($ex->getMessage());
        }
        return $this->db->getInsertId();
    }

    public function getUsersIdsInMyDepartments($from_level = 5) {
        $ids = $this->getMyDepartementsIds($from_level);
        $new = [];
        foreach ($this->getUsersIn($ids) as $key => $value) {
            $new[] = $value['internal_id'];
        }
        return $new;
    }

    public function getMyMaxRole() {
        return $this->db->where("user_id", $this->userId)
                        ->orderBy("role")
                        ->getValue($this->tableName, "role");
    }

    /**
     *
     * @param stdClass $deps
     */
    public function updateFromLDAP($deps) {
        foreach ($deps as $key => $dep) {
            $data['name'] = $dep->name;
            $data['code'] = $dep->code;
            try {
                $this->createDepartment($data);
            } catch (\JR\CORE\midleware\DuplicateRowException $ex) {
                bdump($data, 'DEP not created');
                bdump($this->db->getLastQuery());
            }
        }
    }

    public function getHighestRoleIn($app_deps) {
        $app_deps_ids = [];
        foreach ($app_deps as $app_dep) {
            $app_deps_ids[] = $app_dep['id'];
        }
        $highest_role = 0;
        foreach ($this->getMyDepartments() as $dep) {
            if (in_array($dep['dep_id'], $app_deps_ids)) {
                if ($highest_role < $dep['role']) {
                    $highest_role = $dep['role'];
                }
            }
        }
        return $highest_role;
    }

    public function updateUserDepartmentsFromLDAP($deps) {
        bdump($deps);
        foreach ($deps as $dep) {
            $this->addDepartment($this->getOneDepartmentByCode('code', $dep->code)['id'], $dep->role);
            bdump($this->db->getLastQuery());
        }
    }

    public function getOneDepartmentByCode($key, $value) {
        $this->db->where($key, $value);
        return $this->db->getOne('departments');
    }

}
