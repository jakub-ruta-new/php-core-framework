<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\push;

use JR\CORE\midleware\cron\Cron;

/**
 * Description of PushUtils
 *
 * @author jakub
 */
class PushUtils extends \JR\CORE\midleware\Midleware
{

    protected $primaryKey = "id";
    protected $tableName = "users_devices";

    protected $push;
    /**
     * @var PushMessagesLog
     */
    protected $push_log;

    /**
     * @param $push
     */
    public function __construct(\MysqliDb $db)
    {
        $this->db = $db;
        $this->push = new PushMessages($db);
        $this->push_log = new PushMessagesLog($db);

    }


    /**
     * Add push device id to user
     * @param type $device_id
     * @param type $user_id
     */
    public function addDevice($device_id, $user_id)
    {
        parent::insertOrUpdate(array("device_id" => $device_id, "user_id" => $user_id));
    }

    public function getUserDevices($user_id)
    {
        return $this->db->where('user_id', $user_id)->get($this->tableName);
    }

    public function create(string $title, string $body, array $user_ids, int $user_id = 5, string $origin = 'local')
    {
        $users = [];
        foreach ($user_ids as $user_id) {
            $users[] = (string)$user_id;
        }
        $id = $this->push->insert([
            'title' => $title,
            'body' => $body,
            'origin' => $origin,
            'recepients' => json_encode($users)]);
        $cron = new Cron($this->db);
        $cron->addToQueue(Cron::$PUSH_TASK, $user_id, '0', 0);
        return $id;
    }

    public function getUnsendedPush()
    {
        return $this->push->getAllWithStatus('new');
    }

    public function setStatus($id, $status)
    {
        $this->push->setStatus($id, $status);
    }

    public function setExtId($id, $ext_id)
    {
        $this->push->setExtId($id, $ext_id);
    }

    public function setLog($event, $id, $device, $ip)
    {
        switch ($event) {
            case 'notification.dismissed':
                $event = 'dismissed';
                break;
            case 'notification.displayed':
                $event = 'displayed';
                break;
            case 'notification.clicked':
                $event = 'clicked';
                break;
            default:
                return;
        }
        $this->push_log->insert([
            'notif_ext_id' => $id,
            'device_ext_id' => $device,
            'log_type' => $event,
            'ip' => $ip,
        ]);
    }
}
