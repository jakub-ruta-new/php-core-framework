<?php

namespace JR\CORE\midleware\push;

class PushMessagesLog extends \JR\CORE\midleware\Midleware
{
    protected $primaryKey = "id";
    protected $tableName = "push_notifications_data";
}