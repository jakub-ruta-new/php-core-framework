<?php

namespace JR\CORE\midleware\push;

class PushMessages extends \JR\CORE\midleware\Midleware
{
    protected $primaryKey = "id";
    protected $tableName = "push_notifications";

    public function getAllWithStatus(string $status)
    {
        return $this->db->where('status', $status)
            ->get($this->tableName, 100);
    }

    public function setStatus($id, $status)
    {
        return $this->db->where('id', $id)
            ->update($this->tableName, ['status' => $status]);
    }

    public function setExtId($id, $ext_id)
    {
        return $this->db->where('id', $id)
            ->update($this->tableName, ['external_id' => $ext_id]);
    }
}