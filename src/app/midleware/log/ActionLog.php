<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\log;

/**
 * Description of ActionLog
 *
 * @author jakub
 */
class ActionLog extends \JR\CORE\midleware\Midleware
{

    protected $primaryKey = "id";
    protected $tableName = "action_log";
    protected $user_id;
    protected $admin_level;
    protected $ip;
    protected $type;
    protected $admin_id;
    protected $session_id;
    protected $user_id_for_next_add_log = null;

    public function __construct(\MysqliDb $db, $user_id, $ip, $admin_level = -1, $session_id = null, $type = 'normal', $admin_id = null)
    {
        parent::__construct($db);
        $this->admin_id = $admin_id;
        $this->user_id = $user_id;
        $this->ip = $ip;
        $this->type = $type;
        $this->session_id = $session_id;
        $this->admin_level = $admin_level;
    }

    public function setAdmin_level($admin_level): void
    {
        $this->admin_level = $admin_level;
    }

    public function setData($user_id, $ip, $session_id = null, $type = 'normal', $admin_id = null)
    {
        $this->admin_id = $admin_id;
        $this->user_id = $user_id;
        $this->ip = $ip;
        $this->type = $type;
        $this->session_id = $session_id;
    }

    public function geData($limit, $offset, $form_values): array
    {

        if ($form_values['user_ids'] != -1) {
            $this->db->where("l.user_id", $form_values['user_ids'], "IN");
        }
        if (isset($form_values['tool'])) {
            if ($form_values['tool'] != '--All--') {
                $this->db->where('l.tool', $form_values['tool']);
            }
        }
        if (isset($form_values['action'])) {
            if ($form_values['action'] != '--All--') {
                $this->db->where('l.action', $form_values['action']);
            }
        }
        $data = $this->db
            ->join('users u', 'l.user_id=u.internal_id', "LEFT")
            ->join('users a', 'l.admin_id=a.internal_id', "LEFT")
            ->orderBy('l.timestamp', "DESC")
            ->where('l.saw_by', $this->admin_level, "<=")
            ->where("timestamp", $form_values['from'], ">")
            ->where("timestamp", $form_values['to'], "<")
            ->get($this->tableName . ' l', array($offset, $limit),
                'l.*, u.login_name, a.login_name AS admin_login_name');
        if ($form_values['user_ids'] != -1) {
            $this->db->where("user_id", $form_values['user_ids'], "IN");
        }
        if (isset($form_values['tool'])) {
            if ($form_values['tool'] > 0) {
                $this->db->where('tool', $form_values['tool']);
            }
        }
        if (isset($form_values['action'])) {
            if ($form_values['action'] > 0) {
                $this->db->where('action', $form_values['action']);
            }
        }

        $count = $this->db
            ->where('saw_by', $this->admin_level, "<=")
            ->where("timestamp", $form_values['from'], ">")
            ->where("timestamp", $form_values['to'], "<")
            ->getValue($this->tableName, "COUNT(*)");
        return array($data, $count);
    }

    public function insert($data)
    {
        throw new \Exception("This method is not allowed!");
    }

    public function insertOrUpdate($data)
    {
        throw new \Exception("This method is not allowed!");
    }

    public function update($value, $data, $key = null)
    {
        throw new \Exception("This method is not allowed!");
    }

    public function addLog($tool, $action, $data = null, $saw_by = 1, $use_differentUserID = false)
    {
        if (is_array($data)) {
            $data = json_encode($data);
        }
        parent::insert(['user_id' => ($use_differentUserID &&
        $this->user_id_for_next_add_log != null ?
            $this->user_id_for_next_add_log :
            $this->user_id),
            'session_id' => $this->session_id,
            'tool' => $tool,
            'action' => $action,
            'data' => $data,
            'IP' => $this->ip,
            'type' => $this->type,
            'admin_id' => $this->admin_id,
            'saw_by' => $saw_by]);
        $this->user_id_for_next_add_log = null;
    }

    public function getDistinctUsers()
    {
        return $this->db->
        join('users u', 'l.user_id=u.internal_id', 'LEFT')
            ->orderBy('u.login_name', 'ASC')
            ->get($this->tableName . ' l', null,
                'DISTINCT(user_id), u.login_name, u.nickname, u.internal_id');
    }

    public function getDistinctTool()
    {
        return $this->db
            ->orderBy('tool', 'ASC')
            ->get($this->tableName, null, 'DISTINCT(tool)');
    }

    public function getDisctinctAction()
    {
        return $this->db
            ->orderBy('action', 'ASC')
            ->get($this->tableName, null, 'DISTINCT(action)');
    }

    public function getCountOf($tool, $action, $time = 60 * 60, $ip = null)
    {
        if (isset($ip)) {
            $this->db->where("IP", $ip);
        }
        return $this->db->where("tool", $tool)
            ->where("action", $action)
            ->where("timestamp", date("Y-m-d H:i:s", time() - $time), ">")
            ->getValue($this->tableName, "COUNT(*)");
    }

    public function cleanOld()
    {
        $this->db->where("timestamp", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 60), "<")
            ->delete($this->tableName);
        return "Deleted " . $this->db->count . " records until " . date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 60);
    }

    public function setUser_id_for_next_add_log($user_id_for_next_add_log): void
    {
        $this->user_id_for_next_add_log = $user_id_for_next_add_log;
    }

}
