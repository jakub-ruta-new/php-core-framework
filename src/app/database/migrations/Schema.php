<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\database\migrations;

/**
 * Description of Schema
 *
 * @author jakub
 */
class Schema {

    /**
     *
     * @var \JR\CORE\database\migrations\Table
     */
    protected $commands = array();

    /**
     * If should be all tables dropped before executing
     * ONLY FOR DEV ENV or FRESH installs
     * @var bool
     */
    protected $freshDB = false;

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    public function __construct(\MysqliDb $db, $freshDB = false) {
        $this->freshDB = $freshDB;
        $this->db = $db;
    }

    public function getDB() {
        return $this->db;
    }

    public function deleteTable($file_name, $name, $put_in_global = true) {
        $obj = new Table($name, "delete");
        if ($put_in_global) {
            $this->commands[$file_name] = $obj;
        }
        return $obj;
    }

    public function rawTable($file_name, $name, $raw) {
        $obj = new Table($name, "raw", $raw);
        $this->commands[$file_name] = $obj;
        return $obj;
    }

    protected function deleteAllDatabaseTables() {
        $this->db->setPrefix('');
        $tables = $this->db->rawQuery("SELECT
                                            table_name
                                        FROM
                                            information_schema.tables
                                        WHERE
                                            table_schema = ?", array($_ENV['DB_NAME']));
        $this->db->setPrefix($_ENV['DB_prefix']);
        $commands = array();
        foreach ($tables as $key => $value) {
            $commands[] = $this->deleteTable("", $value['table_name'], false);
        }
        $this->db->setPrefix('');
        foreach ($commands as $value) {
            $value->execute($this->db, false);
        }
        $this->db->setPrefix($_ENV['DB_prefix']);
    }

    public function execute() {
        if ($this->freshDB) {
            $this->deleteAllDatabaseTables();
        }
        if (!$this->db->tableExists("migrations")) {

            $this->createMigrationsTable();
        }
        foreach ($this->commands as $key => $value) {
            echo("Migration of $key started \n");
            try {
                $this->startMigration($key);
            } catch (\JR\CORE\database\migrations\MigrationAlreadyDoneException $exc) {
                echo("Migration of $key allready done \n");
                continue;
            }
            $this->db->setPrefix('');
            $value->execute($this->db);
            $this->db->setPrefix($_ENV['DB_prefix']);
            $this->finishMigration($key);
            echo("Migration of $key ended \n");
        }
        echo("Starting seed");
        $this->doNeededSeed();
        echo("ending seed");
    }

    protected function createMigrationsTable() {
        echo("Creating migrations table");
        $sql = "CREATE TABLE `" . $_ENV['DB_prefix'] . "migrations` (
 `file_name` varchar(128) COLLATE utf8_bin NOT NULL,
 `timestamp` datetime DEFAULT current_timestamp(),
 `state` tinyint(4) NOT NULL DEFAULT 1,
 UNIQUE KEY `file_name` (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $this->db->rawQuery($sql);
    }

    public function startMigration($file_name) {
        $existed = $this->db->where("file_name", $file_name)->getOne("migrations");
        if (isset($existed['state'])) {
            if ($existed['state'] == 2) {
                throw new MigrationAlreadyDoneException($file_name);
            } else {
                throw new MigrationStuckException("Migrations stuck on file " . $file_name);
            }
        }
        $this->db->insert("migrations", array("file_name" => $file_name));
    }

    public function finishMigration($file_name) {
        $this->db->where("file_name", $file_name)
                ->update("migrations", array("state" => "2"));
    }

    public function loadMigrations() {
        $this->loadCoreMigrations();
        $this->loadAppMigrations();
        trigger_error("Missing implementation of load app migrations", E_USER_WARNING);
    }

    protected function loadCoreMigrations($up = true) {
        $this->loadFolder(__DIR__ . "/..//core_migrations/", $up);
    }

    protected function loadFolder($path, $up = true) {
        $files = array_diff(scandir($path), array('..', '.'));
        foreach ($files as $value) {
            $class = "\JR\CORE\migrations\core_migrations\\" . str_replace(".php", "", $value);
            $migration = new $class();
            if ($up) {
                try {
                    $migration->up($this);
                } catch (\JR\CORE\database\migrations\MigrationAlreadyDoneException $exc) {
                    echo("Migration of $class allready done \n");
                }
            } else {
                $migration->down($this);
            }
        }
    }

    public function doNeededSeed() {
        $this->loadFolder(__DIR__ . "/..//core_sedders/");
        if (dir("app/database/seeders/")) {
            $this->loadFolder("app/database/seeders/");
        }
    }

    public function loadAppMigrations() {
        if (dir("app/database/migrations/")) {
            $this->loadFolder("app/database/migrations/");
        }
    }

}
