<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\database\migrations;

/**
 * Description of Migrations
 *
 * @author jakub
 */
abstract class Migrations {

    abstract function up(\JR\CORE\database\migrations\Schema $Schema);

    abstract function down(\JR\CORE\database\migrations\Schema $Schema);
}
