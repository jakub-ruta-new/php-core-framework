<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202209161318_addPushNotifications extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'push_notifications';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), $this->table_name);
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `title` varchar(256) COLLATE utf8_bin NOT NULL,
 `body` text COLLATE utf8_bin NOT NULL,
 `status` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT 'new',
 `origin` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT 'local',
 `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `external_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
 `recepients` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `external_id` (`external_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
