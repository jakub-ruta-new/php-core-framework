<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108131847_createRights
 *
 * @author jakub
 */
class migration_202108131847_createRights extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "rights");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "rights`  (
 `right_id` int(11) NOT NULL AUTO_INCREMENT,
 `right_name` varchar(32) COLLATE utf8_bin NOT NULL,
 `right_action` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'view',
 `right_description` text COLLATE utf8_bin DEFAULT NULL,
 `right_granteable_by` tinyint(4) NOT NULL DEFAULT 9,
 `visible_from` tinyint(4) NOT NULL DEFAULT 4,
 PRIMARY KEY (`right_id`),
 UNIQUE KEY `right_name` (`right_name`,`right_action`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "rights", $raw);
    }

}
