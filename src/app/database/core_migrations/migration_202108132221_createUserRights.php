<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108132221_createUserRights
 *
 * @author jakub
 */
class migration_202108132221_createUserRights extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_rights");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_rights`  (
 `user_internal_id` int(11) NOT NULL,
 `right_id` int(11) NOT NULL,
 `right_value` int(11) NOT NULL,
 UNIQUE KEY `user_internal_id` (`user_internal_id`,`right_id`),
 KEY `right_id` (`right_id`),
 CONSTRAINT `users_rights_ibfk_1` FOREIGN KEY (`right_id`) REFERENCES `rights` (`right_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `users_rights_ibfk_2` FOREIGN KEY (`user_internal_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users_rights", $raw);
    }

}
