<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108122115_createUserPersonalInformations
 *
 * @author jakub
 */
class migration_202108122115_createUserPersonalInformations extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_personal_informations");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_personal_informations`  (
 `user_internal_id` int(11) NOT NULL,
 `first_name` varchar(32) COLLATE utf8_bin NOT NULL,
 `last_name` varchar(32) COLLATE utf8_bin NOT NULL,
 `birth_date` date DEFAULT NULL,
 `country` varchar(32) COLLATE utf8_bin DEFAULT '',
 `city` varchar(32) COLLATE utf8_bin DEFAULT '',
 `zip` varchar(16) COLLATE utf8_bin DEFAULT '',
 `adress` text COLLATE utf8_bin DEFAULT '',
 `company` varchar(32) COLLATE utf8_bin DEFAULT '',
 `phone` varchar(32) COLLATE utf8_bin DEFAULT '',
 `mobile` varchar(32) COLLATE utf8_bin DEFAULT '',
 `gender` varchar(16) COLLATE utf8_bin DEFAULT '',
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 PRIMARY KEY (`user_internal_id`),
 UNIQUE KEY `mobile` (`mobile`),
 CONSTRAINT `users_personal_informations_ibfk_1` FOREIGN KEY (`user_internal_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users_personal_informations", $raw);
    }

}
