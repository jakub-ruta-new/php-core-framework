<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202207242210_createOptions extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'options';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), $this->table_name);
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `category` varchar(64) COLLATE utf8_bin NOT NULL,
 `key` varchar(64) COLLATE utf8_bin NOT NULL,
 `value` text COLLATE utf8_bin DEFAULT NULL,
 `type` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT 'string',
 `is_global` tinyint(1) NOT NULL DEFAULT 1,
 `is_feature_flag` tinyint(1) NOT NULL DEFAULT 0,
 `editable_by` tinyint(4) NOT NULL DEFAULT 9,
 `viewable_by` int(11) NOT NULL DEFAULT 4,
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `updated_by` int(11) DEFAULT NULL,
 `default_value` text COLLATE utf8_bin DEFAULT NULL,
 `description` text COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `key` (`key`,`category`),
 KEY `updated_by` (`updated_by`),
 CONSTRAINT `settings_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`internal_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
