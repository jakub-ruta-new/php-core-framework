<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202210052108_addGoogleIdToUser extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'users';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), $this->table_name);
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "ALTER TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "` ADD `google_id` VARCHAR(128) NULL DEFAULT NULL AFTER `portal_id`, ADD UNIQUE (`google_id`);";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
