<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202109072129_createRightsGroupsAssign extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "rights_groups_assign");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "rights_groups_assign` (
 `right_id` int(11) NOT NULL,
 `group_id` int(11) NOT NULL,
 UNIQUE KEY `right_id` (`right_id`,`group_id`),
 KEY `group_id` (`group_id`),
 CONSTRAINT `rights_groups_assign_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `rights_groups` (`id`) ON UPDATE CASCADE,
 CONSTRAINT `rights_groups_assign_ibfk_2` FOREIGN KEY (`right_id`) REFERENCES `rights` (`right_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "rights_groups_assign", $raw);
    }

}
