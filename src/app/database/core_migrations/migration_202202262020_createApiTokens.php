<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202202262020_createApiTokens extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "api_tokens");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "api_tokens` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `app_code` varchar(126) COLLATE utf8_bin NOT NULL,
 `token` varchar(255) COLLATE utf8_bin NOT NULL,
 `type` set('get','set','send','setup','recovery','secret') COLLATE utf8_bin NOT NULL,
 `expire` datetime NOT NULL DEFAULT current_timestamp(),
 `auto_renew` tinyint(1) NOT NULL DEFAULT 0,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 PRIMARY KEY (`id`),
 UNIQUE KEY `app_id` (`app_code`,`type`),
 KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "api_tokens", $raw);
    }

}
