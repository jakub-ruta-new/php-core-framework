<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111052140_createDepartmentInvitations extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "departments_invitations");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "departments_invitations` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `admin_id` int(11) NOT NULL,
 `user_id` int(11) NOT NULL,
 `department_id` int(11) NOT NULL,
 `state` set('pending','decelined','accepted','withdrawn') COLLATE utf8_bin NOT NULL DEFAULT 'pending',
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `role` tinyint(4) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `admin_id` (`admin_id`),
 KEY `user_id` (`user_id`),
 KEY `department_id` (`department_id`),
 CONSTRAINT `departments_invitations_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`internal_id`) ON UPDATE CASCADE,
 CONSTRAINT `departments_invitations_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`internal_id`) ON UPDATE CASCADE,
 CONSTRAINT `departments_invitations_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "departments_invitations", $raw);
    }

}
