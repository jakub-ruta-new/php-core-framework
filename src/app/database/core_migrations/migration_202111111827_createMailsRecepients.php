<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111111827_createMailsRecepients extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "mails_recepients");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "mails_recepients`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `mail_id` int(11) NOT NULL,
 `mail` varchar(128) COLLATE utf8_bin DEFAULT NULL,
 `status` set('new','pending','sended','error') COLLATE utf8_bin NOT NULL DEFAULT 'new',
 `sent` datetime DEFAULT NULL,
 `message` text COLLATE utf8_bin DEFAULT NULL,
 `user_id` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `mail_id` (`mail_id`),
 KEY `user_id` (`user_id`),
 CONSTRAINT `mails_recepients_ibfk_1` FOREIGN KEY (`mail_id`) REFERENCES `mails` (`id`) ON UPDATE CASCADE,
 CONSTRAINT `mails_recepients_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`internal_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "mails_recepients", $raw);
    }

}
