<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111081423_createUsersNotifications extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_notifications");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `type` set('custom','system','department_invitation','department_invitation_accept','department_invitation_deceline') COLLATE utf8_bin NOT NULL DEFAULT 'system',
 `color` set('success','danger','secondary','primary','') COLLATE utf8_bin DEFAULT '',
 `new` tinyint(1) NOT NULL DEFAULT 1,
 `from_user_id` int(11) NOT NULL DEFAULT 5,
 `head` varchar(40) COLLATE utf8_bin DEFAULT NULL,
 `content` varchar(128) COLLATE utf8_bin DEFAULT NULL,
 `link` varchar(128) COLLATE utf8_bin DEFAULT NULL,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 PRIMARY KEY (`id`),
 KEY `from_user_id` (`from_user_id`),
 KEY `user_id` (`user_id`),
 CONSTRAINT `users_notifications_ibfk_1` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `users_notifications_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users_notifications", $raw);
    }

}
