<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108141118_createUsersSession extends \JR\CORE\database\migrations\Migrations
{

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), "users_sessions");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_sessions` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `token` varchar(255) COLLATE utf8_bin NOT NULL,
 `expire` int(11) NOT NULL DEFAULT 0,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `type` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'normal',
 `admin_id` int(11) DEFAULT NULL,
 `IP` varchar(32) COLLATE utf8_bin DEFAULT NULL,
 `browser` text COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `token` (`token`),
 KEY `admin_id` (`admin_id`),
 KEY `user_id` (`user_id`),
 CONSTRAINT `users_sessions_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `users_sessions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users_sessions", $raw);
    }

}
