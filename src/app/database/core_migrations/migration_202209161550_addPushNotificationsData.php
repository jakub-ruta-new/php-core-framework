<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202209161550_addPushNotificationsData extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'push_notifications_data';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), $this->table_name);
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "` (
 `log_id` int(11) NOT NULL AUTO_INCREMENT,
 `notif_ext_id` varchar(128) COLLATE utf8_bin NOT NULL,
 `device_ext_id` varchar(128) COLLATE utf8_bin NOT NULL,
 `log_type` set('dismissed','clicked','displayed') COLLATE utf8_bin NOT NULL,
 `log_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`log_id`),
 KEY `notif_id` (`notif_ext_id`),
 KEY `user_id` (`device_ext_id`),
 CONSTRAINT `push_notifications_data_ibfk_1` FOREIGN KEY (`device_ext_id`) REFERENCES `users_devices` (`device_id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `push_notifications_data_ibfk_2` FOREIGN KEY (`notif_ext_id`) REFERENCES `push_notifications` (`external_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
