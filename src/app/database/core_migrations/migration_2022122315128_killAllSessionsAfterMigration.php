<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_2022122315128_killAllSessionsAfterMigration
 *
 * @author jakub
 */
class migration_2022122315128_killAllSessionsAfterMigration extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'users_sessions';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        throw new \Exception('not possible');
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = " UPDATE `" . $_ENV['DB_prefix'] . $this->table_name . "` SET `expire`=1 WHERE 1;";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
