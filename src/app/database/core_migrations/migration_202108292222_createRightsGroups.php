<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108292222_createRightsGroups extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "rights_groups");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "rights_groups`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(64) COLLATE utf8_bin NOT NULL,
 `department_id` int(11) NOT NULL DEFAULT -1,
 `role` int(11) NOT NULL DEFAULT -1,
 `note` varchar(256) COLLATE utf8_bin DEFAULT NULL,
 `system` tinyint(1) NOT NULL DEFAULT 0,
 PRIMARY KEY (`id`),
 KEY `department_id` (`department_id`),
 CONSTRAINT `rights_groups_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "rights_groups", $raw);
    }

}
