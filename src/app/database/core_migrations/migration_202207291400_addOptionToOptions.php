<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202207291400_addOptionToOptions extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'options';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), $this->table_name);
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "ALTER TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "`  ADD `options` TEXT NULL DEFAULT NULL AFTER `description`;;";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
