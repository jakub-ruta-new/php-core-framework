<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111091538_createCronTasksOverview extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "cron_tasks_overview");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "cron_tasks_overview`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) COLLATE utf8_bin NOT NULL,
 `enabled` tinyint(1) NOT NULL DEFAULT 1,
 `default_priority` tinyint(4) NOT NULL DEFAULT 5,
 `auto_repeat` varchar(11) COLLATE utf8_bin NOT NULL DEFAULT '0',
 `default_data` text COLLATE utf8_bin DEFAULT NULL,
 `path` varchar(128) COLLATE utf8_bin NOT NULL,
 `long` tinyint(4) NOT NULL DEFAULT 2,
 `default_due` int(11) NOT NULL DEFAULT 0,
 `default_expiry` int(11) NOT NULL DEFAULT 5184000,
 `manual_level` int(11) NOT NULL DEFAULT 5,
 PRIMARY KEY (`id`),
 KEY `long` (`long`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "cron_tasks_overview", $raw);
    }

}
