<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;


class migration_202303051213_create2FA extends \JR\CORE\database\migrations\Migrations
{

    protected $table_name = 'users';

    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        throw new \Exception('not possible');
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = " ALTER TABLE `" . $_ENV['DB_prefix'] . $this->table_name . "` ADD `2fa_settings` VARCHAR(64) NOT NULL DEFAULT 'no' AFTER `banned`;";
        $Schema->rawTable(get_class($this), $this->table_name, $raw);
    }

}
