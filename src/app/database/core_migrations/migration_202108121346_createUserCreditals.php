<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108121346_createUserCreditals
 *
 * @author jakub
 */
class migration_202108121346_createUserCreditals extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_creditals");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_creditals` (
 `user_internal_id` int(11) NOT NULL,
 `password` varchar(128) COLLATE utf8_bin NOT NULL,
 `salt1` varchar(16) COLLATE utf8_bin DEFAULT NULL,
 `salt2` varchar(32) COLLATE utf8_bin DEFAULT NULL,
 `type` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT 'jr_hash',
 `created` datetime DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 PRIMARY KEY (`user_internal_id`),
 CONSTRAINT `users_creditals_ibfk_1` FOREIGN KEY (`user_internal_id`) REFERENCES `users` (`internal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users_creditals", $raw);
    }

}
