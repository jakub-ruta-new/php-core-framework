<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108241922_createActionLog extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "action_log");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "action_log` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) DEFAULT NULL,
 `session_id` text COLLATE utf8_bin DEFAULT NULL,
 `tool` varchar(32) COLLATE utf8_bin NOT NULL,
 `action` varchar(32) COLLATE utf8_bin NOT NULL,
 `data` text COLLATE utf8_bin DEFAULT NULL,
 `type` varchar(16) COLLATE utf8_bin NOT NULL,
 `admin_id` int(11) DEFAULT NULL,
 `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
 `IP` varchar(32) COLLATE utf8_bin NOT NULL,
 `saw_by` tinyint(4) NOT NULL DEFAULT 0,
 PRIMARY KEY (`id`),
 KEY `user_id` (`user_id`),
 KEY `admin_id` (`admin_id`),
 CONSTRAINT `action_log_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`internal_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "action_log", $raw);
    }

}
