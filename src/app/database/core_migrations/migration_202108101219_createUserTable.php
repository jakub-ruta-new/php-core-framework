<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of 202108101219_createUserTable
 *
 * @author jakub
 */
class migration_202108101219_createUserTable extends \JR\CORE\database\migrations\Migrations
{

    //put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->deleteTable(get_class($this), "users");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users`  (
 `internal_id` int(11) NOT NULL AUTO_INCREMENT,
 `portal_id` int(11) DEFAULT NULL,
 `login_name` varchar(32) COLLATE utf8_bin NOT NULL,
 `nickname` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
 `mail` varchar(64) COLLATE utf8_bin DEFAULT NULL,
 `main_role` int(11) NOT NULL DEFAULT 0,
 `origin` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'local',
 `locale` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT 'en_EN',
 `avatar` text COLLATE utf8_bin DEFAULT NULL,
 `registered` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `disabled` tinyint(1) NOT NULL DEFAULT 0,
 `mail_verified_at` datetime DEFAULT NULL,
 `banned` tinyint(1) NOT NULL DEFAULT 0,
 PRIMARY KEY (`internal_id`),
 UNIQUE KEY `login_name` (`login_name`),
 UNIQUE KEY `portal_id` (`portal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
        $Schema->rawTable(get_class($this), "users", $raw);
    }

}
