<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202209170814_AddPushFeatureFlag extends \JR\CORE\database\migrations\Migrations
{

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->startMigration(get_class($this));
        $dataKeys = array(
            "category",
            "key",
            "value",
            "type",
            "is_global",
            "is_feature_flag",
            "editable_by",
            "viewable_by",
            "updated",
            "default_value",
            "description",
            'options'
        );
        $options['options'] = ['off' => 'Off', 'ldap' => 'LDAP', 'onesignal' => 'OneSignal'];
        $multiInsertData = array(
            array("feature_flag", 'push_notifications', 0, 'select', 1, 1, 10, 6, null, 'off', 'Push notification settings', json_encode($options)),

        );
        $Schema->getDB()->insertMulti("options", $multiInsertData, $dataKeys);

        $Schema->finishMigration(get_class($this));
    }

}
