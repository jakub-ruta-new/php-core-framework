<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202207242214_AddOptions extends \JR\CORE\database\migrations\Migrations
{

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->startMigration(get_class($this));
        $dataKeys = array(
            "category",
            "key",
            "value",
            "type",
            "is_global",
            "is_feature_flag",
            "editable_by",
            "viewable_by",
            "updated",
            "default_value",
            "description"
        );
        $multiInsertData = array(
            array("app", 'infoBar', null, 'string', 1, 0, 9, 4, null, null, 'Text for special information (f.e. for maitenance) Vissible everywhere! (should be used only by devs)'),
            array("feature_flag", 'enable_user_override', 0, 'bool', 1, 1, 10, 4, null, 0, 'Enable Feature flags user override'),

        );
        $Schema->getDB()->insertMulti("options", $multiInsertData, $dataKeys);

        $Schema->finishMigration(get_class($this));
    }

}
