<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202111091549_AddCronJobs extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("id", "name",
            "enabled",
            "default_priority",
            "auto_repeat",
            "default_data",
            "path",
            "long",
            "default_due",
            "default_expiry",
            "manual_level");
        $multiInsertData = array(
            array(1, "Tasks dispatch", 1, 0, "auto", null,
                'JR\CORE\controlers\cron\\tasks\Task::dispatch::null', 0, 0, "5184000", 9),
            array(2, "Old tasks delete", 1, 9, "1 day", null,
                'JR\CORE\controlers\cron\\tasks\Task::deleteOld::null', 0, 0, "172799", 9),
            array(3, "Mail queue process", 1, 2, "10 minutes", null,
                'JR\CORE\controlers\cron\tasks\mail\MailTask::dispatch::null', 5, 0, "599", 7),
            array(4, "Clean ActionLog", 1, 9, "1 day", null,
                'JR\CORE\controlers\cron\tasks\cleaning\Cleaning::actionLog::null', 5, 0, "172799", 9),
            array(5, "Renew API Tokens", 1, 4, "1 hour", null,
                'JR\CORE\controlers\cron\tasks\app\RenewTokensTask::renewTokens::null', 2, 0, "172799", 7),
        );
        $Schema->getDB()->insertMulti("cron_tasks_overview", $multiInsertData, $dataKeys);
        $Schema->getDB()->insert("cron_tasks", ['task_id' => 1, "priority" => 0,
            "owner" => 6, "due" => "1999-01-01 00:00:00", "expire" => "2050-12-30 23:59:59",
            "status" => "pending"]);
        $Schema->finishMigration(get_class($this));
    }

}
