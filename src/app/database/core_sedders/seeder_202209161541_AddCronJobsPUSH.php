<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202209161541_AddCronJobsPUSH extends \JR\CORE\database\migrations\Migrations
{

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("id", "name",
            "enabled",
            "default_priority",
            "auto_repeat",
            "default_data",
            "path",
            "long",
            "default_due",
            "default_expiry",
            "manual_level");
        $multiInsertData = array(
            array(6, "Push queue process", 1, 1, "1 day", null,
                'JR\CORE\controlers\cron\tasks\push\PushTask::dispatch::null', 5, 0, "172799", 7),
        );
        $Schema->getDB()->insertMulti("cron_tasks_overview", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
