<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202108292225_createRightsGroups extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("name",
            "role",
            "note",
            "system");
        $multiInsertData = array(
            array("default_all", "10",
                "All rights for Head of development", "1"),
            array("system_guest", "-1",
                "Rights for not logged in user", "1"),
            array("system_lvl_guest", "0",
                "Default rights for guest", "1"),
            array("system_lvl_0", "1",
                "Default rights for member lvl 0", "1"),
            array("system_lvl_1", "2",
                "Default rights for member lvl 1", "1"),
            array("system_lvl_2", "3",
                "Default rights for member lvl 2", "1"),
            array("system_lvl_admin", "4",
                "Default rights for member lvl admin", "1"),
            array("system_lvl_head", "5",
                "Default rights for member lvl head", "1"),
            array("system_lvl_supervisor", "6",
                "Default rights for member lvl supervisor", "1"),
            array("system_lvl_director", "7",
                "Default rights for member lvl director", "1"),
            array("system_lvl_developer", "9",
                "Default rights for member lvl developer", "1"),
        );
        $Schema->getDB()->insertMulti("rights_groups", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
