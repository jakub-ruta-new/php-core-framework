<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108101241_createDefaultUsers
 *
 * @author jakub
 */
class seeder_202108101242_createDefaultDepartements extends \JR\CORE\database\migrations\Migrations {

    //put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("id", "name", "code");
        $multiInsertData = array(
            array("-1", "System", "system"),
        );
        $Schema->getDB()->insertMulti("departments", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
