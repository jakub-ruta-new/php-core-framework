<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 *
 * @author jakub
 */
class seeder_202303111608_Add2FAFeatureFlag extends \JR\CORE\database\migrations\Migrations
{

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema)
    {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema)
    {
        $Schema->startMigration(get_class($this));
        $dataKeys = array(
            "category",
            "key",
            "value",
            "type",
            "is_global",
            "is_feature_flag",
            "editable_by",
            "viewable_by",
            "updated",
            "default_value",
            "description",
            'options'
        );
        $multiInsertData = array(
            array("feature_flag", '2fa_administrative_access_required', 0, 'bool', 1, 1, 10, 2, null, 0, 'Require 2FA auth to gain administrative access via role admin and higher', null),
        );
        $Schema->getDB()->insertMulti("options", $multiInsertData, $dataKeys);

        $Schema->finishMigration(get_class($this));
    }

}
