<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202108132212_createRights extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("right_name", "right_action",
            "right_description",
            "right_granteable_by", "visible_from");
        $multiInsertData = array(
            array("users", "view",
                "Right allow to see my users in app", 5, 4),
            array("users", "edit",
                "Right allow to edit my users with lower role", 5, 4),
            array("users", "all",
                "Right allow to view all users and edit users with lower role", 7, 4),
            array("users", "admin_login",
                "Right allow to log in as user", 10, 9),
            array("rights", "overRide",
                "Right allow to override all rights", 10, 10),
            array("rights", "change_password",
                "Right allow to change password. DO NOT GRANT FOR LDAP USERS!", 4, 0),
            array("rights", "my_rights",
                "Right allow see granted rights on profile!", 4, 10),
            array("rights", "view",
                "Right allow see rights groups", 5, 4),
            array("rights", "assign",
                "Right allow assign rights groups to users", 5, 10),
            array("annoucements", "view",
                "Right to view all annoucements", 5, 2),
            array("ip_tools", "view",
                "Right to view IP bans", 4, 2),
            array("configuration", "view",
                "Right to view config", 4, 2),
            array("tech_admin", "view",
                "Right for tech admin for various page", 6, 4),
            array("deamon", "view",
                "Right for Cron deamon overview", 4, 1),
            array("action_log", "view",
                "Right to view action log", 4, 0),
            array("action_log", "my",
                "Right to view Action log of users in my department", 5, 4),
            array("action_log", "all",
                "Right to view Action log of ALL users", 7, 5),
            array("departments", "edit",
                "Right to edit departments", 6, 4),
            array("departments", "all",
                "Right to view ALL departments", 7, 5),
            array("translations", "view",
                "Right to view Transaltions", 4, 3),
            array("translations", "edit",
                "Right to edit Translations", 7, 5),
            array("translations", "create",
                "Right to create Languages for Translations", 9, 5),
            array("mail_tool", "view",
                "Right to view emails queue", 5, 5),
            array("mail_tool", "my",
                "Right to view emails queue for my departments", 5, 5),
            array("mail_tool", "all",
                "Right to view emails queue for everyone", 8, 6),
        );
        $Schema->getDB()->insertMulti("rights", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
