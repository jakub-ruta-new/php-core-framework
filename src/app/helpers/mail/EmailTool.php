<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\mail;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

/**
 * Description of EmailTool
 *
 * @author jakub
 */
class EmailTool {

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \PHPMailer\PHPMailer\PHPMailer
     */
    protected $mail;

    /**
     *
     * @var \JR\CORE\midleware\mail\Mail
     */
    protected $utils;
    protected $mailerPrepared = false;

    public function __construct(\MysqliDb $db) {
        $this->db = $db;
        $this->utils = new \JR\CORE\midleware\mail\Mail($db);
    }

    protected function preparePHPMailer() {
        if ($this->mailerPrepared) {
            $this->mail->clearAllRecipients();
            return;
        }
        $this->mail = new PHPMailer(true);
        $this->mail->SMTPAutoTLS = false;
//$this->mail->SMTPDebug = $_ENV['env'] == "dev"?SMTP::DEBUG_LOWLEVEL:;                      //Enable verbose debug output
        $this->mail->isSMTP();                                            //Send using SMTP
        $this->mail->Host = $_ENV['SMTP_HOST'];                     //Set the SMTP server to send through
        $this->mail->SMTPAuth = true;                                   //Enable SMTP authentication
        $this->mail->Username = $_ENV['SMTP_NAME'];                     //SMTP username
        $this->mail->Password = $_ENV['SMTP_PASS'];                               //SMTP password
//$this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
        $this->mail->Port = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        $this->mail->setFrom($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
    }

    /**
     *
     * @param string $subject
     * @param string $content
     * @param array of strings $recepients
     * @param bool $sent_immidiattely
     */
    public function addToQueue($subject, $content, $recepients, $sent_immidiattely = false) {
        $id = $this->utils->insert(['subject' => $subject,
            "content" => $content]);
        $this->utils->addRecepients($id, $recepients);
        if ($sent_immidiattely) {
            $this->sendMail($id);
        }
        return $id;
    }

    public function Test() {
        $this->preparePHPMailer();
        try {
            $this->mail->addAddress('jakub@jakubruta.tk', 'Jakub Ruta');
            $this->mail->addAddress('jakub.ruta123@googlemail.com');
            $this->mail->isHTML(true);                                  //Set email format to HTML
            $this->mail->Subject = 'Here is the subject';
            $this->mail->Body = 'This is the HTML message body <b>in bold!</b>';
            $this->mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $this->mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$this->mail->ErrorInfo}";
        }
        die;
    }

    public function sendMail($id) {
        try {
            $email = $this->utils->getOneForSend($id);
            $recepients = $this->utils->getRecepients($id);
            foreach ($recepients as $r) {
                try {
                    $this->send($email, $r);
                } catch (Exception $e) {
                    bdump($ex);
                    $this->utils->setSendStatus($r['id'], "error", "Mailer Error: {$this->mail->ErrorInfo}");
                    continue;
                }
                $this->utils->setSendStatus($r['id'], "sended", "OK", date("Y-m-d H:i:s"));
            }
        } catch (JR\CORE\midleware\mail\MailNotNew $ex) {
            return $id . " mail not new!";
        }
        $this->utils->setStatus($id, "sended");
    }

    public function send($email, $r) {
        $this->preparePHPMailer();
        $this->mail->addAddress($r['mail']);
        $this->mail->isHTML(true);
        $this->mail->Subject = $email['subject'];
        $this->mail->Body = $email['content'];
        $this->mail->send();
    }

    public function processQueue() {
        $mails = $this->utils->getMailsForSend();
        foreach ($mails as $m) {
            $this->sendMail($m['id']);
        }
        return "Done " . count($mails);
    }

    public function getDistinctUsers() {
        return $this->db->join("users u", "r.mail=u.mail", "LEFT")
                        ->join("mails m", "r.mail_id=m.id")
                        ->groupBy("u.internal_id")
                        ->get('mails_recepients r');
    }

    public function getData($limit, $offset, $form_values) {
        if ($form_values['user_ids'] != -1) {
            $this->db->where("u.internal_id", $form_values['user_ids'], "IN");
        }
        $data = $this->db->join("users u", "r.mail=u.mail", "LEFT")
                ->join("mails m", "r.mail_id=m.id")
                ->groupBy("u.internal_id")
                ->where('m.send_at', $form_values['from'], ">")
                ->where('m.send_at', $form_values['to'], "<")
                ->get('mails_recepients r', [$offset, $limit]);
        if ($form_values['user_ids'] != -1) {
            $this->db->where("u.internal_id", $form_values['user_ids'], "IN");
        }
        $count = $this->db->join("users u", "r.mail=u.mail", "LEFT")
                ->join("mails m", "r.mail_id=m.id")
                ->where('m.send_at', $form_values['from'], ">")
                ->where('m.send_at', $form_values['to'], "<")
                ->getValue('mails_recepients r', "COUNT(*)", [$offset, $limit]);
        return [$count[0], $data];
    }

}
