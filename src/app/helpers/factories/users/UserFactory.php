<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\factories\users;

/**
 * Description of UserFactory
 *
 * @author jakub
 */
class UserFactory extends \JR\CORE\helpers\factories\Factory
{

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep)
    {
        parent::__construct($dep);
        $this->tryLoginViaCookie();
    }

    protected function tryLoginViaCookie()
    {
        $cookie = $this->dep->getCookies()->getUserSession();
        if ($cookie) {
            $cookie = explode("$", $cookie);
            $data = \JR\CORE\midleware\sessions\Session::getUserSession($cookie[1],
                $cookie[0], $this->dep->getDB());
            if (isset($data['type']) && $data['expire'] > time() + 10) {
                if ($data['type'] == 'admin') {
                    $this->dep->setAdmin(new \JR\CORE\midleware\users\User($data['admin_id'], $this->dep->getDB()));
                }
                $this->dep->setUser($user = new \JR\CORE\midleware\users\User($data['user_id'], $this->dep->getDB()));
                $user->setRoleSession(($data['active_role'] > $user->getMainRole() ? $user->getMainRole() : $data['active_role']));
                $user->currentSessionId = $data['id'];
                return true;
            } else {
                $this->dep->getCookies()->destroySession();
            }
        }
        $this->dep->setUser(new \JR\CORE\midleware\users\User(1, $this->dep->getDB()));
    }

    public function tryLogin($session, $request)
    {
        return new \JR\CORE\midleware\users\User(1, $this->dep->getDB());
    }

    public function tryLocalLogin($userName, $password, $remember_me)
    {
        $userMidleware = $this->dep->getUser();
        $user = $userMidleware->getOne($userName, "LOWER(login_name)");
        if ($user == null || $user['internal_id'] < 10) {
            throw new LocalLoginException();
        }
        if ($user['banned'] == 1) {
            throw new AccountBannedException();
        }
        if ($user['disabled'] == 1) {
            throw new AccountDissabledException();
        }
        $pass = $userMidleware->password->getOne($user['internal_id'], "user_internal_id");
        switch ($pass['type']) {
            case 'jr_hash':
                if (password_verify($password, $pass['password'])) {
                    $this->loginUser($user['internal_id'], $remember_me);
                    return;
                }
                throw new LocalLoginException();
                break;
            case 'old_hash':
                $hash = sha1($password . $pass['salt1']);
                if ($hash === $pass['password']) {
                    $this->loginUser($user['internal_id'], $remember_me);
                    $this->dep->getUser()->password->change($password);
                    return;
                }
                throw new LocalLoginException();
                break;
            case 'new_hash':
                $hash = hash('sha256', ($pass['salt2'] . $password . $pass['salt1']));
                if ($hash === $pass['password']) {
                    $this->loginUser($user['internal_id'], $remember_me);
                    $this->dep->getUser()->password->change($password);
                    return;
                }
                throw new LocalLoginException();
                break;
            default:
                throw new LocalLoginException();
        }
    }

    public function loginUser($user_id, $rememberMe = true, $type = 'normal', $adminId = null)
    {
        $user = new \JR\CORE\midleware\users\User($user_id, $this->dep->getDB());
        $user_data = $user->get();
        if ($type != 'admin') {
            if ($user_data == null || $user_data['internal_id'] < 10) {
                throw new LocalLoginException();
            }
            if ($user_data['banned'] == 1) {
                throw new AccountBannedException();
            }
            if ($user_data['disabled'] == 1) {
                throw new AccountDissabledException();
            }
        }
        $data = $user->userSession->insertSess($this->dep->getRequest()->getUserIP(),
            $this->dep->getRequest()->getUserAgent(),
            ($user_data['main_role'] > 3 ? 3 : $user_data['main_role']), $rememberMe, $type, $adminId);
        $data[0] = $user->getInternalId() . '$' . $data[0];
        $this->dep->getCookies()->setSession($data);
        $this->dep->setUser($user);
    }

    public function logoutUser()
    {
        $cookie = $this->dep->getCookies()->getUserSession();
        $cookie = explode("$", $cookie);
        $this->dep->getUser()->userSession->destroySession($cookie[1]);
        $this->dep->getCookies()->destroySession();
    }

    public function getUserByLoginName($login_name)
    {
        return \JR\CORE\midleware\users\User::getUserByLoginName($login_name, $this->dep->getDB());
    }

    public function getUserByPortalId($id)
    {
        return \JR\CORE\midleware\users\User::getUserByPortalId($id, $this->dep->getDB());
    }

    public function collectUserDataForAppLogin($user_id, $app_deps)
    {
        $user = new \JR\CORE\midleware\users\User($user_id, $this->dep->getDB());
        $user->checkLoginState();
        $data['user']['login_name'] = $user->getLoginName();
        $data['user']['nickname'] = $user->getNickName();
        $data['user']['id'] = $user->getInternalId();
        $data['user']['mail'] = $user->getMail();
        $data['user']['locale'] = $user->getLang();
        $data['user']['avatar'] = $user->getAvatar();
        $data['user']['role'] = ($user->getRole() > 9 ? $user->getRole() : $user->departments->getHighestRoleIn($app_deps));
        $data['departements'] = $user->departments->getMyDepartments();
        $data['all_departments'] = $user->departments->getAllDepartments();
        return $data;
    }

    public function tryLDAPLogin($user, $admin_id = null)
    {
        $userMidleware = $this->dep->getUser();
        $user_loc = $userMidleware->getOne($user->id, 'portal_id');
        if ($user_loc == null || $user_loc['internal_id'] < 10) {
            $id = $this->registerUser($user->login_name, $user->mail);
            $this->loginUser($id, false, ($admin_id ? 'admin' : 'normal'), $admin_id);
            $user_class = $this->dep->getUser();
            $user_class->setPortalId($user->id);
            $user_class->setOrigin('ldap');
        } else if ($user_loc['banned'] == 1) {
            throw new AccountBannedException();
        } else {
            $this->loginUser($user_loc['internal_id'], false, ($admin_id ? 'admin' : 'normal'), $admin_id);
            $user_class = $this->dep->getUser();
            $data['login_name'] = $user->login_name;
            $data['locale'] = $user->locale;
            $data['mail'] = $user->mail;
            $data['avatar'] = $user->avatar;
            $user_class->updateUser($data);
        }
    }

    public function registerUser($name, $email, $password = null, $departements = array(), $dev = false)
    {
        $id = $this->dep->getUser()->insert(["login_name" => $name,
            "mail" => $email, 'nickname' => ucfirst($name), 'main_role' => $dev ? "10" : "0"]);
        if (isset($password)) {
            $this->dep->getUser()->password->insert(['user_internal_id' => $id,
                "password" => password_hash($password, PASSWORD_DEFAULT)]);
        }
        $this->dep->getUser()->rights->repairRights($id);
        if (count($departements) > 0) {
            $user = new \JR\CORE\midleware\users\User($id, $this->dep->getDB());
            foreach ($departements as $key => $value) {
                $user->departments->addDepartment($key, $value);
            }
        }
        return $id;
    }

    public function getUserByGoogleId(string $id)
    {
        return \JR\CORE\midleware\users\User::getUserByGoogleId($id, $this->dep->getDB());
    }

}
