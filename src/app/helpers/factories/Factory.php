<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\factories;

/**
 * Description of Factory
 *
 * @author jakub
 */
class Factory {

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        $this->dep = $dep;
    }

}
