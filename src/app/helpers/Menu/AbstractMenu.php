<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app\menu;

/**
 * Description of Menu
 *
 * @author jakub
 */
abstract class AbstractMenu {

    /**
     *
     * @var \JR\CORE\helpers\menu\NavBar
     */
    protected $navbar = null;

    /**
     *
     * @var \JR\CORE\helpers\menu\SideBar
     */
    protected $sidebar = null;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $user;

    function __construct(\JR\CORE\request\request $request,
            \JR\CORE\helpers\menu\SideBar $sidebar,
            \JR\CORE\helpers\menu\NavBar$navbar,
            \JR\CORE\midleware\users\User $user) {
        $this->request = $request;
        $this->user = $user;
        $this->navbar = $navbar;
        $this->sidebar = $sidebar;
        $this->AddToNavbar();
    }

    abstract public function AddToNavbar(): void;
}
