<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\menu;

/**
 * Description of MenuItem
 *
 * @author jakub
 */
class MenuItem
{

    public $category;
    public $name;
    public $url;
    public $is_active = false;
    public $icon = null;
    public $counter = 0;
    public $counter_color = 'secondary';

    function __construct($category, $name, $url, $icon = null, $active = false)
    {
        $this->category = $category;
        $this->name = $name;
        $this->url = $url;
        $this->is_active = $active;
        $this->icon = $icon;
    }

}
