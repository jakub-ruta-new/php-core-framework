<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\menu;

/**
 * Description of Menu
 *
 * @author jakub
 */
class Menu
{

    public $applications = null;
    /**
     *
     * @var \JR\CORE\helpers\menu\NavBar
     */
    protected $navbar = null;
    /**
     *
     * @var \JR\CORE\helpers\menu\SideBar
     */
    protected $sidebar = null;
    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;
    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $user;

    function __construct(\JR\CORE\request\request $request, \JR\CORE\midleware\users\User $user, \JR\CORE\helpers\dependencies\DependenciContainer $dep, array $menuCounter)
    {
        $this->request = $request;
        $this->user = $user;
        $this->navbar = $this->buildNavBar();
        $this->sidebar = $this->buildSidebar();
        if (class_exists("\JR\CORE\app\menu\Menu")) {
            new \JR\CORE\app\menu\Menu($request, $this->sidebar, $this->navbar, $user);
        }
        $this->addCounters($menuCounter);
        $ldap_apps = $dep->getSession()->getFromTemp("ldap_apps");
        if (isset($ldap_apps) && $ldap_apps['expire'] > time()) {
            $this->applications = $ldap_apps['apps'];
        }
    }

    private function buildNavBar()
    {
        $navbar = new NavBar();
        // Default menu options
        $this->addDefaultToNavBar($navbar);

        return $navbar;
    }

    public function addDefaultToNavBar($navbar)
    {
        $rights = $this->user->rights;
        if ($rights->can("annoucements", "view", false)) {
            $navbar->addOne(new MenuItem("Admin", "Annoucements",
                "admin/annoucements", "fas fa-bullhorn",
                $this->request->startWith("admin/annoucements")));
        }
        if ($rights->can("ip_tools", "view", false)) {
            $navbar->addOne(new MenuItem("Tech Admin", "IP tools",
                "tech-admin/ip_tools", "fas fa-ban",
                $this->request->startWith("admin/ip_tools")));
        }
        if ($rights->can("configuration", "view", false)) {
            $navbar->addOne(new MenuItem("zDebug", "Configuration",
                "debug/config", "fas fa-tools",
                $this->request->startWith("debug/config")));
        }
        if ($rights->can("users", "view", false)) {
            $navbar->addOne(new MenuItem("Admin", "Users",
                "admin/users", "fas fa-users-cog",
                $this->request->startWith("admin/users")));
        }
        if ($rights->can("rights", "view", false)) {
            $navbar->addOne(new MenuItem("Admin", "Rights groups",
                "admin/rights-groups", "fas fa-th",
                $this->request->startWith("admin/rights-groups")));
        }
        if ($rights->can("action_log", "view", false)) {
            $navbar->addOne(new MenuItem("Tech Admin", "Action log",
                "tech-admin/action_log", "fas fa-eye",
                $this->request->startWith("tech-admin/action_log")));
        }
        if ($rights->can("deamon", "view", false)) {
            $navbar->addOne(new MenuItem("Tech Admin", "Cron Jobs",
                "tech-admin/deamon", "fas fa-magic",
                $this->request->startWith("tech-admin/deamon")));
        }
        if ($rights->can("tech_admin", "view", false)) {
            $navbar->addOne(new MenuItem("zDebug", "Rights",
                "debug/rights", "fas fa-bug",
                $this->request->startWith("tech-admin/rights")));
            $navbar->addOne(new MenuItem("zDebug", "PHP info",
                "debug/php_info", "fas fa-sitemap",
                $this->request->startWith("debug/php_info")));
            $navbar->addOne(new MenuItem("zDebug", "LDAP adopt",
                "gateway/ldap/adopt", "fas fa-sitemap",
                $this->request->startWith("gateway/ldap/adopt")));
        }
        if ($this->user->getRole() > 3) {
            $navbar->addOne(new MenuItem("Admin", "Departments",
                "admin/departments", "fas fa-building",
                $this->request->startWith("admin/departments")));
        }
        if ($rights->can("translations", "view", false)) {
            $navbar->addOne(new MenuItem("Tech Admin", "Translations",
                "tech-admin/translations", "fas fa-language",
                $this->request->startWith("tech-admin/translations")));
        }
        if ($rights->can("mail_tool", "view", false)) {
            $navbar->addOne(new MenuItem("Tech Admin", "Mail tool",
                "tech-admin/mail-tool", "fas fa-mail-bulk",
                $this->request->startWith("tech-admin/mail-tool")));
        }
    }

    private function buildSideBar()
    {
        $sidebar = new SideBar();
        //for test only
        $sidebar->addOne(new MenuItem("Profile", "My profile",
            "user/profile", "fas fa-id-card",
            $this->request->startWith("user/profile")));
        return $sidebar;
    }

    protected function addCounters(array $data)
    {
        foreach ($this->sidebar->getAll($this->navbar) as $sidebar) {
            foreach ($sidebar['items'] as $item) {
                if (isset($data[$item->category][$item->name])) {
                    $item->counter = $data[$item->category][$item->name]['count'];
                    $item->counter_color = $data[$item->category][$item->name]['color'];
                }
            }
        }
    }

    public function getSideBar()
    {
        if ($this->sidebar == null) {
            throw new Exception("SideBar has not been builded yet!");
        }
        return $this->sidebar->getAll($this->navbar);
    }

    public function getNavBar()
    {
        if ($this->navbar == null) {
            throw new Exception("NavBar has not been builded yet!");
        }
        return $this->navbar->getAll();
    }

}
