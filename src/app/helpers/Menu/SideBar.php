<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\menu;

/**
 * Description of SideBar
 *
 * @author jakub
 */
class SideBar {

    protected $list = array();

    function addOne(MenuItem $item) {
        $this->list[$item->category]['items'][] = $item;
        if ($item->is_active || !isset($this->list[$item->category]['active']))
            $this->list[$item->category]['active'] = $item->is_active;
    }

    function getAll(NavBar $navBar) {
        $var = array_merge_recursive($this->list, $navBar->getAll());
        ksort($var);
        foreach ($var as $key => $value) {
            $for_sort = $value['items'];
            usort($for_sort, '\JR\CORE\helpers\ArrayUtils::cmpByName');
            $var[$key]['items'] = $for_sort;
        }
        return $var;
    }

}
