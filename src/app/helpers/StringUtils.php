<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers;

/**
 * Description of StringUtils
 *
 * @author jakub
 */
class StringUtils
{

    public static function generate_string($lenght)
    {
        $chars = 'abcdefghijklmnpqstuvwxyz123456789ABCDEFGHIJKLMNPQSTUVWXYZ';
        $count = strlen($chars) - 1;
        $return = "";
        for ($i = 0; $i < $lenght; $i++) {
            $return = $return . $chars[mt_rand(0, $count)];
        }
        return $return;
    }

    public static function addPrefix($array, $prefix)
    {
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $data[$prefix . $key] = $value;
            }
            return $data;
        } else {
            throw new Exception("Not an array!");
        }
    }

    public static function anonimizeMail($mail)
    {

        $mail = explode("@", $mail);

        $mail[0] = str_split($mail[0]);
        if (strlen($mail[1]) > 10) {
            $mail[1] = str_split($mail[1]);
            for ($i = 2; $i < count($mail[1]) - 6; $i++) {
                $mail[1][$i] = "*";
            }
        }

        for ($i = 1; $i < count($mail[0]) - 2; $i++) {
            $mail[0][$i] = "*";
        }
        $mail[0] = implode("", $mail[0]);
        $mail[1] = implode("", $mail[1]);
        return implode("@", $mail);
    }

    public static function toCamleCase($string)
    {
        return str_replace('-', '', ucwords($string, '-'));
    }

    public static function time_ago($date, $use_short = false)
    {
        $is_valid = self::is_date_time_valid($date);
        if ($is_valid) {
            $timestamp = strtotime($date);
            $difference = time() - $timestamp;
            if ($use_short) {
                $periods = array("s", "m", "H", "D", "W", "M", "Y", "D");
            } else {
                $periods = array("sec", "min", "hour", "day", "week", "month", "year", "decade");
            }
            $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

            if ($difference >= 0) { // this was in the past time
                $ending = "ago";
            } else { // this was in the future time
                $difference = -$difference;
                $ending = "to go";
            }

            for ($j = 0; $difference >= $lengths[$j]; $j++) {
                if ($j > 6) {
                    break;
                }
                $difference /= $lengths[$j];
            }

            $difference = round($difference);

            if ($difference > 1 && !$use_short)
                $periods[$j] .= "s";

            $text = "$difference $periods[$j] " . ($use_short ? "" : $ending);

            return $text;
        } else {
            return 'Date Time must be in "yyyy-mm-dd hh:mm:ss" format';
        }
    }

    public static function is_date_time_valid($date)
    {

        if (date('Y-m-d H:i:s', strtotime($date)) == $date) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function generateCode($lenght)
    {
        $chars = '1234567890';
        $count = strlen($chars) - 1;
        $return = "";
        for ($i = 0; $i < $lenght; $i++) {
            $return = $return . $chars[mt_rand(0, $count)];
        }
        return $return;
    }

}
