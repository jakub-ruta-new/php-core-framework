<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers;

/**
 * Description of Assets
 *
 * @author jakub
 */
class Assets {

    protected $pathPrefix;
    protected $version;

    public function __construct($pathPrefix, $version) {
        $this->pathPrefix = $pathPrefix;
        $this->version = urlencode($version);
    }

    public function get($path) {
        return $this->pathPrefix . "/" . $path . "?v=" . $this->version;
    }

}
