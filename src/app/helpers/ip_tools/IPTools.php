<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\ip_tools;

/**
 * Description of IPTools
 *
 * @author jakub
 */
class IPTools {

    public function __construct() {
        $this->loadFile('app/config/ipBans.json');
    }

    public function checkIPBan($ip) {
        if (str_contains($ip, ":")) {
            return $this->checkIPv6Ban($ip);
        } elseif (str_contains($ip, ".")) {
            return $this->checkIPv4Ban($ip);
        } else {
            return true;
        }
    }

    protected function checkIPv6Ban($ip) {
        if (in_array($ip, $this->data['v6'])) {
            return true;
        }
        return false;
    }

    protected function checkIPv4Ban($ip) {
        if (in_array($ip, $this->data['v4'])) {
            return true;
        }
        return false;
    }

    private function loadFile($path) {
        if (file_exists($path)) {
            $this->data = json_decode(file_get_contents($path), true);
        } else {
            $this->data = array("v4" => array(), "v6" => array());
        }
    }

}
