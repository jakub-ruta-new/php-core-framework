<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\langs;

/**
 * Description of Lang
 *
 * @author jakub
 */
class Lang
{

    /**
     *
     * @var JR\CORE\helpers\langs\Lang
     */
    static $_instance;

    /**
     * Value that store locale from user profile
     * @var string
     */
    protected $preferedLang = 'en_EN';

    /**
     * List of availables languages for app
     * @var array code -> name
     */
    protected $availableLangs = array('en_EN' => 'English (international)');

    /**
     * Array of strings for translations
     * @var array
     */
    protected $strings = [];

    /**
     * array of missing strings in translations
     * @var array
     */
    protected $missing_strings = [];

    /**
     * Path to temp translations dir for _descruct
     * @var type
     */
    private $path = "";

    public function __construct()
    {
        \Tracy\Debugger::getBar()->addPanel(
            new \JR\CORE\bridges\tracy\TranslationsPanel($this));
        $this->loadLanguages();
        $this->setPath();
    }

    /**
     * Load Available languages from file
     */
    protected function loadLanguages()
    {
        if (file_exists("../app/temp/translations/langs.json")) {
            $this->availableLangs = json_decode(
                file_get_contents("../app/temp/translations/langs.json"), true);
        } else {
            if (!is_dir("../app/temp/translations")) {
                mkdir("../app/temp/translations", 0777, true);
            }
            file_put_contents("../app/temp/translations/langs.json",
                json_encode($this->availableLangs));
        }
    }

    private function setPath()
    {
        $this->path = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']);
    }

    /**
     * ONLY available if makeMeStatic was called before;
     * @return \JR\CORE\helpers\langs\Lang
     */
    static function gI()
    {
        if (self::$_instance == null)
            throw new \Exception("Object has no instance");
        return self::$_instance;
    }

    public function __destruct()
    {
        if (count($this->missing_strings) > 0) {
            $this->saveMissingStrings();
        }
    }

    public function saveMissingStrings()
    {
        $this->loadTranslations();
        file_put_contents($this->path . "../app/temp/translations/" . $this->preferedLang . ".json",
            json_encode(array_merge($this->strings, $this->missing_strings)));
    }

    public function loadTranslations()
    {
        if (file_exists("../app/temp/translations/" . $this->preferedLang . ".json")) {
            $this->strings = json_decode(
                file_get_contents("../app/temp/translations/" . $this->preferedLang . ".json"), true);
        }
        return $this;
    }

    /**
     * Will make this class static for inline function call from view
     */
    public function makeMeStatic()
    {
        self::$_instance = $this;
    }

    public function str($original, $vars = array())
    {
        if (count($this->strings) < 1) {
            $this->loadTranslations();
        }
        if (isset($this->strings[$original]) && strlen($this->strings[$original]) > 0 && $this->strings[$original] != "") {
            $string = $this->strings[$original];
        } else {
            $string = $original;
            $this->addMissing($original);
        }
        $string = $this->filterOutSystemVars($string);
        if (count($vars) > 0) {
            return $this->replaceVars($string, $vars);
        }
        return $string;
    }

    public function addMissing($original)
    {
        if (isset($this->missing_strings[$original]))
            return;
        if ($this->preferedLang == "en_EN") {
            $this->missing_strings[$original] = $original;
        } else {
            $this->missing_strings[$original] = null;
        }
    }

    public function filterOutSystemVars($string)
    {
        if ($pos = strpos($string, "|*")) {
            return substr($string, strpos($string, "|*") + 2);
        }
        return $string;
    }

    public function replaceVars($string, $vars)
    {
        foreach ($vars as $key => $value) {
            $string = str_replace("%" . $key . "%", $value, $string);
        }
        return $string;
    }

    /**
     * Return All supported lang in app
     * @return array of supported langs under key=>value
     */
    public function getLangs()
    {
        return $this->availableLangs;
    }

    /**
     * Set prefered locale
     * @param string locale
     */
    public function setPrefered($lang)
    {
        if (array_key_exists($lang, $this->availableLangs)) {
            $this->preferedLang = $lang;
            if (count($this->strings) > 0) {
                $this->loadTranslations();
            }
        }
    }

    /**
     *
     * @return string lang code
     */
    public function getSelectedLang()
    {
        return $this->preferedLang;
    }

    public function getAvailableLangs(): array
    {
        return $this->availableLangs;
    }

    public function getStrings(): array
    {
        return $this->strings;
    }

    public function getMissing_strings(): array
    {
        return $this->missing_strings;
    }

    public function update($strings)
    {
        $this->missing_strings = $strings;
        return;
        $i = file_put_contents($this->path . "../app/temp/translations/" . $this->preferedLang . ".json",
            json_encode($strings));
    }

    public function addLanguage($code, $name)
    {
        if ($name == "") {
            if ($code != "en_EN")
                unset($this->availableLangs[$code]);
        } else {
            $this->availableLangs[$code] = $name;
        }
        file_put_contents("../app/temp/translations/langs.json",
            json_encode($this->availableLangs));
        if ($name == "") {
            return;
        }
        $this->repair();
    }

    public function repair()
    {
        $tran = array();
        foreach ($this->availableLangs as $key => $value) {
            if (file_exists("../app/temp/translations/" . $key . ".json")) {
                $texts = json_decode(
                    file_get_contents("../app/temp/translations/" . $key . ".json"), true);
                $tran = array_merge($tran, $texts);
            }
        }
        foreach ($this->availableLangs as $key => $value) {
            if (file_exists("../app/temp/translations/" . $key . ".json")) {
                $saved = json_decode(
                    file_get_contents("../app/temp/translations/" . $key . ".json"), true);
                file_put_contents($this->path . "../app/temp/translations/" . $key . ".json",
                    json_encode($this->addMissingToArray($saved, $tran, $key)));
            } else {
                file_put_contents($this->path . "../app/temp/translations/" . $key . ".json",
                    json_encode($this->addMissingToArray(array(), $tran, $key)));
            }
        }
    }

    protected function addMissingToArray($saved, $tran, $code)
    {
        foreach ($tran as $key => $value) {
            if (isset($saved[$key]))
                continue;
            if ($code == "en_EN") {
                $saved[$key] = $key;
            } else {
                $saved[$key] = null;
            }
        }
        return $saved;
    }

}
