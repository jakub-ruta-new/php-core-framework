<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\dependencies;

use Curl\Curl;
use JR\CORE\bridges\tracy\CURLPanel;
use JR\CORE\bridges\tracy\OptionsPanel;
use JR\CORE\midleware\options\Options;
use Tracy\Debugger;

/**
 * Description of DependenciContainer
 *
 * @author jakub
 */
class DependenciContainer
{

    /**
     *
     * @var JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected static $_instance = null;

    /**
     *
     * @var array of user defined dependencies
     */
    protected $userDefined = array();

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $user;

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $admin = null;

    /**
     *
     * @var \JR\CORE\helpers\factories\users\UserFactory
     */
    protected $userFactory;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request = null;

    /**
     *
     * @var \JR\CORE\helpers\db\CustomMySqlLi
     */
    protected $db = null;
    protected $curl;
    protected ?CURLPanel $curlTracyPanel = null;

    /**
     *
     * @var \JR\CORE\helpers\session\SessionsUtils
     */
    protected $session;

    /**
     *
     * @var \JR\CORE\helpers\cookies\Cookies
     */
    protected $cookies;

    /**
     *
     * @var \JR\CORE\helpers\langs\Lang
     */
    protected $translations;

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $config;

    /**
     *
     * @var \JR\CORE\midleware\log\ActionLog
     */
    protected $actionLog;

    /**
     *
     * @var \JR\CORE\midleware\push\PushUtils
     */
    protected $pushUtils;

    /**
     *
     * @var \JR\CORE\midleware\notifications\Notifications
     */
    protected $notifications = null;

    /**
     *
     * @var \JR\CORE\helpers\mail\EmailTool
     */
    protected $email = null;

    public function __construct(\JR\CORE\helpers\db\CustomMySqlLi $db = null, \JR\CORE\request\request $request = null)
    {
        $this->request = $request;
        $this->db = $db;
        $this->getDB();
        $this->actionLog = new \JR\CORE\midleware\log\ActionLog($this->db, 1, $this->request ? $this->request->getUserIP() : null);
        if ($request != null) {
            $this->userFactory = new \JR\CORE\helpers\factories\users\UserFactory($this);
            $this->actionLog->setAdmin_level($this->user->getRole());
            $this->prepareTransaltions();
        }
        $this->makeMeStatic();
    }

    public function getDB()
    {
        if ($this->db == null) {
            $this->db = new \JR\CORE\helpers\db\CustomMySqlLi($_ENV["DB_HOST"],
                $_ENV["DB_USER"],
                $_ENV["DB_PASS"],
                $_ENV["DB_NAME"]
            );
            if (isset($_ENV["DB_prefix"])) {
                $this->db->setPrefix($_ENV["DB_prefix"]);
            }
            $this->db->setTrace(true);
            \Tracy\Debugger::getBar()->addPanel(
                new \JR\CORE\bridges\tracy\DatabasePanel($this->db));
        }
        return $this->db;
    }

    /**
     * Function for init translations
     */
    protected function prepareTransaltions()
    {
        $this->translations = new \JR\CORE\helpers\langs\Lang();
        $this->translations->makeMeStatic();
        if (isset($this->user)) {
            $this->translations->setPrefered($this->user->getLang());
        }
        if ($this->getSession()->getPreferedLang()) {
            $this->translations->setPrefered($this->getSession()->getPreferedLang());
        }
    }

    public function getSession()
    {
        if (isset($this->session))
            return $this->session;
        else {
            $this->session = new \JR\CORE\helpers\session\SessionsUtils($this->request);
            \Tracy\Debugger::getBar()->addPanel(
                new \JR\CORE\bridges\tracy\SessionPanel($this->session));
            return $this->session;
        }
    }

    public function makeMeStatic()
    {
        self::$_instance = $this;
    }

    /**
     * ONLY available if makeMeStatic was called before;
     * @return DependenciContainer
     */
    static function gI()
    {
        if (self::$_instance == null)
            throw new \Exception("Object has no instance");
        return self::$_instance;
    }

    public function getUserDefined($name)
    {
        if (isset($this->userDefined[$name])) {
            return $this->userDefined[$name];
        } else {
            $this->userDefined[$name] = new $name;
            return $this->userDefined[$name];
        }
    }

    public function getUser()
    {
        if (isset($this->user))
            return $this->user;
        throw new \Exception("User class has not been inited yet");
    }

    public function setUser(\JR\CORE\midleware\users\User $user)
    {
        $this->user = $user;
        $this->actionLog->setData($user->getInternalId(),
            $this->request->getUserIP(),
            $this->cookies->getUserSession(),
            ($this->admin != null ? "admin" : "normal"),
            ($this->admin != null ? $this->admin->getInternalId() : null));
    }

    public function getRequest()
    {
        if ($this->request == null) {
            $this->request = new \JR\CORE\request\request();
        }
        return $this->request;
    }

    public function getCurl()
    {
        throw new Exception("CURL was not registered");
    }

    public function getConfig()
    {
        if (isset($this->config)) {
            return $this->config;
        } else {
            $optionsUtils = new Options($this->db);
            $this->config = new \JR\CORE\config\Config($optionsUtils, $this);
            \Tracy\Debugger::getBar()->addPanel(
                new OptionsPanel($this->config));
            return $this->config;
        }
    }


    public function getUserFactory(): \JR\CORE\helpers\factories\users\UserFactory
    {
        if (isset($this->userFactory)) {
            return $this->userFactory;
        } else {
            $this->userFactory = new \JR\CORE\helpers\factories\users\UserFactory($this);
            return $this->userFactory;
        }
    }

    public function getAdmin()
    {
        return $this->admin;
    }

    public function setAdmin($user)
    {
        $this->admin = $user;
    }

    public function getCookies()
    {
        if (isset($this->cookies)) {
            return $this->cookies;
        } else {
            $this->cookies = new \JR\CORE\helpers\cookies\Cookies($this->request);
            return $this->cookies;
        }
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function getActionLog(): \JR\CORE\midleware\log\ActionLog
    {
        return $this->actionLog;
    }

    /**
     *
     * @return \JR\CORE\midleware\push\PushUtils
     */
    public function getPushUtils()
    {
        if (isset($this->pushUtils)) {
            return $this->pushUtils;
        } else {
            $this->pushUtils = new \JR\CORE\midleware\push\PushUtils($this->db);
            return $this->pushUtils;
        }
    }

    /**
     *
     * @return \JR\CORE\midleware\notifications\Notifications
     */
    public function getNotifications()
    {
        if (isset($this->notifications)) {
            return $this->notifications;
        } else {
            $this->notifications = new \JR\CORE\midleware\notifications\Notifications($this->db);
            return $this->notifications;
        }
    }

    /**
     *
     * @return \JR\CORE\helpers\mail\EmailTool
     */
    public function getEmailTool()
    {
        if (isset($this->email)) {
            return $this->email;
        } else {
            $this->email = new \JR\CORE\helpers\mail\EmailTool($this->db);
            return $this->email;
        }
    }

    /**
     *
     * @return \Curl\Curl
     */
    public function createCURL()
    {
        $curl = new Curl();
        if ($this->curlTracyPanel) {
            $this->curlTracyPanel->addCurl($curl);
        } else {
            $this->curlTracyPanel = new CURLPanel($curl);
            Debugger::getBar()->addPanel(
                $this->curlTracyPanel);
        }
        return $curl;
    }

}
