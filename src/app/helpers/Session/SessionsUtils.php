<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\session;

/**
 * Description of SessionsUtils
 *
 * @author jakub
 */
class SessionsUtils
{

    /**
     *
     * @var array copy of $_SESSION globalVar
     */
    protected $cache;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *  NOT WORKING
     * For comply with no set cookies utils user do action
     * @var bool Holds info if sessions already started
     */
    protected $started = 0;

    /**
     * pages that will not be shown in Last pages
     * @var array
     */
    private $hiddenPages = ['login', 'logout', 'auth', 'api',
        'cron', 'gateway', 'error', "password", "pass", "token", "reset",
        "create"];

    public function __construct(\JR\CORE\request\request $request)
    {
        $this->request = $request;
        $this->tryToLoad();
    }

    public function tryToLoad()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            $this->cache = $this->request->getSession()['app'];
        }
    }

    public function __destruct()
    {
        $_SESSION['app'] = $this->cache;
    }

    public function storeInTemp($key, $value)
    {
        $this->started = true;
        $this->cache['temp'][$key] = $value;
    }

    public function storeInSystem($key, $value)
    {
        $this->started = true;
        $this->cache['system'][$key] = $value;
    }

    public function setUserAdminId($admin_id)
    {
        $this->started = true;
        $this->cache['user']['admin_id'] = $admin_id;
        $this->cache['user']['admin_login_active'] = true;
    }

    public function disableAdminLogin()
    {
        $this->started = true;
        $this->cache['user']['admin_id'] = null;
        $this->cache['user']['admin_login_active'] = false;
    }

    public function setUserId($user_id, $expire)
    {
        $this->started = true;
        $this->cache['user']['user_id'] = $user_id;
        $this->cache['user']['session_expire'] = $expire;
    }

    public function getFromTemp($key)
    {
        if (isset($this->cache['temp'][$key]))
            return $this->cache['temp'][$key];
        return null;
    }

    public function getUserAdminId()
    {
        return $this->cache['user']['admin_id'];
    }

    public function IsAdminLoginActive()
    {
        if (!isset($this->cache['user']['admin_login_active'])) {
            return false;
        }
        return $this->cache['user']['admin_login_active'];
    }

    public function getUserId()
    {
        return $this->cache['user']['user_id'];
    }

    public function getSessionExpiryTime()
    {
        return $this->cache['user']['session_expire'];
    }

    public function getErrorMessages()
    {
        $data = $this->getFromSystem("error_messages");
        $this->setSystem("error_messages", array());
        return $data;
    }

    public function getFromSystem($key)
    {
        return $this->cache['system'][$key];
    }

    public function setSystem($key, $value, $add_to_array = false)
    {
        if ($add_to_array) {
            $this->cache['system'][$key][] = $value;
        } else {
            $this->cache['system'][$key] = $value;
        }

        $_SESSION['app'] = $this->cache;
    }

    public function setErrorMessages($message)
    {
        $this->setSystem("error_messages", $message, true);
    }

    public function getMessages()
    {
        $data = $this->getFromSystem("messages");
        $this->setSystem("messages", array());
        return $data;
    }

    /**
     *
     * @param string $message massage to be displaied, will not be translated
     * @param string $color = primary|danger|default|secondary|success
     * @param string|null $head bolt text before message
     * @param string|null $url url that will be displaied and clickable
     */
    public function setMessages($message, $color = 'primary', $head = null, $url = null)
    {
        $this->setSystem("messages", array("msg" => $message, "color" => $color, "head" => $head, "url" => $url), true);
    }

    public function checkCSRF($csrf)
    {
        if ($csrf != $this->getCSRFToken()) {
            throw new \JR\CORE\router\RedirectToError(403, "CSRF token is not valid. Try it again!");
        }
        return true;
    }

    public function getCSRFToken()
    {
        if (($this->getFromSystem("csrf") !== null)) {
            return $this->getFromSystem("csrf");
        } else {
            $token = \JR\CORE\helpers\StringUtils::generate_string(20);
            $this->setSystem("csrf", $token);
            return $token;
        }
    }

    public function getPreferedLang()
    {
        if (isset($this->cache['temp']['lang'])) {
            return $this->cache['temp']['lang'];
        }
        return null;
    }

    public function setLastPage($path)
    {
        foreach ($this->hiddenPages as $p) {
            if (str_contains($path, $p)) {
                return;
            }
        }
        for ($i = 4; $i >= 0; $i--) {
            if (isset($this->cache['temp']['lastPage'][$i])) {
                $this->cache['temp']['lastPage'][$i + 1] = $this->cache['temp']['lastPage'][$i];
            }
        }
        $this->cache['temp']['lastPage'][0] = $path;
    }

    public function getLastPages()
    {
        return $this->cache['temp']['lastPage'];
    }

}
