<?php

namespace JR\CORE\config;

use JR\CORE\helpers\dependencies\DependenciContainer;
use JR\CORE\helpers\form\FormFactory;
use JR\CORE\midleware\users\User;

class OptionsFormFactory
{
    /**
     * @var FormFactory
     */
    protected $form;
    /**
     * @var DependenciContainer
     */
    protected $dep;
    /**
     * @var User
     */
    protected $user;

    public function __construct(string $formName, string $csrf, DependenciContainer $dep)
    {
        $this->form = new FormFactory($formName, $csrf);
        $this->dep = $dep;
        $this->user = $dep->getUser();
    }

    public function createOption(array $options, bool $overideDisabled)
    {
        foreach ($options as $option) {
            if ($option['viewable_by'] > $this->user->getRole() && !$overideDisabled) {
                continue;
            }
            switch ($option['type']) {
                case 'bool' :
                    $control = $this->form->createCheckBox($option['category'] . '::' . $option['key'], ucfirst($option['category']) . ' : ' . $option['key']);
                    break;
                case 'select' :
                    $control = $this->form->createSelect($option['category'] . '::' . $option['key'], ucfirst($option['category']) . ' : ' . $option['key'])
                        ->setOptions(json_decode($option['options'], true)['options']);
                    break;
                default:
                case 'string':
                    $control = $this->form->createTextInput($option['category'] . '::' . $option['key'], ucfirst($option['category']) . ' : ' . $option['key'] . '');
                    break;
            }
            $control->value($option['value']);
            $control->setText($option['description']);
            if ($option['is_feature_flag'] && $this->user->getRole() < 9) {
                $control->setDisabledFunc();
            }
            if ($option['editable_by'] > $this->user->getRole() && !$overideDisabled) {
                $control->setDisabledFunc();
            }
            if ($this->user->getRole() < 10 && $option['is_global'] == 0 && !$overideDisabled) {
                $control->setDisabledFunc();
            }
        }
        $this->form->createButton('save', 'Save');
    }

    public function getForm()
    {
        return $this->form;
    }
}