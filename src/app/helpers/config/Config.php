<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\config;

use JR\CORE\helpers\dependencies\DependenciContainer;
use JR\CORE\midleware\options\Options;

/**
 * Description of Config
 *
 * @author jakub
 */
class Config
{

    protected $config = [];
    protected $featureFlags = [];
    protected $version = "unknown";
    protected $core_version = "unknown";
    protected $is_loaded = false;

    /**
     * @var Options
     */
    protected $options = null;
    /**
     * @var DependenciContainer
     */
    protected $dep = null;

    public function __construct(Options $options, DependenciContainer $dep)
    {
        $this->loadVersion();
        $this->options = $options;
        if ($this->get('ENVIRONMENT') === 'dev') {
            $this->regenerateFile();
        }
        $this->loadFromFile();
        $this->dep = $dep;
    }

    public function loadVersion()
    {
        if (file_exists("../version")) {
            $this->version = file_get_contents("../version");
        }
        if (file_exists(__DIR__ . "/../../../../version")) {
            $this->core_version = file_get_contents(__DIR__ . "/../../../../version");
        }
    }

    public function get($key, $cat = "ENV")
    {
        if ($cat == "ENV") {
            return $_ENV[$key];
        }
        return $this->getConfig($key, $cat);
    }

    protected function getConfig($key, $cat)
    {
        if ($this->is_loaded) {
            if (isset($this->config[$cat][$key])) {
                return $this->config[$cat][$key];
            }
        } else {
            $this->loadFromFile();
        }
    }

    protected function loadFromFile()
    {
        if (file_exists('../app/temp/cache/options.json')) {
            $this->loadFile();
        } else {
            $this->regenerateFile();
        }
    }

    protected function loadFile()
    {
        $data = file_get_contents('../app/temp/cache/options.json');
        $data = json_decode($data, true);
        $this->featureFlags = $data['feature_flags'];
        $this->config = $data['config'];
        $this->is_loaded = true;
    }

    public function regenerateFile()
    {
        $data = [];
        try {
            $featureFlags = $this->options->getFeatureFlags();
            foreach ($featureFlags as $flag) {
                $data['feature_flags'][$flag['key']] = $flag['value'];
            }
            $options = $this->options->getOptions();
            foreach ($options as $option) {
                $data['config'][$option['category']][$option['key']] = $option['value'];
            }
        } catch (\Exception|\mysqli_sql_exception $ex) {
            //do nothing - just skip;
        }
        @mkdir('../app/temp/cache/', 0777, true);
        file_put_contents('../app/temp/cache/options.json', json_encode($data));
    }

    /**
     * @return Options
     */
    public function getOptions(): ?Options
    {
        return $this->options;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getCoreVersion()
    {
        return $this->core_version . ' (' . substr(\Composer\InstalledVersions::getRawData()['versions']['jr/php_new_core']['reference'], 0, 7) . ')';
    }

    public function getTitle()
    {
        return $_ENV["APP_NAME"];
    }

    public function getBool($key, $cat = "ENV")
    {
        throw new \Exception("This part of code doesn´t work and still return false even if it is true");
        if ($cat == "ENV") {
            return boolval($_ENV[$key]);
        }
        return boolval($this->config[$cat][$key]);
    }

    public function getLogo()
    {
        return $_ENV["APP_LOGO"];
    }

    public function getFeatureFlag($key)
    {
        if (isset($this->featureFlags[$key])) {
            return $this->featureFlags[$key];
        }
        throw new FeatureFlagNotExistsException($key);
    }

    public function setOption($category, $key, $value)
    {
        $this->options->insertOrUpdate(['category' => $category, 'key' => $key, 'value' => $value, 'updated_by' => $this->dep->getUser()->getInternalId()]);
        $this->regenerateFile();
        $this->loadFile();
    }

    public function createForm($options, $overideDisabled = false)
    {
        $factory = new OptionsFormFactory('options-change', $this->dep->getSession()->getCSRFToken(), $this->dep);
        $factory->createOption($options, $overideDisabled);
        return $factory->getForm();
    }

    /**
     * @return array
     */
    public function getFeatureFlags(): array
    {
        return $this->featureFlags;
    }

    public function getConfigs(): array
    {
        return $this->config;
    }


}
