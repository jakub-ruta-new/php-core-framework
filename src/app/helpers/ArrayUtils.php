<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers;

/**
 * Description of ArrayUtils
 *
 * @author jakub
 */
class ArrayUtils {

    public static function createTreeFromArrayAndAddToEnd($array, $end) {
        $new_array = array();
        $val = array_pop($array);
        if (count($array) > 0) {
            $data = self::createTreeFromArrayAndAddToEnd($array, $end);
        } else {
            $data = $end;
        }
        $new_array[$val] = $data;
        return $new_array;
    }

    /**
     * Compare two object via its public name
     * @param object $a with public name
     * @param object $b with public name
     * @return type
     */
    static function cmpByName($a, $b) {
        return strcmp($a->name, $b->name);
    }

    public static function makeCategoryArray($array, $key) {
        $new = array();
        foreach ($array as $u) {
            $new[$u[$key]][] = $u;
        }
        return $new;
    }

}
