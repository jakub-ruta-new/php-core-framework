<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormTextArea
 *
 * @author jakub
 */
class FormTextArea extends FormControl
{

    protected $placeholder = null;

    //put your code here
    public function render()
    {
        $html = '';
        if ($this->renderGroupStart) {
            $html .= $this->renderGroupStart();
        }
        if ($this->label)
            $html .= $this->renderLabel();
        $html .= '<textarea class="' . $this->class . ' ' . $this->margin . '" id="' . $this->id . '"
                    name="' . $this->name . '"';
        if ($this->placeholder)
            $html .= ' placeholder="' . $this->placeholder . '" ';
        if ($this->required)
            $html .= ' required="required" ';
        if ($this->disabledFunc)
            $html .= ' disabled" ';
        if ($this->readonly)
            $html .= ' readonly" ';
        $html .= ($this->disabledFunc ? " disabled " : "") . '>';
        if ($this->value)
            $html .= $this->value;
        $html .= '</textarea>';
        if ($this->text)
            $html .= $this->renderFormText();
        if ($this->renderGroupEnd) {
            $html .= $this->renderGroupEnd();
        }
        return $html;
    }

    public function placeholder($pl)
    {
        $this->placeholder = $pl;
        return $this;
    }

    public function evaluate()
    {
        if (!isset($_REQUEST[$this->name])) {
            throw new FormException("missing input");
        }
    }

}
