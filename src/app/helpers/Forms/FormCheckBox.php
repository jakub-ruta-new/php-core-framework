<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormCheckBox
 *
 * @author jakub
 */
class FormCheckBox extends FormControl
{

    protected $class = "custom-checkbox";

    public function __construct($name, $label, $value)
    {
        $this->name = $name;
        $this->id = $name;
        $this->label = $label;
        if ($_REQUEST['csrf']) {
            if ($_REQUEST[$this->name] == "on") {
                $this->value = true;
            } else {
                $this->value = false;
            }
        } else if ($value) {
            $this->value = $value;
        }
    }

    //put your code here
    public function render()
    {
        $html = '';
        if ($this->renderGroupStart) {
            $html .= $this->renderGroupStart();
        }
        $html .= '<div class="' . $this->class . ' ' . $this->margin . '">';
        $html .= '<input type="checkbox" id="' . $this->id . '"'
            . ' name="' . $this->name . '" ';
        if ($this->value)
            $html .= ' checked="checked" ';
        if ($this->readonly)
            $html .= ' readonly" ';
        $html .= ($this->disabledFunc ? " disabled " : "") . '>';
        if ($this->label)
            $html .= $this->renderLabel();
        if ($this->text)
            $html .= $this->renderFormText();
        $html .= '</div>';
        if ($this->renderGroupEnd) {
            $html .= $this->renderGroupEnd();
        }
        return $html;
    }

    protected function renderLabel()
    {
        return '
                <label for="' . $this->id . '" '
            . '><b>' . $this->label . '</b></label>';
    }

    public function evaluate()
    {
        if ($_REQUEST[$this->name] == 'on') {
            return true;
        }
        return false;
    }
}
