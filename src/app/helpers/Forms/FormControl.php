<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

use JR\CORE\helpers\langs\Lang;
use JR\CORE\helpers\StringUtils;

/**
 *
 * @author jakub
 */
abstract class FormControl
{

    protected $required = false;
    protected $name;
    protected $id;
    protected $onclick = null;
    protected $label;
    protected $class = 'form-control';
    protected $prepend = null;
    protected $append = null;
    protected $value = null;
    protected $settedValue = null;
    protected $margin = "my-10";
    protected $text = null;
    protected $translate = true;
    protected $renderGroupStart = false;
    protected $renderGroupEnd = false;

    /**
     * If control should render as disabled
     * @var bool
     */
    protected $disabledFunc = false;
    protected $readonly = false;

    public function __construct(string $name, string $label, string $value = null)
    {
        $this->name = $name;
        $this->id = $name;
        $this->label = $label;
        if (isset($_REQUEST[$this->name])) {
            $this->value = $_REQUEST[$this->name];
        } else if ($value) {
            $this->value = $value;
        }
    }

    public function setText($text): void
    {
        $this->text = $text;
    }

    public function setPrepend($prepend)
    {
        $this->prepend = $prepend;
        return $this;
    }

    public function setMargin($m)
    {
        $this->margin = $m;
        return $this;
    }

    public function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    public function required($param = true)
    {
        $this->required = $param;
        return $this;
    }

    public function value($param = true, $override = false)
    {
        $this->settedValue = $param;
        if ($this->value === null || $this->value === '' || $override) {

            $this->value = $param;
        }
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function id($name)
    {
        $this->id = $name;
        return $this;
    }

    public function onClick($param)
    {
        $this->onclick = $param;
        return $this;
    }

    public function Class($class)
    {
        $this->class = $class;
        return $this;
    }

    abstract public function render();

    public function getValue()
    {
        return $this->value;
    }

    public function readonly()
    {
        $this->readonly = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabledFunc(): bool
    {
        return $this->disabledFunc;
    }

    public function setDisabledFunc($disabled = true): void
    {
        $this->disabledFunc = $disabled;
    }

    abstract function evaluate();

    /**
     * @return null
     */
    public function getSettedValue()
    {
        return $this->settedValue;
    }

    /**
     * @param bool $translate
     */
    public function setTranslate(bool $translate): FormControl
    {
        $this->translate = $translate;
        return $this;
    }

    protected function renderLabel()
    {
        return '
                <label for="' . $this->id . '" '
        . 'class="' . ($this->required ? " required " : "") . '"'
        . '><b>' . $this->translate ? Lang::gI()->str($this->label) : $this->label . ':</b></label>';
    }

    protected function renderInputGroupStart()
    {
        return ' <div class="input-group">';
    }

    protected function renderInputGroupEnd()
    {
        return ' </div>';
    }

    protected function renderFormText()
    {
        return ' <div class="form-text">
      ' . $this->text . '
    </div>';
    }

    protected function renderPrepend()
    {
        return '<div class="input-group-prepend ' . $this->margin . '">
        <span class="input-group-text">' . $this->prepend . ' </span>
      </div>';
    }

    protected function renderAppend()
    {
        return '<div class="input-group-append ' . $this->margin . '">
        <span class="input-group-text">' . $this->append . ' </span>
      </div>';
    }

    protected function renderGroupStart()
    {
        return '<div class="form-group mb-0">';
    }

    protected function renderGroupEnd()
    {
        return '</div>';
    }

    /**
     * @param bool $renderGroupStart
     */
    public function setRenderGroupStart(bool $renderGroupStart = true): self
    {
        $this->renderGroupStart = $renderGroupStart;
        return $this;
    }

    /**
     * @param bool $renderGroupEnd
     */
    public function setRenderGroupEnd(bool $renderGroupEnd = true): self
    {
        $this->renderGroupEnd = $renderGroupEnd;
        return $this;
    }


}
