<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormNumberInput
 *
 * @author jakub
 */
class FormNumberInput extends FormControl
{

    protected $min = null;
    protected $max = null;
    protected $step = null;

    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }

    public function render()
    {
        $html = '';
        if ($this->renderGroupStart) {
            $html .= $this->renderGroupStart();
        }
        if ($this->label)
            $html .= $this->renderLabel();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupStart();
        if ($this->prepend)
            $html .= $this->renderPrepend();
        $html .= '<input type="number" name="' . $this->name
            . '" id="' . $this->id
            . '" class="' . $this->class . " " . $this->margin
            . '" value="' . $this->value . '" '
            . ($this->disabledFunc ? " disabled " : "");
        if ($this->max)
            $html .= ' max="' . $this->max . '" ';
        if ($this->disabledFunc)
            $html .= ' disabled" ';
        if ($this->readonly)
            $html .= ' readonly" ';
        if ($this->min)
            $html .= ' min="' . $this->min . '" ';
        if ($this->step)
            $html .= ' step="' . $this->step . '" ';
        $html .= '/>';
        if ($this->append)
            $html .= $this->renderAppend();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupEnd();
        if ($this->text)
            $html .= $this->renderFormText();
        if ($this->renderGroupEnd) {
            $html .= $this->renderGroupEnd();
        }
        return $html;
    }

    public function evaluate()
    {
        if (!isset($_REQUEST[$this->name]) && !$this->disabledFunc) {
            throw new FormException("missing input");
        }
        /* if ($this->min) {
          if ((float) $_REQUEST[$this->name] > (float) $this->min) {
          throw new FormException("Number must be higher than " . $this->min);
          }
          }
          if ($this->max) {
          if ((float) $_REQUEST[$this->name] < (float) $this->max) {
          throw new FormException("Number must be lower than " . $this->max);
          }
          }
          if (!is_numeric($_REQUEST[$this->name])) {
          throw new FormException("NaN");
          } */
    }

}
