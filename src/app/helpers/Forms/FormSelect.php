<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

use JR\CORE\helpers\langs\Lang;

/**
 * Description of FormSelect
 *
 * @author jakub
 */
class FormSelect extends FormControl
{

    protected $options = array();
    protected $hidden = array();
    protected $disabled = array();
    protected $onchange = null;
    protected $vissible = array();
    protected $vissible_bool = false;
    protected $selectMe = false;
    protected $selectAll = false;

    public function setOptions($options, $key = null, $value = null)
    {
        if ($key != null) {
            foreach ($options as $opt) {
                $this->options[$opt[$key]] = $opt[$value];
            }
        } else {
            $this->options = $options;
        }
        return $this;
    }

    public function setSelected($selected, $override = false)
    {
        $this->value($selected, $override);
        return $this;
    }

    public function setOnChange($action)
    {
        $this->onchange = $action;
        return $this;
    }

    /**
     * For disable some options
     * @param array $disabled array of keys
     * @return $this
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * For hide some values unless checked
     * @param array $hidden array of keys
     * @return $this
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }

    public function setVissible($vissible, $key = null, $value = null)
    {
        $this->vissible_bool = true;
        if ($key != null) {
            foreach ($vissible as $vis) {
                $this->vissible[$vis[$key]] = $vis[$value];
            }
        } else {
            $this->vissible = $vissible;
        }
        return $this;
    }

    public function render()
    {
        if ($this->vissible_bool)
            $this->convertVissible();
        $html = '';
        if ($this->renderGroupStart) {
            $html .= $this->renderGroupStart();
        }
        if ($this->label)
            $html .= $this->renderLabel();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupStart();
        if ($this->prepend)
            $html .= $this->renderPrepend();
        $html .= $this->renderSelectStart();
        if ($this->selectMe) {
            $html .= $this->renderSelectMe($this->selectMe);
        }
        if ($this->selectAll) {
            $html .= $this->renderSelectAll($this->selectAll);
        }
        foreach ($this->options as $key => $value) {
            $html .= $this->renderOption($key, $value);
        }
        $html .= $this->renderSelectEnd();
        if ($this->append)
            $html .= $this->renderAppend();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupEnd();
        if ($this->text)
            $html .= $this->renderFormText();
        if ($this->renderGroupEnd) {
            $html .= $this->renderGroupEnd();
        }
        return $html;
    }

    public function convertVissible()
    {
        foreach ($this->options as $key => $value) {
            if ($this->vissible[$key]) {
                continue;
            } else {
                $this->hidden[] = $key;
            }
        }
    }

    protected function renderSelectStart()
    {
        return '<select class="' . $this->class . ' ' . $this->margin . '" '
            . 'id="' . $this->id . '" '
            . '' . ($this->required ? ' required="required" ' : "") . ''
            . '' . ($this->readonly ? ' readonly="readonly" ' : "") . ''
            . '' . ($this->onchange ? 'onchange="' . $this->onchange . '"' : "") . ''
            . '' . ($this->name ? 'name="' . $this->name . '"' : "") . ''
            . ($this->disabledFunc ? " disabled " : "") . '>';
    }

    protected function renderSelectMe($text)
    {
        $html = '<option selected="selected" disabled="disabled">' . Lang::gI()->str($text) . '</option>';
        return $html;
    }

    protected function renderSelectAll($text)
    {
        return $this->renderOption($text, '-All-', false);
    }

    protected function renderOption($key, $value, $ucFirst = true)
    {
        $html = '';
        $hidden = (in_array($key, $this->hidden) ? true : false);
        $disabled = (in_array($key, $this->disabled) ? true : false);
        $selected = ($key == $this->value ? true : false);
        if (!$hidden || $selected) {
            $html .= '<option value="' . $key . '"';
            if ($selected)
                $html .= ' selected="selected" ';
            if ($disabled)
                $html .= 'disabled="disabled" ';
            $html .= '>';
            $html .= $ucFirst ? ucfirst(($this->translate ? Lang::gI()->str($value) : $value)) : ($this->translate ? Lang::gI()->str($value) : $value);
            $html .= '</option>';
        }
        return $html;
    }

    protected function renderSelectEnd()
    {
        return '
            </select>
            ';
    }

    public function evaluate()
    {
        if (!isset($_REQUEST[$this->name]) && $this->required) {
            throw new FormException("missing input");
        }
        if (!array_key_exists($_REQUEST[$this->name], $this->options) && $this->required) {
            throw new FormException("Option is not from options");
        }
    }

    /**
     * @param string $selectMe
     * @return
     */
    public function setSelectMe(string $selectMe): FormSelect
    {
        $this->selectMe = $selectMe;
        return $this;
    }

    /**
     * @param string $selectAll
     */
    public function setSelectAll(string $selectAll): FormSelect
    {
        $this->selectAll = $selectAll;
        return $this;
    }


}
