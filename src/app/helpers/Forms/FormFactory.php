<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

use JR\CORE\helpers\dependencies\DependenciContainer;
use JR\CORE\helpers\session\SessionsUtils;
use JR\CORE\midleware\sessions\Session;

/**
 * Description of FormFactory
 *
 * @author jakub
 */
class FormFactory
{

    /**
     *
     * @var class FormControl
     */
    protected $controls = array();

    /**
     *
     * @var string Name of form
     */
    protected $form_name;

    /**
     *
     * @var string Form method GET/POST
     */
    protected $method = "POST";

    /**
     *
     * @var string defeult action for form
     */
    protected $action = null;

    /**
     *
     * @var string class of form form/form-inline
     */
    protected $form_class = "form";

    /**
     *
     * @var bool is form sended?
     */
    protected $sended = false;
    /**
     * @var bool if form should persist page reload
     */
    protected bool $persist = false;
    /**
     * @var array|null form data in session
     */
    protected ?array $dataInSession = null;

    protected SessionsUtils $session;


    /**
     *
     * @var string CSRF token
     */
    protected $csrf;

    /**
     *
     * @param string $form_name
     * @param string $csrf
     * @return FormFactory
     */
    public function __construct($form_name, $csrf, $persist = false)
    {
        $this->form_name = $form_name;
        $this->csrf = $csrf;
        $this->session = DependenciContainer::gI()->getSession();
        $this->createHidden("csrf", "test")->value($csrf, true);
        $this->createHidden("form-name", $form_name)->value($form_name, true);
        if (isset($_REQUEST['csrf']) && isset($_REQUEST['form-name']) && $form_name == $_REQUEST['form-name']) {
            $this->sended = true;
            $this->dataInSession = null;
            $this->session->storeInTemp('form-' . $this->form_name, null);
        }
        $this->persist = $persist;
        $dataInSession = $this->session->getFromTemp('form-' . $this->form_name);
        if ($dataInSession) {
            $this->dataInSession = $dataInSession;
            $this->sended = true;
        }
        return $this;
    }

    /**
     *
     * @param string $name
     * @param type $value
     * @return FormHidden
     */
    public function createHidden($name, $value)
    {
        return $this->controls[$name] = new FormHidden($name, $value);
    }

    public function getForm_name()
    {
        return $this->form_name;
    }

    public function setForm_name($form_name)
    {
        $this->form_name = $form_name;
        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function getForm_class()
    {
        return $this->form_class;
    }

    /**
     *
     * @param type $form_class
     * @return $this
     */
    public function setForm_class($form_class)
    {
        $this->form_class = $form_class;
        return $this;
    }

    /**
     * Get one already created control
     * @param string $name
     * @return FormControl
     */
    public function getControl($name)
    {
        return $this->controls[$name];
    }

    /**
     *
     * @param string $name
     * @return FormSelect
     */
    public function createSelect($name, $label)
    {
        $value = null;
        if ($this->persist && $this->dataInSession) {
            $value = $this->dataInSession[$name];
        }
        return $this->controls[$name] = new FormSelect($name, $label, $value);
    }

    /**
     *
     * @param string $name
     * @return FormTextArea
     */
    public function createTextArea($name, $label)
    {
        $value = null;
        if ($this->persist && $this->dataInSession) {
            $value = $this->dataInSession[$name];
        }
        return $this->controls[$name] = new FormTextArea($name, $label, $value);
    }

    /**
     *
     * @param string $name
     * @return FormBUtton
     */
    public function createButton($name, $value)
    {
        /*if ($this->persist && $this->dataInSession) {
            $value = $this->dataInSession[$name];
        }*/
        return $this->controls[$name] = new FormButton($name, $value);
    }

    /**
     *
     * @param string $name
     * @return FormCheckBox
     */
    public function createCheckBox($name, $label)
    {
        $value = null;
        if ($this->persist && $this->dataInSession) {
            $value = $this->dataInSession[$name];
        }
        return $this->controls[$name] = new FormCheckBox($name, $label, $value);
    }

    /**
     *
     * @param string $name
     * @param string $label
     * @return FormNumberInput
     */
    public function createNumberInput($name, $label)
    {
        $value = null;
        if ($this->persist && $this->dataInSession) {
            $value = $this->dataInSession[$name];
        }
        return $this->controls[$name] = new FormNumberInput($name, $label, $value);
    }

    /**
     *
     * @param string $name
     * @param string $label
     * @return FormTextInput
     */
    public function createTextInput($name, $label)
    {
        $value = null;
        if ($this->persist && $this->dataInSession && str_contains($name, 'password')) {
            $value = 'PII contains';
        }
        return $this->controls[$name] = new FormTextInput($name, $label, $value);
    }

    /**
     * Render all form
     * @return string HTML code
     */
    public function renderAll()
    {
        $html = $this->renderFormStart();
        foreach ($this->controls as $cont) {
            $html .= $cont->render();
        }
        $html .= $this->rederFormEnd();
        return $html;
    }

    /**
     * Render start of form, without any control
     * @return string
     */
    protected function renderFormStart()
    {
        $html = '<form method="' . $this->method . '" '
            . 'class="' . $this->form_class . '" '
            . 'name="' . $this->form_name . '" '
            . 'id="' . $this->form_name . '" ';
        if ($this->action) {
            $html .= 'action="' . $this->action . '" ';
        }
        $html .= ">";
        return $html;
    }

    /**
     * Render form end
     * @return string
     */
    public function rederFormEnd()
    {
        return '</form>';
    }

    public function isSend()
    {
        if ($this->sended) {
            if ($this->persist) {
                $this->session->storeInTemp('form-' . $this->form_name, $this->getValues(true));
                return true;
            }
            $this->evaluate();
            return true;
        }
        return false;
    }

    public function getValues($hidePII = false)
    {
        $data = array();
        foreach ($this->controls as $key => $value) {

            if ($this->dataInSession && $hidePII && str_contains($value->getName(), 'pass')) {
                if (str_contains($value->getName(), 'password')) {
                    $data[$value->getName()] = 'hidden';
                } else {
                    $data[$value->getName()] = $value->getValue();
                }
            } else if (!$this->dataInSession && $key == 'csrf') {
                if ($this->sended) {
                    if ($_REQUEST['csrf'] != $this->csrf) {
                        bdump($this);
                        throw new CSRFViolation();
                    }
                }
            } elseif ($value instanceof FormButton) {
                continue;
            } elseif ($key == 'form-name') {
                continue;
            } elseif ($value->isDisabledFunc()) {
                continue;
            } else {
                $data[$value->getName()] = $value->getValue();
            }
        }
        return $data;
    }

    public function evaluate()
    {
        foreach ($this->controls as $key => $value) {
            $value->evaluate();
        }
    }

    public function setDisabled($names = array())
    {
        if (count($names) == 0) {
            foreach ($this->controls as $value) {
                $value->setDisabledFunc();
            }
        } else {
            foreach ($names as $value) {
                $this->controls[$value]->setDisabledFunc();
            }
        }
    }

    public function vulues($data, $override = false)
    {
        foreach ($data as $key => $value) {
            if ($this->controls[$key]) {
                $this->controls[$key]->value($value, $override);
            }
        }
    }

    public function getValue($key)
    {
        return $this->controls[$key]->getValue();
    }

    public function getChanged()
    {
        $data = array();
        foreach ($this->controls as $key => $value) {
            assert($value instanceof FormControl);
            if ($value->getValue() !== $value->getSettedValue()) {
                $data[$key] = ['old' => $value->getSettedValue(), 'new' => $value->getValue()];
            }
        }
        return $data;
    }


}
