<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormButton
 *
 * @author jakub
 */
class FormButton extends FormControl
{

    protected $class = 'form-control btn';

    //put your code here

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
        $this->id = $name;
        $this->label = null;
    }

    public function getValue()
    {
        if (isset($_REQUEST[$this->name])) {
            return $_REQUEST[$this->name];
        }
        return null;
    }


    public function render()
    {
        $str = '';
        if ($this->renderGroupStart) {
            $str .= $this->renderGroupStart();
        }
        $str .= '<input class="' . $this->class . ' ' . $this->margin . '" '
            . ' type="submit" value="' . $this->value . '" '
            . 'name="' . $this->name . '" '
            . '' . ($this->readonly ? ' readonly="readonly" ' : "") . ''
            . 'id="' . $this->id . '" '
            . ($this->disabledFunc ? " disabled " : "") . '>';
        if ($this->renderGroupEnd) {
            $str .= $this->renderGroupEnd();
        }
        return $str;
    }


    public function evaluate()
    {
        return;
    }

}
