<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\gateway\ldap;

use \JR\CORE\midlewares\apiTokens\ApiTokens;

/**
 * Description of LDAP
 *
 * @author jakub
 */
class LDAP {

    /**
     *
     * @var string Adopt URL in LDAP
     */
    protected $url;

    /**
     *
     * @var string
     */
    protected $ldap_base_url;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    /**
     *
     * @var \JR\CORE\midlewares\apiTokens\ApiTokens
     */
    protected $api_tokens;

    function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        $this->url = $_ENV['LDAP_URL'] . "api/v1/ldap/app/adopt";
        $this->ldap_base_url = $_ENV['LDAP_URL'] . "api/v1/ldap/";
        $this->dep = $dep;
        $this->api_tokens = new ApiTokens($dep->getDB());
    }

    /**
     * Function for adopt app to LDAP -> create tokens
     * @param type $token
     * @throws \Exception
     */
    public function adoptLDAP($token) {
        $curl = $this->dep->createCURL();
        $data = [
            'app_name' => $_ENV['APP_NAME'],
            'app_code' => $_ENV['APP_CODE'],
            'token' => $token,
            'url' => $this->dep->getRequest()->getBasePath(),
            'login_url' => $this->dep->getRequest()->getBasePath() . "/login/ldap/",
        ];
        $data = $curl->post($this->url, $data);
        if (isset($data->msg_error)) {
            throw new \Exception(json_encode($data));
        }
        if (!isset($data->data->tokens->token)) {
            throw new \Exception("Missing token");
        }
        $this->createToken($data->data->tokens->token);
        $this->createTokens();
    }

    /**
     * Store token in DB from LDAP
     * @param array $tokens
     */
    public function createToken($tokens) {
        $data = [
            'app_code' => 'ldap',
            'token' => $tokens->token,
            'expire' => $tokens->expire,
            'auto_renew' => 0,
            'type' => 'recovery',
        ];
        $this->api_tokens->insert($data);
    }

    /**
     * Create tokens for app in LDAP
     */
    public function createTokens() {
        $this->api_tokens->renew('setup', 'ldap', $this->dep->createCURL());
        $this->api_tokens->renew('get', 'ldap', $this->dep->createCURL());
        $this->api_tokens->renew('set', 'ldap', $this->dep->createCURL());
        $this->api_tokens->renew('send', 'ldap', $this->dep->createCURL());
    }

    public function tryLogin($login_token) {
        $token = $this->api_tokens->getToken("get", "ldap");
        $curl = $this->dep->createCURL();
        $data = [
            'user_token' => $login_token,
            'app_token' => $token['token'],
            'app_code' => $_ENV['APP_CODE'],
        ];
        bdump($data);
        $response = $curl->post($this->ldap_base_url . "app/login", $data);
        bdump($response);
        if ($curl->httpStatusCode != 200) {
            throw new LDAPNotResponnding();
        }
        $this->checkLDAPLoginRsponse($response);
        $this->loginUserViaLDAP($response);
    }

    protected function checkLDAPLoginRsponse($response) {
        if (isset($response->msg_error)) {
            if ($response->msg_error === 'token_expired') {
                throw new TokenExpired();
            } else if ($response->msg_error === 'missing_rights') {
                throw new MissingRights();
            } else {
                throw new \Exception(json_encode($response));
            }
        }
    }

    public function loginUserViaLDAP($response) {
        bdump($response);
        $this->dep->getUser()->departments->updateFromLDAP(
                $response->data->user->all_departments
        );
        $this->dep->getUserFactory()->tryLDAPLogin(
                $response->data->user->user,
                isset($response->data->admin_login) ? $response->data->admin_login : null
        );
        $this->dep->getUser()->departments->updateUserDepartmentsFromLDAP(
                $response->data->user->departements
        );
        $this->dep->getUser()->setRole($response->data->user->user->role);
        $this->dep->getUser()->enable();
    }

    public function getApps() {
        $token = $this->api_tokens->getToken("get", "ldap");
        $curl = $this->dep->createCURL();
        $data = [
            'app_code' => $_ENV['APP_CODE'],
            'app_token' => $token['token'],
            'user_id' => $this->dep->getUser()->getPortalId(),
        ];
        $data = $curl->post($this->ldap_base_url . 'app/apps', $data);
        if (isset($data->msg_error)) {
            throw new \Exception(json_encode($data));
        }
        return $data->data;
    }

    public function logOutEveryWhere($portal_id) {
        $token = $this->api_tokens->getToken("set", "ldap");
        $curl = $this->dep->createCURL();
        $data = [
            'app_code' => $_ENV['APP_CODE'],
            'app_token' => $token['token'],
            'user_id' => $portal_id,
        ];
        $data = $curl->post($this->ldap_base_url . 'user/logout', $data);
        if (isset($data->msg_error)) {
            throw new \Exception(json_encode($data));
        }
    }

}
