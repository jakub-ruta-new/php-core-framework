<?php

namespace JR\CORE\helpers\gateway\push;

use Curl\Curl;
use JR\CORE\helpers\dependencies\DependenciContainer;
use JR\CORE\helpers\gateway\interface\push\PushInterface;
use JR\CORE\midleware\push\PushUtils;

class Onesignal implements PushInterface
{
    /**
     * @var DependenciContainer
     */
    protected $dep;
    protected $apiKey = null;
    /**
     * @var PushUtils
     */
    protected $utils;
    /**
     * @var Curl
     */
    protected $curl;

    public function __construct(DependenciContainer $dep)
    {
        $this->dep = $dep;
        $this->apiKey = $_ENV['oneSignalAPIKey'];
        $this->utils = $dep->getPushUtils();
        $this->curl = $dep->createCURL();
    }

    public function post($notification)
    {
        $ext_id = $this->sendToServer($notification);
        $this->utils->setStatus($notification['id'], 'dispatched');
        $this->utils->setExtId($notification['id'], $ext_id);
    }

    protected function sendToServer($notification)
    {
        $content = array(
            "en" => $notification['body'],
        );

        $fields = array(
            'app_id' => $_ENV['oneSignalAppID'],
            'include_external_user_ids' => json_decode($notification['recepients'], true),
            'channel_for_external_user_ids' => 'push',
            'data' => array("foo" => "bar"),
            'web_push_topic' => '1',
            'android_group' => '1',
            'contents' => $content
        );
        $this->curl->setHeader('Authorization', 'Basic ' . $this->apiKey);
        $this->curl->setHeader('Content-Type', 'application/json');
        $this->curl->post('https://onesignal.com/api/v1/notifications', json_encode($fields));
        $respo = $this->curl->getResponse();
        if ($respo->errors) {
            throw new DispatchException(json_encode($respo->errors));
        }
        return $respo->id;
    }
}