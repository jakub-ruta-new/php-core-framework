<?php

namespace JR\CORE\helpers\gateway\push\ldap;

use Curl\Curl;
use JR\CORE\helpers\dependencies\DependenciContainer;
use JR\CORE\helpers\gateway\interface\push\PushInterface;
use JR\CORE\helpers\gateway\push\DispatchException;
use JR\CORE\midleware\push\PushUtils;
use JR\CORE\midlewares\apiTokens\ApiTokens;

class LDAP implements PushInterface
{

    /**
     * @var DependenciContainer
     */
    protected $dep;
    protected $apiKey = null;
    /**
     * @var PushUtils
     */
    protected $utils;
    /**
     * @var Curl
     */
    protected $curl;

    public function __construct(DependenciContainer $dep)
    {
        $this->dep = $dep;
        $apiUtils = new ApiTokens($dep->getDB());
        $this->apiKey = $apiUtils->getToken('send', 'ldap');
        $this->utils = $dep->getPushUtils();
        $this->curl = $dep->createCURL();
    }

    public function post($notification)
    {
        $ext_id = $this->sendToServer($notification);
        $this->utils->setStatus($notification['id'], 'dispatched');
        $this->utils->setExtId($notification['id'], $ext_id);
    }

    protected function sendToServer($notification)
    {
        $recipients = json_decode($notification['recepients'], true);
        $newRecipients = [];
        foreach ($recipients as $user) {
            $newRecipients[] = $this->dep->getUser()::getPortalIdByInternalId($user, $this->dep->getDB());
        }
        $notification['recepients'] = json_encode($newRecipients);
        $data['notification'] = $notification;
        $data['token'] = $this->apiKey['token'];
        $data['app_code'] = $_ENV['APP_CODE'];
        $this->curl->post($_ENV['LDAP_URL'] . 'api/v1/ldap/push_notifications/post', $data);
        $respo = $this->curl->getResponse();
        bdump($respo);
        if ($respo->msg_error || $respo->msg_warning) {
            throw new DispatchException(json_encode([$respo->errors, $respo->msg_warning]));
        }
        return $respo->id;
    }
}