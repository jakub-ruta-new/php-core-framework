<?php

namespace JR\CORE\helpers\gateway\interface\push;
interface PushInterface
{
    public function post($notification);
}