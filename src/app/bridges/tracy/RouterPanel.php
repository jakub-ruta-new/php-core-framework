<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of UserPanel
 *
 * @author jakub
 */
class RouterPanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \JR\CORE\router\Main
     */
    protected $router;

    /**
     * Path from request
     * @var string
     */
    protected $path;

    public function __construct(\JR\CORE\router\Main $router, $path) {
        $this->router = $router;
        $this->path = $path;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->preparePanel(), "bridge.tracyBar.Router.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView(array("route" => $this->path), "bridge.tracyBar.Router.tab");
        return $view->renderToString();
    }

    public function preparePanel() {
        return array("Router" => $this->router);
    }

}
