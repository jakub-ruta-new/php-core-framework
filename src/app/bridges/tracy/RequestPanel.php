<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of RequestPanel
 *
 * @author jakub
 */
class RequestPanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    public function __construct(\JR\CORE\request\request $request) {
        $this->request = $request;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->preparePanel(), "bridge.tracyBar.Request.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView(array(), "bridge.tracyBar.Request.tab");
        return $view->renderToString();
    }

    public function preparePanel() {
        return array("Request" => $this->request);
    }

}
