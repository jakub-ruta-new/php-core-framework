<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use Curl\Curl;
use JR\CORE\views\RawView;

/**
 * Description of CURLPanel
 *
 * @author jakub
 */
class CURLPanel implements \Tracy\IBarPanel
{

    /**
     *
     * @var array<Curl>
     */
    protected array $curl = [];

    public function __construct(Curl $curl)
    {
        $this->curl[] = $curl;
    }

    public function getPanel(): ?string
    {
        $view = new RawView($this->preparePanel(), "bridge.tracyBar.Curl.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string
    {
        $count = count($this->curl);
        $view = new RawView(array("count" => $count), "bridge.tracyBar.Curl.tab");
        return $view->renderToString();
    }

    public function preparePanel()
    {
        $panel = [];
        foreach ($this->curl as $item) {
            $panel[$item->url] = $item;
        }
        return $panel;
    }

    public function addCurl(Curl $curl)
    {
        $this->curl[] = $curl;
    }

}
