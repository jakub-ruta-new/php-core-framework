<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views\RawView;

/**
 * Description of OptionsPanel
 *
 * @author jakub
 */
class OptionsPanel implements \Tracy\IBarPanel
{

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $options;

    public function __construct(\JR\CORE\config\Config $options)
    {
        $this->options = $options;
    }

    public function getPanel(): ?string
    {
        $view = new RawView($this->preparePanel(), "bridge.tracyBar.Options.panel");
        return $view->renderToString();
    }

    public function preparePanel()
    {
        return array(
            'Version' => $this->options->getVersion(),
            'Core version' => $this->options->getCoreVersion(),
            'Core build' => \Composer\InstalledVersions::getRawData(),
            'Feature Flags' => $this->options->getFeatureFlags(),
            'Config' => $this->options->getConfigs(),
        );
    }

    public function getTab(): ?string
    {
        $view = new RawView(array(), "bridge.tracyBar.Options.tab");
        return $view->renderToString();
    }

}
