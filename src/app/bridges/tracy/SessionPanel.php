<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of SessionPanel
 *
 * @author jakub
 */
class SessionPanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \JR\CORE\helpers\session\SessionsUtils
     */
    protected $session;

    public function __construct(\JR\CORE\helpers\session\SessionsUtils $session) {
        $this->session = $session;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->preparePanel(), "bridge.tracyBar.Session.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView(array(), "bridge.tracyBar.Session.tab");
        return $view->renderToString();
    }

    public function preparePanel() {
        return array("Session" => $this->session);
    }

}
