<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of UserPanel
 *
 * @author jakub
 */
class UserPanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $user;

    public function __construct(\JR\CORE\midleware\users\User $user) {
        $this->user = $user;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->preparePanel(), "bridge.tracyBar.User.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView(array("name" => $this->user->getLoginName()), "bridge.tracyBar.User.tab");
        return $view->renderToString();
    }

    public function preparePanel() {
        return array("User" => $this->user);
    }

}
