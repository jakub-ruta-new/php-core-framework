<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

use JR\CORE\views;

/**
 * Description of WebRouter
 *
 * @author jakub
 */
class WebRouter extends Router
{

    /**
     *
     * @var \JR\CORE\controler\WebControler
     */
    protected $next;

    //put your code here
    public function execute()
    {
        $this->register();
        try {
            $this->next = $this->select($this->request->getParsedPath());
            $this->next = new $this->next($this->dep);
        } catch (NotExistsException $exc) {
            $this->redirectToError("404", "Path not exists");
            return;
        }
        try {
            $this->next->execute($this->dep);
        } catch (\JR\CORE\helpers\form\CSRFViolation $exc) {
            $this->redirectToError("403", "CSRF token is not valid");
            return;
        }
        $this->next->prepareSharedData();
        $latte = new views\LatteView($this->next->data, $this->next->view);
        $latte->render();
    }

    private function register()
    {
        $this->addRoute("error", \JR\CORE\error\ErrorControler::class);
        $this->addRoute("login", \JR\CORE\controlers\login\LoginControler::class);
        $this->addRoute("password-restore", \JR\CORE\controlers\password\PasswordRestoreControler::class);
        $this->addRoute("user", \JR\CORE\controlers\user\UserControler::class);
        $this->addRoute("admin", \JR\CORE\controlers\admin\AdminControler::class);
        $this->addRoute("tech-admin", \JR\CORE\controlers\tech_admin\AdminControler::class);
        $this->addRoute("debug/php_info", \JR\CORE\controlers\debug\PhpInfo::class);
        $this->addRoute("debug/rights", \JR\CORE\controlers\debug\Rights::class);
        $this->addRoute("debug/config", \JR\CORE\controlers\debug\Options::class);
        $this->registerAppSecificsRoutes();
        $keys = array_map('strlen', array_keys($this->routes));
        array_multisort($keys, SORT_ASC, $this->routes);
    }

    private function registerAppSecificsRoutes()
    {
        if (file_exists("app/config/routes.json")) {
            $data = json_decode(file_get_contents("app/config/routes.json"), true);
            foreach ($data as $key => $value) {
                $this->addRoute($key, $value);
            }
        }
        bdump(class_exists(\JR\CORE\app\router\WebRouter::class));
        if (class_exists(\JR\CORE\app\router\WebRouter::class)) {
            new \JR\CORE\app\router\WebRouter($this);
        }
    }

}
