<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

use JR\auth\midleware\apps\WrongToken;
use JR\CORE\midlewares\apiTokens\WrongTokenType;

/**
 * Description of ApiRouter
 *
 * @author jakub
 */
abstract class ApiRouter extends \JR\CORE\router\Router
{

    /**
     *
     * @var string path prefix
     */
    protected $prefix_path;

    /**
     *
     * @var \JR\CORE\controler\APIControler
     */
    protected $next;

    //put your code here
    public function execute()
    {
        $this->register();
        $this->code = 200;
        try {
            $this->next = $this->select($this->request->getParsedPath($this->prefix_path));
            $this->next = new $this->next($this->dep);
            $this->next->execute($this->dep);
        } catch (NotExistsException $exc) {
            $this->code = 404;
            $this->msg_error = 'Path not exists - ' . json_encode($this->request->getParsedPath($this->prefix_path));
        } catch (\JR\CORE\helpers\form\CSRFViolation $exc) {
            $this->code = 403;
            $this->msg_error = 'CSRF token is not valid';
        } catch (LoggedInPolicy $exc) {
            $this->code = 401;
            $this->msg_error = 'You have to be logged in to view requested page';
        } catch (AdminLevelPolicy $exc) {
            $this->code = 403;
            $this->msg_error = "You do not have required admin level (" . $exc->getMessage() . ") to do this action!";
        } catch (\JR\CORE\midleware\rights\RightViolationException $exc) {
            $this->code = 403;
            $this->msg_error = "You do not have required right (" . $exc->getMessage() . ") to do this action!";
        } catch (RedirectToError $exc) {
            $this->code = $exc->getHTTPCode();
            $this->msg_error = $exc->getMessage();
        } catch (WrongTokenType $ex) {
            $this->code = 403;
            $this->msg_error = 'Token is not valid';
        } catch (\JR\CORE\midleware\tokens\WrongTokenType $ex) {
            $this->code = 403;
            $this->msg_error = 'Token is not valid';
        } catch (WrongToken $ex) {
            $this->code = 403;
            $this->msg_error = 'Token is not valid w';
        }
        if ($this->code === 200) {
            $view = new \JR\CORE\views\JSONView($this->next->msg_success,
                $this->next->msg_error, $this->next->msg_warning,
                $this->next->data, $this->code);
        } else {
            $view = new \JR\CORE\views\JSONView($this->next->msg_success,
                $this->msg_error, $this->msg_warning,
                $this->data, $this->code);
        }
        $view->send();
    }

    abstract protected function register();

    public function redirect($path)
    {
        parent::redirect($this->prefix_path . $path);
    }

}
