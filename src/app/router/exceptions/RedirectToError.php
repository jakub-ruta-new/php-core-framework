<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

/**
 * Description of RedirectToError
 *
 * @author jakub
 */
class RedirectToError extends \Exception {

    protected $httpCode;

    public function __construct($httpCode, $message = "You do not have rights to do this action!") {
        $this->httpCode = $httpCode;
        $this->message = $message;
    }

    public function getHTTPCode() {
        return $this->httpCode;
    }

}
