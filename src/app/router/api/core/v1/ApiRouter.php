<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\routes\api\core\v1;

/**
 * Description of ApiRouter
 *
 * @author jakub
 */
class ApiRouter extends \JR\CORE\router\ApiRouter {

    /**
     *
     * @var string path prefix
     */
    protected $prefix_path = "api/core/v1/";

    protected function register() {
        $this->addRoute("error", \JR\CORE\controlers\api\core\v1\ErrorControler::class);
        $this->addRoute("set/user", \JR\CORE\controlers\api\core\v1\set\UserControler::class);
        $this->addRoute("set/notifications", \JR\CORE\controlers\api\core\v1\set\NotificationsControler::class);
        $this->addRoute("delete/notification", \JR\CORE\controlers\api\core\v1\delete\NotificationControler::class);
        $this->addRoute("set/webhooks", \JR\CORE\controlers\api\core\v1\set\WebHooksControler::class);
        $this->addRoute("set/language", \JR\CORE\controlers\api\core\v1\set\Language::class);
        $this->addRoute("get/ldap", \JR\CORE\controlers\api\core\v1\get\LDAP::class);
        $keys = array_map('strlen', array_keys($this->routes));
        array_multisort($keys, SORT_ASC, $this->routes);
    }

}
