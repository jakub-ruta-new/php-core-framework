<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

use JR\CORE\views\LatteView;

/**
 * Description of LDAPRouter
 *
 * @author jakub
 */
class LDAPRouter extends Router {

    //put your code here
    public function execute() {
        $this->register();
        try {
            $this->next = $this->select($this->request->getParsedPath("gateway/ldap/"));
            $this->next = new $this->next($this->dep);
        } catch (NotExistsException $exc) {
            $this->redirectToError("404", "Path not exists");
            return;
        }
        try {
            $this->next->execute($this->dep);
        } catch (\JR\CORE\helpers\form\CSRFViolation $exc) {
            $this->redirectToError("403", "CSRF token is not valid");
            return;
        }
        $this->next->prepareSharedData();
        $latte = new LatteView($this->next->data, $this->next->view);
        $latte->render();
    }

    private function register() {
        $this->addRoute("adopt", \JR\CORE\gateway\ldap\AdoptControler::class);

        $keys = array_map('strlen', array_keys($this->routes));
        array_multisort($keys, SORT_ASC, $this->routes);
    }

}
