<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

use JR\CORE\helpers;

/**
 * Description of Router
 *
 * @author jakub
 */
abstract class Router {

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var array For storearray of routes
     */
    protected $routes = array();

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    /**
     *
     * @var \JR\CORE\router\Main
     */
    protected $router;

    function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        $this->request = $dep->getRequest();
        $this->db = $dep->getDB();
        $this->dep = $dep;
    }

    abstract function execute();

    function redirectToError($code, $msg) {
        $this->dep->getSession()->setErrorMessages($msg);
        header("Location: " . $this->request->getPathPrefix() . "/error/$code");
        header("Connection: close");
        return;
    }

    public function redirect($path) {
        if (str_contains($path, "http")) {
            header("Location: " . $path);
        } else {
            header("Location: " . $this->request->getPathPrefix() . "/$path");
        }
        header("Connection: close");
        return;
    }

    /**
     * Function for register router in router
     * @param array|string $path
     * @param class $class
     */
    public function addRoute($path, $class) {
        if (is_array($path)) {
            foreach ($path as $value) {
                $this->registerRoute($value, $class);
            }
        } else {
            $this->registerRoute($path, $class);
        }
    }

    protected function registerRoute($path, $class) {
        $parsedPath = array_reverse(explode("/", $path));
        $new = helpers\ArrayUtils::createTreeFromArrayAndAddToEnd($parsedPath, $class);
        $this->routes = array_merge_recursive($this->routes, $new);
    }

    /**
     * @param string Parsed path from $this->request->getParsedPath()
     * @return string className
     * @throws NotExistsException
     */
    protected function select($parsedPath) {
        $path = array_reverse($parsedPath);
        $routes = $this->routes;
        if ($path[0] == "") {
            $value = $this->routes[''];
            return $value;
        }
        $i = 0;
        $not_exists = false;
        do {
            $one_path = array_pop($path);
            if (isset($routes[$one_path]))
                $routes = $routes[$one_path];
            else
                $not_exists = true;
            if (is_string($routes)) {
                return $routes;
            }
            if ($path == null && !$not_exists) {
                if (!isset($routes[0])) {
                    throw new NotExistsException("route " . $this->request->getPath() . " doesn´t exists");
                }
                return $routes[0];
            }
            if ($not_exists)
                break;
            $i++;
        } while ($i < 10 && count($path) > 0);
        throw new NotExistsException("route " . $this->request->getPath() . " doesn´t exists");
    }

}
