<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\error;

/**
 * Description of ErrorControler
 *
 * @author jakub
 */
class ErrorControler extends \JR\CORE\controler\WebControler {

    public $view = "core.errors.error";

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[1]) {
            case 401:
                header("HTTP/1.0 401 Unauthorized");
                break;
            case 409:
                header("HTTP/1.0 409 Conflict");
                break;
            case 403:
                header("HTTP/1.0 403 Forbidden");
                break;
            default:
                throw new \JR\CORE\router\RedirectError("dashBoard");
            case 404:
                header("HTTP/1.0 404 Not Found");
                break;
            case 500:
                header("HTTP/1.0 500 Internal server error");
                break;
            case 501:
                header("HTTP/1.0 501 Not Implemented");
                break;
        }
        $this->data['error_messages'] = $this->dep->getSession()->getErrorMessages();
        $this->data['last_pages'] = $this->dep->getSession()->getLastPages();
    }

}
