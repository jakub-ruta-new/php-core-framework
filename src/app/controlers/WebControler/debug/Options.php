<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\debug;

use JR\CORE\helpers\form\FormFactory;
use JR\CORE\router\AdminLevelPolicy;

/**
 * Description of PHP-info
 *
 * @author jakub
 */
class Options extends \JR\CORE\controler\WebControler
{

    public $view = 'core.debug.options';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'tech_admin';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 2;

    public function execute()
    {
        if ($this->request->getParsedPath()[2] === 'do-update') {
            if ($this->dep->getUser()->getRole() < 10) {
                throw new AdminLevelPolicy();
            }
            $data = shell_exec($this->dep->getConfig()->get('DEPLOY_SCRIPT') . ' 2>&1');
            echo('<pre>');
            dump($data);
            $this->dep->getActionLog()->addLog('debug', 'do-update', json_encode('data'), 9);
            exit();
        }
        $this->data['options'] = $this->dep->getConfig()->getOptions()->getAll()[0];
        $form = $this->dep->getConfig()->createForm($this->data['options']);
        if ($form->isSend()) {
            $this->proccedForm($form);
        }
        $this->data['form'] = $form;
    }

    public function proccedForm(FormFactory $form)
    {
        foreach ($form->getValues() as $key => $value) {
            $key = explode('::', $key);
            if ($value === 'null') {
                $value = null;
            }
            $this->dep->getConfig()->setOption($key[0], $key[1], $value);
        }
        $this->makeLogAndLeave('Options has been updated', 'options', 'edit', json_encode($form->getChanged()), 'debug/config', 3);
    }

}
