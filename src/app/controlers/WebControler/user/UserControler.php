<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\user;

/**
 * Description of UserControler
 *
 * @author jakub
 */
class UserControler extends \JR\CORE\controler\WebControler {

    //put your code here
    public function execute() {

        switch ($this->request->getParsedPath()[1]) {
            case 'password-restore':
                break;
            case 'register':
                if (($this->dep->getConfig()->get('can_register')) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Registrations are not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new Register($this->dep);
                break;
            case 'profile':
                $new = new Profile($this->dep);
                break;
            case 'logout':
                $new = new LogOut($this->dep);
                break;
            default:
                throw new \JR\CORE\router\RedirectError("user/profile");
                break;
        }
        $new->execute();
        $this->data = $new->data;
        $this->view = $new->view;
    }

}
