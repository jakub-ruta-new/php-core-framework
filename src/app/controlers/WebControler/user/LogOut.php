<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\user;

/**
 * Description of LogOut
 *
 * @author jakub
 */
class LogOut extends \JR\CORE\controler\WebControler {

    //put your code here
    public function execute() {
        $this->dep->getActionLog()->addLog('user', 'logout');
        $this->dep->getUserFactory()->logoutUser();
        throw new \JR\CORE\router\RedirectError("login");
    }

}
