<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

use JR\CORE\midleware\users\UserNotExist;

/**
 * Description of LDAPLogin
 *
 * @author jakub
 */
class GoogleLogin extends \JR\CORE\controler\WebControler
{

    //put your code here
    public function execute()
    {
        if ($this->request->getPost()['credential']) {
            $client = new \Google_Client(['client_id' => $_ENV['google_login_key']]);  // Specify the CLIENT_ID of the app that accesses the backend
            $payload = $client->verifyIdToken($this->request->getPost()['credential']);
            if ($payload) {
                $id = $payload['sub'];
                try {
                    $user = $this->dep->getUserFactory()->getUserByGoogleId($id);
                    //TODO odchyntou výjimky
                    $this->dep->getActionLog()->addLog('login', 'google_login',
                        json_encode(array("id" => $id
                        )), 6);
                    $this->dep->getUserFactory()->loginUser($user->getInternalId(), true, 'google');
                    $this->makeLogAndLeave('Looged in via google', 'User', 'google_login', null, 'user/profile');
                } catch (UserNotExist $ex) {
                    $this->makeLogAndLeave('Account under this google id do not exists', 'User', 'login-google-fail', json_encode(array('message' => 'id not exists', "id" => $id
                    )), 'login/google', 6, 'danger');
                } catch (\JR\CORE\helpers\factories\users\AccountBannedException $ex) {
                    $this->dep->getSession()->setMessages("This account is banned!", "danger");
                } catch (\JR\CORE\helpers\factories\users\AccountDissabledException $ex) {
                    $this->dep->getSession()->setMessages("This account has been disabled!", "danger");
                }
            } else {
                $this->makeLogAndLeave('Unable to communicate with google', 'User', 'login-google-fail', null, 'login/google', 1, 'danger');
            }
        }
    }

    public function ldapLogin()
    {
        try {
            $ldapHelper = new \JR\CORE\helpers\gateway\ldap\LDAP($this->dep);
            $ldapHelper->tryLogin($this->request->getParsedPath()[2]);
            $this->dep->getSession()->setMessages("User has been logged in!", "success");
            $redirect = $this->request->getGet();
            $this->dep->getActionLog()->addLog("login", "ldap_login");
            if (isset($redirect['redirect'])) {
                throw new \JR\CORE\router\RedirectError($redirect['redirect']);
            }
            throw new \JR\CORE\router\RedirectError("dashBoard");
        } catch (\JR\CORE\helpers\gateway\ldap\LDAPNotResponnding $ex) {
            $this->dep->getSession()->setMessages("LDAP error", "danger");
        } catch (\JR\CORE\midleware\DuplicateRowException $ex) {
            $this->dep->getSession()->setMessages("User with this name already exist - contact your head to resolve this issue", "danger");
            throw new \JR\CORE\router\RedirectToError(500, "Account can be created only via admin");
        }
    }

    private function makeCallToGoogle(string $credential)
    {
    }

}
