<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class LoginControler extends \JR\CORE\controler\WebControler
{

    //put your code here
    public function execute()
    {
        $new = null;
        $localLoginEnabled = $this->dep->getConfig()->get('local_login');
        $ldapLoginEnabled = $this->dep->getConfig()->get('ldap_login');
        $googleLoginEnabled = $this->dep->getConfig()->get('google_login');
        switch ($this->request->getParsedPath()[1]) {
            case 'local':
                if (($localLoginEnabled) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login locally is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new LocalLogin($this->dep);
                break;
            case 'hidden_local':
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new LocalLogin($this->dep);
                $new->data['hiden_login'] = true;
                break;
            case 'ldap':
                if (($ldapLoginEnabled) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login via LDAP is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new LDAPLogin($this->dep);
                if (!$localLoginEnabled && !$googleLoginEnabled) {
                    $new->redirectToLdap();
                }
                break;
            case 'google':
                if (($googleLoginEnabled) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login via google is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new GoogleLogin($this->dep);
                break;
            case 'admin':
                $new = new AdminLogin($this->dep);
                break;
            case '2fa':
                throw new \JR\CORE\router\RedirectToError(403, "Not supported!");
                $new = new TwoFactorLogin($this->dep);
                break;
            default:
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                break;
        }
        if ($new != null) {
            $new->execute();
            $this->data = $new->data;
        }
        $this->view = ($new->view ?: 'core.login.login');
        $this->data['hide_sidebar'] = true;
    }

}
