<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

/**
 * Description of LocalLogin
 *
 * @author jakub
 */
class TwoFactorLogin extends \JR\CORE\controler\WebControler
{
    public $view = 'core.login.two-factor';

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep)
    {
        parent::__construct($dep);
    }

//put your code here
    public function execute()
    {
        $this->data['twoFactorForm'] = $form = $this->createForm();
        if ($form->isSend()) {
            $this->proccedLogin($form->getValues());
        } else {

        }
    }

    public function createForm()
    {
        $form = new \JR\CORE\helpers\form\FormFactory("twofa-login", $this->dep->getSession()->getCSRFToken());
        $form->createTextInput("name", "Username")
            ->placeholder("Username")
            ->setPrepend('<i class="fas fa-user"></i>')
            ->required()
            ->readonly();
        $form->createTextInput("code", "2FA code")
            ->placeholder("2FA Code")
            ->setType("text")
            ->setPrepend('<i class="fas fa-lock"></i>')
            ->required();
        $form->createButton('login', 'Continue');
        return $form;
    }

    public function proccedLogin($values)
    {
        $count = $this->dep->getActionLog()->getCountOf('login', 'local_login',
            60 * 60 * 2, $this->request->getUserIP());
        if ($count > $_ENV['max_login_atemps']) {
            throw new \JR\CORE\router\RedirectToError(401,
                "Count of login atemps from this ip exceed limit");
        }
        $this->dep->getActionLog()->addLog('login', 'local_login',
            json_encode(array("user_name" => $values['username']
            )), 6);
        try {
            $this->dep->getUserFactory()->tryLocalLogin(
                strtolower($values['username']),
                $values['password'], $values['remember_me']);
            $this->dep->getSession()->setMessages("User has been logged in!", "success");
            $redirect = $this->request->getGet();
            $this->dep->getActionLog()->addLog("login", "local_login");
            if (isset($redirect['redirect'])) {
                throw new \JR\CORE\router\RedirectError($redirect['redirect']);
            }
            throw new \JR\CORE\router\RedirectError("dashBoard");
        } catch (\JR\CORE\helpers\factories\users\LocalLoginException $ex) {
            $this->dep->getSession()->setMessages("User name and/or password is wrong", "danger");
        } catch (\JR\CORE\helpers\factories\users\AccountBannedException $ex) {
            $this->dep->getSession()->setMessages("This account is banned!", "danger");
        } catch (\JR\CORE\helpers\factories\users\AccountDissabledException $ex) {
            $this->dep->getSession()->setMessages("This account has been disabled!", "danger");
        }
    }

}
