<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

/**
 * Description of LDAPLogin
 *
 * @author jakub
 */
class LDAPLogin extends \JR\CORE\controler\WebControler {

    //put your code here
    public function execute() {
        if (isset($this->request->getParsedPath()[2])) {
            $this->ldapLogin();
        } else {
            $this->redirectToLdap();
        }
    }

    public function redirectToLdap() {
        $redirect = $this->request->getGet()['redirect'];

        throw new \JR\CORE\router\RedirectError($_ENV['LDAP_URL'] . "auth/" . $_ENV['APP_CODE'] .
                        "/auto/?redirect=" . $redirect);
    }

    public function ldapLogin() {
        try {
            $ldapHelper = new \JR\CORE\helpers\gateway\ldap\LDAP($this->dep);
            $ldapHelper->tryLogin($this->request->getParsedPath()[2]);
            $this->dep->getSession()->setMessages("User has been logged in!", "success");
            $redirect = $this->request->getGet();
            $this->dep->getActionLog()->addLog("login", "ldap_login");
            if (isset($redirect['redirect'])) {
                throw new \JR\CORE\router\RedirectError($redirect['redirect']);
            }
            throw new \JR\CORE\router\RedirectError("dashBoard");
        } catch (\JR\CORE\helpers\gateway\ldap\LDAPNotResponnding $ex) {
            $this->dep->getSession()->setMessages("LDAP error", "danger");
        } catch (\JR\CORE\midleware\DuplicateRowException $ex) {
            $this->dep->getSession()->setMessages("User with this name already exist - contact your head to resolve this issue", "danger");
            throw new \JR\CORE\router\RedirectToError(500, "Account can be created only via admin");
        }
    }

}
