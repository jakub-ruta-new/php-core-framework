<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\users;

/**
 * Description of Users
 *
 * @author jakub
 */
class Users extends \JR\CORE\controler\WebControler {

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'users';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        if (is_numeric($this->request->getParsedPath()[2])) {
            $new = new OneUsers($this->dep, $this->request->getParsedPath()[2]);
        } else {
            switch ($this->request->getParsedPath()[2]) {
                case 'new':
                    $new = new NewUsers($this->dep);
                    break;
                default:
                    $new = new AllUsers($this->dep);
                    break;
            }
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
