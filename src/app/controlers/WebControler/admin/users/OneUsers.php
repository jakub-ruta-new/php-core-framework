<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\users;

use JR\CORE\midleware\users\User;

/**
 * Description of OneUsers
 *
 * @author jakub
 */
class OneUsers extends \JR\CORE\controler\WebControler
{

    public $view = 'core.admin.users.user';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'users';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 5;

    /**
     *
     * @var bool set up during start up used for update user
     */
    protected $canIEditUser = false;

    public function execute()
    {
        $this->dep->getActionLog()->addLog('Admin-users', 'view', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        $user = new \JR\CORE\midleware\users\User($this->request->getParsedPath()[2], $this->db);
        $this->canISeeThisUser($user);
        $this->router($user);
        $this->prepareData($user);
        $this->data['can_edit'] = $this->canIEditUser;
    }

    protected function canISeeThisUser(\JR\CORE\midleware\users\User $user)
    {
        $me = $this->dep->getUser();
        if ($me->getRole() > 9 && $me->rights->can("rights", 'overRide', false) == 1) {
            $this->canIEditUser = true;
            return true;
        }
        if ($me->rights->can("users", 'view', false) &&
            $me->rights->can("users", 'all', false) && $me->getRole() >= 6) {
            if ($me->rights->can("users", 'edit', false) && $me->getRole() >= $user->getRole()) {
                $this->canIEditUser = true;
            }
            return true;
        } else if ($me->rights->can("users", 'view', false) &&
            $me->getRole() >= 4 && $user->departments->isIn($me->departments->getMyDepartementsIds(4))) {
            if ($me->rights->can("users", 'edit', false) && $me->getRole() >= $user->getRole()) {
                $this->canIEditUser = true;
            }
            return true;
        }
        throw new \JR\CORE\router\RedirectToError(401, "You do not have rights to view this user");
    }

    protected function router(\JR\CORE\midleware\users\User $user)
    {
        $request = $this->request->getParsedPath("admin/users/");
        switch ($request[1]) {
            case 'kill-session':
                $this->killSession($request[2], $user->userSession);
                break;
            case 'logout-everywhere':
                $this->logoutEveryWhere($user);
                break;
            case 'change-password':
                $this->changePassword($user);
                break;
            case 'kill-token':
                $this->killToken($request[2], $user->getTokens());
                break;
            case 'reset-password':
                $this->resetPassword($user);
                break;
            case 'disable':
                $this->disableUser($user);
                break;
            case 'enable':
                $this->enableUser($user);
                break;
            case 'change-rights':
                $this->changeRights($user);
                break;
            case 'ban':
                $this->ban($user);
                break;
            case 'un-ban':
                $this->unban($user);
                break;
            case 'change-role':
                $this->changeRole($user);
                break;
            case 'change-groups':
                $this->changeGroups($user);
                break;
            case 'change-department-role':
                $this->changeDepartmentRole($user);
                break;
            case 'add-department':
                $this->addDepartment($user);
                break;
            case 'remove-department':
                $this->removeDepartment($user);
                break;
            case 'repair-rights':
                $this->repairRights($user);
                break;
            case 'remove-avatar':
                $this->removeAvatar($user);
                break;
            case 'sendTestMail':
                $this->sendTestMail($user);
                break;
            case 'sendTestPush':
                $this->sendTestPush($user);
                break;
            case 'google-remove':
                $this->googleRemove($user);
                break;
        }
    }

    public function killSession($id, \JR\CORE\midleware\sessions\Session $userSession)
    {
        $this->canIEditUser();
        if ($userSession->InvalideSession($id)) {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User session has been killed'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'kill-session', json_encode(array("user_id" => $this->request->getParsedPath()[2], "sess_id" => $id)));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    protected function canIEditUser()
    {
        if ($this->canIEditUser) {
            return true;
        }
        throw new \JR\CORE\router\RedirectToError(401, "You do not have rights to edit this user");
    }

    public function changePassword(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $post = $this->request->getPost();
        if ($post['password'] == $post['password-again']) {
            if (strlen($post['password']) > 4) {
                $user->password->change($post['password']);
                $this->dep->getSession()
                    ->setMessages($this->dep->
                    getTranslations()
                        ->str('Password has been changed'), 'success');
                $this->dep->getActionLog()->addLog('Admin-users', 'change-password', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
            } else {
                $this->dep->getSession()
                    ->setMessages($this->dep->
                    getTranslations()
                        ->str('Passwords is too week!'), 'danger');
            }
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Passwords do not match!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function killToken($token_id, \JR\CORE\midleware\tokens\Tokens $tokens)
    {
        $this->canIEditUser();
        if ($tokens->InvalideToken($token_id)) {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User token has been killed'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'kill-token', json_encode(array("user_id" => $this->request->getParsedPath()[2], "token_id" => $token_id)));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function resetPassword(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $tokens = $user->getTokens();
        $token = $tokens->createOne($user->getInternalId(), 60 * 60 * 24, "password-reset");
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('Password reset token has been created'), 'success');
        $this->dep->getSession()
            ->setMessages($this->request->getBasePath() .
                "/user/password-restore/" . $user->getInternalId() . "/" .
                $token, null, 'Token: ', $this->request->getBasePath() .
                "/password-restore/" . $user->getInternalId() . "/" .
                $token);
        $this->dep->getActionLog()->addLog('Admin-users', 'reset-password', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function disableUser(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        if ($user->disable(true)) {
            $user->userSession->invalidateAll();
            $user->getTokens()->invalidateAll();
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User has been disabled'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'disable', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function enableUser(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        if ($this->dep->getUser()->rights->can("users", "edit") &&
            $user->enable(true)) {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User has been enabled'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'enable', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function changeRights(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $this->filterRightsAndProccedd($user, $this->request->getPost());
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    /**
     *
     * @param \JR\CORE\midleware\users\User $user
     * @param array $data data from POST
     */
    public function filterRightsAndProccedd(\JR\CORE\midleware\users\User $user, $data)
    {
        $current_rights = $user->rights->getUserRights($this->dep->getUser()->getRole());
        $set = array();
        $unset = array();
        $my_role = $this->dep->getUser()->getRole();
        foreach ($current_rights as $group) {
            foreach ($group as $right) {
                $in_post = (isset($data['right::' . $right['right_name'] .
                        "::" . $right['right_action']]) && $data['right::' . $right['right_name'] .
                    "::" . $right['right_action']]) ? 1 : 0;
                unset($right['right_description']);
                unset($right['visible_from']);
                unset($right['user_internal_id']);
                if ($in_post && !$right['right_value'] && $right['right_granteable_by'] <= $my_role) {
                    $set[] = $right;
                } else if (!$in_post && $right['right_value'] && $right['right_granteable_by'] <= $my_role) {
                    $unset[] = $right;
                }
            }
        }
        $this->proceedRightsChange($set, $unset, $user);
    }

    public function proceedRightsChange($set, $unset, \JR\CORE\midleware\users\User $user)
    {
        $user->rights->setRights($set, $unset);
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('User rights has been set'), 'success');
        $this->dep->getActionLog()->addLog('Admin-users', 'update-rights',
            json_encode(array("user_id" => $this->request->getParsedPath()[2],
                "data" => array("set" => $set, "unset" => $unset))), 6);
    }

    public function ban(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        if ($user->ban(true)) {
            $user->userSession->invalidateAll();
            $user->getTokens()->invalidateAll();
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User has been Banned'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'ban', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function unban(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        if ($user->unBan(true)) {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User has been UNbanned'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'un-ban', json_encode(array("user_id" => $this->request->getParsedPath()[2])));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function changeRole(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $role = $this->request->getPost()['main_role'];
        if ($role > $this->dep->getUser()->getRole()) {
            $role = $this->dep->getUser()->getRole() - 1;
        }
        if ($user->setRole($role)) {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('User Role updated'), 'success');
            $this->dep->getActionLog()->addLog('Admin-users', 'change-main-role', json_encode(array("user_id" => $this->request->getParsedPath()[2], "role" => $role)));
        } else {
            $this->dep->getSession()
                ->setMessages($this->dep->
                getTranslations()
                    ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function changeGroups(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $groups_con = $user->rights->getRightsGroups();
        $groups = $this->request->getPost("right_group::");
        $ids = array();
        foreach ($groups as $key => $value) {
            $ids[] = str_replace("right_group::", "", $key);
        }
        $rights = $groups_con->getRightsINGroupsByIdsUnderRole($ids, $this->dep->getUser()->getRole());
        if ($_POST['type'] == "add") {
            $this->proceedRightsChange($rights, null, $user);
        } else if ($_POST['type'] == "remove") {
            $this->proceedRightsChange(null, $rights, $user);
        } else {
            throw new \Exception("Wrong request type");
        }
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function changeDepartmentRole(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $dep_id = $this->request->getPost()['department_id'];
        $new_role = $this->request->getPost()['main_role'];
        $my_role = $this->dep->getUser()->departments->getMyRoleInDepartment($dep_id);
        $old_role = $user->departments->getMyRoleInDepartment($dep_id);
        $new_role = $this->filterRole($new_role, $my_role, $old_role);
        $user->departments->updateRole($dep_id, $new_role);
        $this->dep->getActionLog()->addLog('Admin-users', 'change-department-role', json_encode(array("user_id" => $this->request->getParsedPath()[2], "dep_id" => $dep_id, "old_role" => $old_role, "new_role" => $new_role)));
        $user->setRoleAuto();
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('User level in department has been changed!'), 'success');
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function filterRole($new_role, $my_role, $old_role)
    {
        if ($this->dep->getUser()->getRole() > 8) {
            return $new_role;
        }
        if ($my_role == null || $my_role < 4) {
            throw new \JR\CORE\router\RedirectToError(401, "You can not manipulate with this department!");
        }
        if ($old_role > $my_role) {
            throw new \JR\CORE\router\RedirectToError(401, "You can not edit higher role than yours!");
        }
        if ($new_role > 9) {
            $this->dep->getSession()->setMessages($this->dep->getTranslations()->str("Role has been adjusted. You can not give role you requested!"), "secondary");
            $new_role = 9;
        }
        if ($new_role < 0) {
            $this->dep->getSession()->setMessages($this->dep->getTranslations()->str("Role has been adjusted. You can not give role you requested!"), "secondary");
            $new_role = 0;
        }
        if ($new_role > $my_role) {
            $this->dep->getSession()->setMessages($this->dep->getTranslations()->str("Role has been adjusted. You can not give higher role than you have!"), "secondary");
            $new_role = $my_role - 1;
        }
        return $new_role;
    }

    /**
     * Function to add department to user
     * @param \JR\CORE\midleware\users\User $user
     * @throws \JR\CORE\router\RedirectError
     */
    public function addDepartment(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $dep_id = $this->request->getPost()['department_id'];
        $new_role = $this->request->getPost()['role'];
        $my_role = $this->dep->getUser()->departments->getMyRoleInDepartment($dep_id);
        $new_role = $this->filterRole($new_role, $my_role, 0);
        $user->departments->addDepartment($dep_id, $new_role);
        $this->dep->getActionLog()->addLog('Admin-users', 'add-department', json_encode(array("user_id" => $this->request->getParsedPath()[2], "dep_id" => $dep_id, "new_role" => $new_role)));
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('User has been added to department!'), 'success');
        $user->setRoleAuto();
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function removeDepartment(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $dep_id = $this->request->getPost()['department_id'];
        $my_role = $this->dep->getUser()->departments->getMyRoleInDepartment($dep_id);
        $user_role = $user->departments->getMyRoleInDepartment($dep_id);
        if ($my_role < $user_role && $this->dep->getUser()->getRole() < 9) {
            throw new \JR\CORE\router\RedirectToError(401, "You can not edit higher role than yours!");
        }
        $user->departments->removeDepartment($dep_id);
        $this->dep->getActionLog()->addLog('Admin-users', 'remove-department', json_encode(array("user_id" => $this->request->getParsedPath()[2], "dep_id" => $dep_id)));
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('User has been removed from department!'), 'success');
        $user->setRoleAuto();
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function repairRights(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $user->rights->repairRights();
        $this->dep->getActionLog()->addLog('Admin-users', 'repair-rights', json_encode(array("user_id" => $this->request->getParsedPath()[2])), 7);
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('Rights repair!'), 'success');
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function removeAvatar(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $user->removeAvatar();
        $this->dep->getActionLog()->addLog('Admin-users', 'remove-avatar', json_encode(array("user_id" => $this->request->getParsedPath()[2])), 7);
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('Avatar removed!'), 'success');
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function sendTestMail(\JR\CORE\midleware\users\User $user)
    {
        $email = $this->dep->getEmailTool();
        $id = $email->addToQueue("This is test mail", "This mail is send by admin to test if mail works"
            , [$user->getMail()], false);
        $this->makeLogAndLeave("Mail added to queue", "Admin-users", "send-mail",
            ["mail_id", $id], "admin/users/" . $this->request->getParsedPath()[2]);
    }

    protected function sendTestPush(User $user)
    {
        $id = $this->dep->getPushUtils()->create("Test", "Test from admin", [$user->getInternalId()], $this->dep->getUser()->getInternalId());
        $this->makeLogAndLeave("Push added to queue", "Admin-users", "send-push",
            ["push_id", $id], "admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function googleRemove(\JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $user->registerGoogle(null);
        $this->dep->getActionLog()->addLog('Admin-users', 'remove-google', json_encode(array("user_id" => $this->request->getParsedPath()[2])), 7);
        $this->dep->getSession()
            ->setMessages($this->dep->
            getTranslations()
                ->str('Google connection removed!'), 'success');
        throw new \JR\CORE\router\RedirectError("admin/users/" . $this->request->getParsedPath()[2]);
    }

    public function prepareData(\JR\CORE\midleware\users\User $user)
    {
        $csrf = $this->dep->getSession()->getCSRFToken();
        $this->data['admin_levels'] = $user->admin_levels;
        $this->data['user'] = $user->get();
        $this->data['user_form'] = $this->generateUserForm($user->get(), $csrf, $user);
        if ($user->personalInfo) {
            $this->data['user_data_form'] = $this->generatePersonalData($user->personalInfo, $csrf, $user);
        }
        $this->data['sessions'] = $user->userSession->getAll(null);
        $this->data['rights'] = $user->rights->getUserRights($this->dep->getUser()->getRole());
        $this->data['departments'] = $user->departments->get();
        $this->data['all_departments'] = $user->departments->getAllDepartments();
        $this->getHeads($user);
        $this->data['all_rights_groups'] = $user->rights->getRightsGroups()->getGroups(11);
        $this->data['tokens'] = $user->getTokens()->getAll();
        $this->data['push_devices'] = $this->dep->getPushUtils()->getUserDevices($user->getInternalId());
    }

    public function generateUserForm($user, $csrf, $user_class)
    {
        $form = new \JR\CORE\helpers\form\FormFactory("user", $csrf);
        $form->createTextInput('login_name', 'Login name')
            ->setPrepend('<i class="fas fa-user" aria-hidden="true"></i>')
            ->value($user['login_name'])
            ->setDisabledFunc();
        $form->createTextInput('nickname', "Nickname")
            ->value($user['nickname'])
            ->setPrepend('<i class="fas fa-id-badge"></i>');
        $email = $form->createTextInput('mail', "E-mail")
            ->required()
            ->value($user['mail'])
            ->setPrepend('<i class="fas fa-at" aria-hidden="true"></i>');
        if (!$this->canIchangeUserEmail($user_class)) {
            $email->setDisabledFunc();
        }
        $form->createSelect('locale', 'Prefered language')
            ->required()
            ->setOptions($this->dep->getTranslations()->getLangs())
            ->value($user['locale'])
            ->setPrepend('<i class="fas fa-language"></i>');
        $form->createTextInput('origin', 'Origin')
            ->setPrepend('<i class="fas fa-boxes"></i>')
            ->value($user['origin'])
            ->setDisabledFunc();
        $form->createTextInput('registered', 'Registered')
            ->setPrepend('<i class="far fa-clock"></i>')
            ->value($user['registered'])
            ->setDisabledFunc();
        $form->createTextInput('avatar', "Avatar URL")
            ->value($user['avatar'])
            ->setPrepend('<i class="far fa-user-circle"></i>');
        $form->createButton("user", 'Save')
            ->Class('form-control btn btn-sm btn-success');
        if ($user['origin'] != 'local') {
            $form->setDisabled();
        } elseif ($form->isSend()) {
            $this->ProccedUserForm($form, $user_class);
        }
        return $form;
    }

    public function canIchangeUserEmail(\JR\CORE\midleware\users\User $user)
    {
        if ($this->dep->getUser()->getRole() > 9) {
            return true;
        }
        if ($user->getRole() > $this->dep->getUser()->getRole()) {
            return false;
        }
        $u_dep = $user->departments->getMyDepartementsIds(-1);
        $a_dep = $user->departments->getMyDepartementsIds(4);
        foreach ($u_dep as $value) {
            if (!in_array($value, $a_dep)) {
                return false;
            }
        }
        return true;
    }

    public function ProccedUserForm(\JR\CORE\helpers\form\FormFactory $form, \JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $values = $form->getValues();
        if (isset($values['mail']) && !$this->canIchangeUserEmail($user)) {
            unset($values['mail']);
        }
        $user->updateUser($values);
        $this->dep->getSession()->setMessages
        ($this->dep->getTranslations()
            ->str("User profile has been updated!"), "success");
        $this->dep->getActionLog()->addLog('Admin-users', 'change-user-profile', json_encode(array("user_id" => $this->request->getParsedPath()[2], "data" => $form->getValues())));
        throw new \JR\CORE\router\RedirectError("admin/users/" . $user->getInternalId());
    }

    public function generatePersonalData(\JR\CORE\midleware\users\PersonalData $data, $csrf, \JR\CORE\midleware\users\User $user)
    {
        $form = new \JR\CORE\helpers\form\FormFactory("user_data", $csrf);
        $form->createTextInput('first_name', "Name")
            ->setPrepend('<i class="fas fa-id-card"></i>');
        $form->createTextInput('last_name', 'Last name')
            ->setPrepend('<i class="fas fa-id-card"></i>');
        $form->createTextInput('birth_data', 'Birth date')
            ->setType("date")
            ->setPrepend('<i class="fas fa-birthday-cake"></i>');
        $form->createTextInput('adress', 'Adress')
            ->setPrepend('<i class="fas fa-map-marked-alt"></i>');
        $form->createTextInput('city', 'City')
            ->setPrepend('<i class="fas fa-city"></i>');
        $form->createTextInput("zip", "Post code")
            ->setPrepend('<i class="fas fa-map-marker-alt"></i>');
        $form->createTextInput('country', 'Country')
            ->setPrepend('<i class="fas fa-globe-europe"></i>');
        $form->createTextInput('company', 'Company')
            ->setPrepend('<i class="fas fa-building"></i>');
        $form->createTextInput('phone', 'Phone')
            ->setPrepend('<i class="fas fa-phone"></i>');
        $form->createTextInput('mobile', 'Mobile')
            ->setPrepend('<i class="fas fa-mobile"></i>');
        $form->createSelect('gender', 'Gender')
            ->setOptions(array('none' => 'None', 'male' => 'Male', 'famele' => 'Female'))
            ->setPrepend('<i class="fas fa-venus-mars"></i>');
        $form->createButton("user-data", 'Save')
            ->Class('form-control btn btn-sm btn-success');
        $form->vulues($data->getMe());
        if ($form->isSend()) {
            $this->ProccedUserDataForm($form, $user);
        }
        return $form;
    }

    public function ProccedUserDataForm(\JR\CORE\helpers\form\FormFactory $form, \JR\CORE\midleware\users\User $user)
    {
        $this->canIEditUser();
        $values = $form->getValues();
        $user->personalInfo->updateMe($values);
        $this->dep->getSession()->setMessages
        ($this->dep->getTranslations()
            ->str("User profile personal data has been updated!"), "success");
        $this->dep->getActionLog()->addLog('Admin-users', 'change-personal-data', json_encode(array("user_id" => $this->request->getParsedPath()[2], "data" => $form->getValues())));
        throw new \JR\CORE\router\RedirectError("admin/users/" . $user->getInternalId());
    }

    public function getHeads(\JR\CORE\midleware\users\User $user)
    {
        $this->data['heads'] = $user->departments->getMyAdmins();
        $this->data['heads']['right'] = $user->rights->getUsersWithRight('users', 'all');
    }

}
