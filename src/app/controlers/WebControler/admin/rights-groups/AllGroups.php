<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\rights;

/**
 * Description of AllGroups
 *
 * @author jakub
 */
class AllGroups extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.rights.rights_groups';

    public function execute() {
        $this->data['all_rights_groups'] = $this->dep->getUser()->rights->getRightsGroups()->getGroupsByDepartements($this->dep->getUser()->departments->getMyDepartementsIds(3));
        $this->data['new_group_form'] = $this->createNewForm();
    }

    public function createNewForm() {
        $form = new \JR\CORE\helpers\form\FormFactory('new_group', $this->dep->getSession()->getCSRFToken());
        $data = $this->dep->getUser()->departments->getMyDepartments(5);
        $form->createTextInput("name", "Name")->required();
        $form->createSelect('department_id', 'Department')
                ->setOptions($data
                        , 'dep_id', 'name');
        $form->setForm_class('form-inline-md');
        $form->createButton('create', 'Create');
        $form->setAction('admin/rights-groups/new');
        return $form;
    }

}
