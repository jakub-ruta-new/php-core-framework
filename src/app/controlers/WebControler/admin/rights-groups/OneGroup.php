<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\rights;

use JR\CORE\router\RedirectToError;

/**
 * Description of AllGroups
 *
 * @author jakub
 */
class OneGroup extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.rights.one_group';

    public function execute() {
        $this->dep->getActionLog()->addLog('Admin-rights-groups', 'view', json_encode(array("group_id" => $this->request->getParsedPath()[2])));
        $groups = $this->dep->getUser()->rights->getRightsGroups();
        $id = $this->request->getParsedPath()[2];
        $this->data['rights_group'] = $group = $groups->getOne($id);
        $this->data['group_form'] = $this->createEditForm($groups, $id, $group);
        $this->data['rights'] = $groups->getRightsForGroup($id);
        $this->router($groups, $id, $group);
    }

    protected function router(\JR\CORE\midleware\rights\RightsGroups $groups, $id, $group) {
        $request = $this->request->getParsedPath("admin/rights-groups/");
        switch ($request[1]) {
            case 'edit-rights':
                $this->editRights($groups, $id, $group);
                break;
            case 'delete':
                $this->delete($groups, $id, $group);
                break;
        }
    }

    public function createEditForm(\JR\CORE\midleware\rights\RightsGroups $groups, $id, $group) {
        $form = new \JR\CORE\helpers\form\FormFactory('edit-group',
                $this->dep->getSession()->getCSRFToken());
        $form->createTextInput('name', 'Name')->setDisabledFunc();
        $form->createNumberInput('dep_name', 'Department')
                ->setDisabledFunc();
        $form->createSelect('role', 'Role')
                ->setOptions(($this->dep->getUser()->admin_levels));
        $form->createTextArea('note', 'Note');
        $form->createCheckBox("system", "System");
        $form->createButton('edit', 'Save');
        $form->vulues($this->data['rights_group']);
        if ($form->isSend()) {
            if ($this->dep->getUser()->getRole() < 9 && $group['system'] == 1) {
                throw new RedirectToError(401, "You can not edit system group");
            }
            $groups->update($form->getValues(), $id);
            $this->makeLogAndLeave("Group has been altered", "Rights-groups",
                    "edit", ["group_id" => $id, "data" => $form->getValues()],
                    "/admin/rights-groups/" . $id);
        }
        return $form;
    }

    public function editRights(\JR\CORE\midleware\rights\RightsGroups $groups, $id, $group) {
        if ($this->dep->getUser()->getRole() < 9 && $group['system'] == 1) {
            throw new RedirectToError(401, "You can not edit system group");
        }
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        $data = $this->request->getPost("right::");
        $groups->setRights($id, $data);
        $this->makeLogAndLeave("Rights in group has been altered",
                "Rights-groups", "rights-edit",
                ['group_id' => $id, "rights" => $data],
                "admin/rights-groups/" . $id);
    }

    public function delete(\JR\CORE\midleware\rights\RightsGroups $groups, $id, $group) {
        $this->dep->getSession()->checkCSRF($_POST['csrf']);
        if ($this->dep->getUser()->getRole() < 9 && $group['system'] == 1) {
            throw new RedirectToError(401, "You can not edit system group");
        }
        $groups->delete($id);
        $this->makeLogAndLeave("Group deleted",
                "Rights-groups", "delete",
                ['group_id' => $id],
                "admin/rights-groups");
    }

}
