<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\rights;

/**
 * Description of NewGroup
 *
 * @author jakub
 */
class NewGroup extends \JR\CORE\controler\WebControler {

    //put your code here
    public function execute() {
        $all = new AllGroups($this->dep);
        $form = $all->createNewForm();
        if ($form->isSend()) {
            $this->createNew($form->getValues());
        }
        throw new \JR\CORE\router\RedirectError("admin/rights-groups/");
    }

    public function createNew($data) {
        $groups = $this->dep->getUser()->rights->getRightsGroups();
        $id = $groups->create($data);
        $this->makeLogAndLeave("New group has been created",
                "Rights-groups", "new", $data, "admin/rights-groups/" . $id);
    }

}
