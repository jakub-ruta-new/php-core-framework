<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin;

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class AdminControler extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.menu';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[1]) {
            case 'users':
                $new = new users\Users($this->dep);
                break;
            case 'rights-groups':
                $new = new rights\RightsGroups($this->dep);
                break;
            case 'departments':
                $new = new departments\Departments($this->dep);
                break;
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
