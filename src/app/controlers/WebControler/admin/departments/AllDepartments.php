<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\departments;

/**
 * Description of AllDepartments
 *
 * @author jakub
 */
class AllDepartments extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.departments.all';

    public function execute() {
        $this->data['departments'] = $this->getVisible();
        $this->data['new_form'] = $this->crateNewForm();
    }

    public function getVisible() {
        if ($this->dep->getUser()->rights->can("departments", "all", false)) {
            return $this->addMembersToDepartments(
                            $this->dep->getUser()->departments->getAllDepartments());
        }
        return $this->addMembersToDepartments(
                        $this->dep->getUser()->departments->getMyDepartments(4));
    }

    public function crateNewForm() {
        $form = new \JR\CORE\helpers\form\FormFactory('new-dep',
                $this->dep->getSession()->getCSRFToken());
        $form->createTextInput('name', 'Name')->required();
        $form->createTextInput('code', 'Code')->required();
        $form->setForm_class('form-inline-md');
        $form->createButton('create', 'Create');
        $form->setAction('admin/departments');
        if ($form->isSend())
            $this->createNew($form->getValues());
        return $form;
    }

    public function createNew($values) {
        $this->dep->getUser()->rights->can("departments", "all", true);
        $id = $this->dep->getUser()->departments->createDepartment($values);
        $this->dep->getUser()->departments->addDepartment($id, 6);
        $this->makeLogAndLeave("Department created",
                "Departments", "crate", ['dep_id' => $id],
                "admin/departments/" . $id);
    }

    public function addMembersToDepartments($deps) {
        $dep_utils = $this->dep->getUser()->departments;
        foreach ($deps as $key => $value) {
            $deps[$key]['members'] = $dep_utils->getUsersIn($value['id']);
        }
        return $deps;
    }

}
