<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\departments;

/**
 * Description of AllDepartments
 *
 * @author jakub
 */
class OneDepartment extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.departments.one';

    /**
     *
     * @var \JR\CORE\midleware\departments\DeparmentsInvitations
     */
    protected $invitations;

    public function execute() {
        $departments = $this->dep->getUser()->departments;
        $this->invitations = new \JR\CORE\midleware\departments\DeparmentsInvitations($this->db);
        $id = $this->request->getParsedPath()[2];
        $this->data['department'] = $departments->getOneDepartment($id);
        $this->data['users_in'] = $departments->getUsersIn($id);
        $this->data['dep_form'] = $this->createEditForm($departments, $id);
        $this->data['invite_form'] = $this->createInviteForm($departments, $id);
        $this->data['crate_user_form'] = $this->createUserForm($departments, $id);
        $this->data['invitations'] = $this->invitations->getByDepartment($id);
        $this->setPermissions($departments, $id);
        $this->router($id, $departments);
    }

    public function createEditForm($deps, $id) {
        $form = new \JR\CORE\helpers\form\FormFactory('edit-dep',
                $this->dep->getSession()->getCSRFToken());
        $form->createTextInput('id', 'Id')->setDisabledFunc();
        $form->createTextInput('name', 'Name')->setDisabledFunc();
        $form->createTextInput('code', 'Code')->setDisabledFunc();
        $form->vulues($this->data['department']);
        return $form;
    }

    public function createInviteForm(\JR\CORE\midleware\departments\Departments $departments, $id) {
        $form = new \JR\CORE\helpers\form\FormFactory('invite-to-dep',
                $this->dep->getSession()->getCSRFToken());
        $form->createTextInput('name', 'Login name')->required();
        $form->createSelect("role", "Role")
                ->required()
                ->setOptions($this->dep->getUser()->admin_levels)
                ->setHidden([-10, -1])
                ->setDisabled([10, 9])
                ->value(1);
        if ($form->isSend()) {
            $this->inviteUser($departments, $id, $form->getValues());
        }
        return $form;
    }

    public function setPermissions(\JR\CORE\midleware\departments\Departments $departments, $id) {
        $this->data['can_manage_members'] = $this->dep->
                        getUser()->rights->can("departments", "all", false) ||
                $departments->getMyRoleInDepartment($id) > 4 ? true : false;
    }

    public function createUserForm(\JR\CORE\midleware\departments\Departments $departments, $id) {
        $form = new \JR\CORE\helpers\form\FormFactory('new-user-dep',
                $this->dep->getSession()->getCSRFToken());
        $form->createTextInput("username", "Name")
                ->placeholder("Username")
                ->setPrepend('<i class="fas fa-user"></i>')
                ->required();
        $form->createTextInput("mail", "Email")
                ->placeholder("Email")
                ->setType('email')
                ->setPrepend('<i class="fas fa-at"></i>')
                ->required();
        if ($form->isSend()) {
            $this->createUser($id, $form->getValues());
        }
        return $form;
    }

    /**
     *
     * @param \JR\CORE\midleware\departments\Departments $departments
     * @param int $id  id of department
     * @param array $data unfiltered data from form
     */
    public function inviteUser(\JR\CORE\midleware\departments\Departments$departments, $id, $data) {
        $my_role = $departments->getMyRoleInDepartment($id);
        if (!$my_role > 4 && !$this->dep->getUser()->rights->can("departments", "all")) {
            throw new \JR\CORE\router\RedirectToError(401, "You are not head of this department!");
        }
        $this->dep->getUser()->rights->can("departments", "edit");
        $role = ($data['role'] > $my_role ? $my_role - 1 : $data['role']);
        try {
            $user = $this->dep->getUserFactory()->getUserByLoginName($data['name']);
            if ($user->departments->isIn([$id])) {
                throw new \JR\CORE\midleware\departments\AlreadyInDepartment();
            }
            $this->invitations->inviteUser($id, $this->dep->getUser()->getInternalId(),
                    $user->getInternalId(), $role);
            $this->dep->getNotifications()->createDepInv($user->getInternalId(),
                    $this->dep->getUser()->getInternalId());
        } catch (\JR\CORE\midleware\users\UserNotExist $ex) {
            $this->makeLogAndLeave("User not exists!", "Departments", "try-invite",
                    ['user_name' => $data['name'], "role" => $data['role']],
                    "admin/departments/" . $id, 5, "danger");
        } catch (\JR\CORE\midleware\departments\AlreadyInDepartment $ex) {
            $this->makeLogAndLeave("User is already in this department!", "Departments", "try-invite",
                    ['user_name' => $data['name'], "role" => $data['role'], "USER already in"],
                    "admin/departments/" . $id, 5, "danger");
        } catch (\JR\CORE\midleware\departments\UserAlreadyHaveInvitation $ex) {
            $this->makeLogAndLeave("User has pending invitation!", "Departments", "try-invite",
                    ['user_name' => $data['name'], "role" => $data['role'], "USER have open invitation"],
                    "admin/departments/" . $id, 5, "danger");
        }
        $this->makeLogAndLeave("User has been invited", "Departments", "invite",
                ['user_name' => $data['name'], "role" => $data['role']],
                "admin/departments/" . $id);
    }

    public function router($id, \JR\CORE\midleware\departments\Departments $departments) {
        $request = $this->request->getParsedPath("admin/departments/");

        switch ($request[1]) {
            case 'withdraw-invitation':
                $this->withdrawInvitation($id, $departments, $request[2]);
                break;
        }
    }

    public function withdrawInvitation($id, \JR\CORE\midleware\departments\Departments $departments, $inv_id) {
        $my_role = $departments->getMyRoleInDepartment($id);
        if (!$my_role > 4 || !$this->dep->getUser()->rights->can("departments", "all")) {
            throw new \JR\CORE\router\RedirectToError(401, "You are not head of this department!");
        }
        $this->invitations->setStatus($inv_id, "withdrawn");
        $this->makeLogAndLeave("invitation withdrawn", "Departments", "invite-withdrawn",
                ["invitation_id" => $inv_id, "dep_id" => $id],
                "admin/departments/" . $id);
    }

    public function createUser($id, $data) {
        $my_role = $this->dep->getUser()->departments->getMyRoleInDepartment($id);
        if ($my_role < 5 && !$this->dep->getUser()->rights->can("departments", "all", false)) {
            throw new \JR\CORE\router\RedirectToError(401, "You are not head of this department!");
        }
        $this->dep->getUser()->rights->can("departments", "edit");
        $user_id = $this->dep->getUserFactory()->registerUser($data['username'],
                $data['mail'], \JR\CORE\helpers\StringUtils::generate_string(300),
                [$id => 1]);
        $user = new \JR\CORE\midleware\users\User($user_id, $this->dep->getDB());
        $tokens = $user->getTokens();
        $token = $tokens->createOne($user->getInternalId(), 60 * 60 * 24, "password-reset");
        $url = $this->request->getBasePath() . "/password-restore/" . $user_id . "/" . $token;
        $this->dep->getEmailTool()->addToQueue("An account has been crated for you",
                "Dear " . $data['username'] . ", an account has been created for you on website " . $_ENV['APP_NAME'] . ". You can set up you password here: <a href=\"" . $url . "\">" . $url . "</a>", [$data['mail']], true);
        $this->makeLogAndLeave("User has been created and email with passoword has been sent",
                "Departments", "create-user", $data, "admin/users/" . $user_id);
    }

}
