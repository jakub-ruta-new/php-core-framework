<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\departments;

/**
 * Description of Departments
 *
 * @author jakub
 */
class Departments extends \JR\CORE\controler\WebControler {

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 4;

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        if (is_numeric($this->request->getParsedPath()[2])) {
            $new = new OneDepartment($this->dep, $this->request->getParsedPath()[2]);
        } else {
            switch ($this->request->getParsedPath()[2]) {
                case 'new':
                    $new = new NewDepartment($this->dep);
                    break;
                default:
                    $new = new AllDepartments($this->dep);
                    break;
            }
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
