<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin\deamon;

/**
 * Description of Deamon
 *
 * @author jakub
 */
class Deamon extends \JR\CORE\controler\WebControler
{

    public $view = 'core.tech_admin.deamon.overview';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'deamon';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 1;

    /**
     *
     * @var \JR\CORE\midleware\cron\Cron
     */
    protected $utils;

    public function execute()
    {
        $this->utils = new \JR\CORE\midleware\cron\Cron($this->db);
        $this->prepareData();
    }

    public function prepareData()
    {
        $this->data['next_tasks'] = $this->utils->getUpcommingTasks(20);
        $this->data['all_tasks'] = $this->utils->getList();
        $this->data['past_tasks'] = $this->utils->getPastTasks(200);
        $this->data['enable_cron'] = $this->dep->getConfig()->getFeatureFlag('enable_cron');
        $this->data['enable_cron_priority'] = $this->dep->getConfig()->getFeatureFlag('enable_cron_priority');
    }

}
