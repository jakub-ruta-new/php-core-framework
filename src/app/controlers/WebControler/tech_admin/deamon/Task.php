<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin\deamon;

/**
 * Description of Deamon
 *
 * @author jakub
 */
class Task extends \JR\CORE\controler\WebControler {

    public $view = 'core.tech_admin.deamon.task';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'deamon';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 1;

    /**
     *
     * @var \JR\CORE\midleware\cron\Cron
     */
    protected $utils;

    public function execute() {
        $this->utils = new \JR\CORE\midleware\cron\Cron($this->db);
        $id = $this->request->getParsedPath()[3];
        $this->prepareData($id);
        $this->data['form_add_to_queue_now'] = $this->createFormAddToQueueNow($id);
        $this->data['form_delete_upcomming'] = $this->createFormDeleteUpcomming($id);
        $this->data['form_task'] = $this->createFormEditTask($id, $this->data['task']);
    }

    public function prepareData($id) {
        $this->data['next_tasks'] = $this->utils->getUpcommingTasks(20, 0, $id);
        $this->data['task'] = $this->utils->getOneTask($id);
        $this->data['past_tasks'] = $this->utils->getPastTasks(200, 0, $id);
    }

    public function createFormAddToQueueNow($id) {
        $form = new \JR\CORE\helpers\form\FormFactory("add_to_queue_noew", $this->getCSRF());
        $form->createHidden("task_id", $id);
        $form->createButton("add", "Add to queue now")
                ->Class("btn-sm btn btn-primary");
        if ($form->isSend()) {
            $this->addToQueue($form->getValues(), $id);
        }
        return $form;
    }

    public function createFormDeleteUpcomming($id) {
        $form = new \JR\CORE\helpers\form\FormFactory("delete_upcomming", $this->getCSRF());
        $form->createHidden("task_id", $id);
        $form->createButton("remove", "Remove all upcomming from queue")
                ->Class("btn-sm btn btn-danger");
        if ($form->isSend()) {
            $this->removeFromQueue($form->getValues(), $id);
        }
        return $form;
    }

    public function createFormEditTask($id, $values) {
        $form = new \JR\CORE\helpers\form\FormFactory("edit_task", $this->getCSRF());
        $form->createTextInput("name", "Name")->required();
        $form->createTextInput("enabled", "Enabled")->required();
        $form->createTextInput("default_priority", "Default priority")->required();
        $form->createTextInput("auto_repeat", "Repeat")->required();
        $form->createTextInput("name", "Name")->required();
        $form->vulues($values);
        return $form;
    }

    public function addToQueue($form_data, $id) {
        $this->utils->addToQueue($id, $this->dep->getUser()->getInternalId(), date("Y-m-d H:i:s"), 0);
        $this->makeLogAndLeave("Task added to queue with priority 0",
                "CRON", "add_to_queue", ["id" => $id],
                "tech-admin/deamon/task/" . $id);
    }

}
