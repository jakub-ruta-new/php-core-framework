<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin;

/**
 * Description of Translations
 *
 * @author jakub
 */
class Translations extends \JR\CORE\controler\WebControler {

    public $view = 'core.tech_admin.translations';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'translations';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 2;

    public function execute() {
        $this->data['results'] = $this->dep->getTranslations()->loadTranslations()->getStrings();
        ksort($this->data['results']);
        $this->createNewForm();
        $this->repairTranslations();
        if (isset($_POST['tran'])) {
            $this->updateTransaltions();
        }
    }

    public function createNewForm() {
        $form = new \JR\CORE\helpers\form\FormFactory("new_language", $this->dep->getSession()->getCSRFToken());
        $form->createTextInput("code", "Language code")
                ->required();
        $form->createTextInput("name", "Language vissible name");
        $form->createButton("create", "Create");
        $form->setForm_class("form-inline-md");
        $this->data['form'] = $form;
        if ($form->isSend()) {
            $this->createNewLanguage($form->getValues());
        }
    }

    public function updateTransaltions() {
        $this->dep->getUser()->rights->can("translations", "edit");
        $tran_util = $this->dep->getTranslations();
        $new = $this->request->getPost()['tran'];
        foreach ($this->data['results'] as $key => $value) {
            if (isset($new[md5($key)])) {
                $this->data['results'][$key] = $new[md5($key)];
            }
        }
        $tran_util->update($this->data['results']);
        $this->makeLogAndLeave("Translations updated",
                "Translations", "edit", null, "tech-admin/translations");
    }

    public function createNewLanguage($data) {
        $this->dep->getUser()->rights->can("translations", "create");
        $this->dep->getTranslations()->addLanguage($data['code'], $data['name']);
        $this->makeLogAndLeave("Language updated", "Translations", "new",
                $data, "tech-admin/translations");
    }

    public function repairTranslations() {
        $form = new \JR\CORE\helpers\form\FormFactory("repair_tran", $this->dep->getSession()->getCSRFToken());
        $form->createButton("repair", "Repair translations")
                ->Class("btn btn-secondary");
        $this->data['form_repair'] = $form;
        if ($form->isSend()) {
            $this->dep->getTranslations()->repair();
        }
    }

}
