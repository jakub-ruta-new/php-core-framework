<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\set\user;

/**
 * Description of PushDevice
 *
 * @author jakub
 */
class PushDevice extends \JR\CORE\controler\APIControler {

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    public function execute() {
        if (isset($_POST['device_id'])) {
            $pushUtils = new \JR\CORE\midleware\push\PushUtils($this->dep->getDB());
            $pushUtils->addDevice($_POST['device_id'], $this->dep->getUser()->getInternalId());
            $this->msg_success = "Device add to user";
        } else {
            throw new RedirectToError(400, "Missing device id");
        }
    }

}
