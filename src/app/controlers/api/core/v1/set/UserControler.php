<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\set;

use JR\CORE\controlers\api\core\v1\set\user\PushDevice;
use JR\CORE\midleware\users\User;
use JR\CORE\midleware\users\UserNotExist;

/**
 * Description of UserControler
 *
 * @author jakub
 */
class UserControler extends \JR\CORE\controler\APIControler
{

    /**
     *
     * @var \JR\CORE\controler\APIControler
     */
    protected $next = null;

    //put your code here
    public function execute()
    {
        switch ($this->request->getParsedPath()[5]) {
            case 'push_device':
                $this->next = new PushDevice($this->dep);
                break;
            case 'webhooks':
                $this->next = new WebHooks($this->dep);
                break;
            case 'logout':
                $this->logout();
                break;
            case 'enableUser':
                $this->enableUser();
                break;
            case 'disableUser':
                $this->disableUser();
                break;
            default:
                throw new \JR\CORE\router\NotExistsException();
        }
        if (isset($this->next)) {
            $this->next->execute();
            $this->data = $this->next->data;
            $this->msg_error = $this->next->msg_error;
            $this->msg_warning = $this->next->msg_warning;
            $this->msg_success = $this->next->msg_success;
        }
    }

    public function logout()
    {
        $post = $this->request->getPost();
        $api_tokens = new \JR\CORE\midlewares\apiTokens\ApiTokens($this->db);
        $api_tokens->checkToken($post['app_code'], $post['app_token'], 'set');
        $user = new \JR\CORE\midleware\users\User($post['user_id'], $this->db);
        $user->userSession->invalidateAll();
        $this->msg_success = "All sessions deleted";
    }

    protected function enableUser()
    {
        $post = $this->request->getPost();
        $api_tokens = new \JR\CORE\midlewares\apiTokens\ApiTokens($this->db);
        $api_tokens->checkToken($post['app_code'], $post['app_token'], 'set');
        try {
            $user = $this->dep->getUserFactory()->getUserByPortalId($post['user_id']);
            $this->dep->getActionLog()->addLog('User', 'enable', ['via' => 'ldap', 'user_id' => $post['user_id']], 1, 8);
        } catch (UserNotExist $ex) {
            $id = $this->dep->getUserFactory()->registerUser($post['user']['user']['login_name'], $post['user']['user']['mail']);
            $this->dep->getActionLog()->addLog('User', 'register', ['via' => 'ldap', 'user_id' => $post['user_id']], 1, 8);
            $user = new User($id, $this->db);
            $user->setPortalId($post['user_id']);
            $user->setOrigin('ldap');
        }
        $user->departments->updateFromLDAP(
            $post['user']['user']['all_departments']
        );
        $user->departments->updateUserDepartmentsFromLDAP(
            $post['user']['user']['departments']
        );
        $user->setRole($post['user']['user']['role']);
        $user->updateUser($post['user']['user']);
        $user->enable();
        $this->msg_success = "User enabled, data synced";
    }

    protected function disableUser()
    {
        $post = $this->request->getPost();
        $api_tokens = new \JR\CORE\midlewares\apiTokens\ApiTokens($this->db);
        $api_tokens->checkToken($post['app_code'], $post['app_token'], 'set');

        $user = $this->dep->getUserFactory()->getUserByPortalId($post['user_id']);
        $this->dep->getActionLog()->addLog('User', 'disable', ['via' => 'ldap', 'user_id' => $post['user_id']], 1, 8);
        $user->userSession->invalidateAll();
        $user->getTokens()->invalidateAll();
        $user->disable();
        $this->msg_success = "All sessions deleted, All tokens disabled, User disabled";
    }

}
