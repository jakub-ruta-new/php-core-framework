<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\set;

/**
 * Description of Language
 *
 * @author jakub
 */
class Language extends \JR\CORE\controler\APIControler {

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath("api/core/v1/set/language/")[0]) {
            case 'change':
                $this->dep->getSession()->storeInTemp("lang", $this->request->getPost()['lang_code']);
                $this->msg_success[] = "Lang changed";
                break;
            default:
                throw new \JR\CORE\router\NotExistsException();
        }
    }

}
