<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\get;

use JR\CORE\router\RedirectToError;

/**
 * Description of LDAP
 *
 * @author jakub
 */
class LDAP extends \JR\CORE\controler\APIControler
{

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    public function execute()
    {
        switch ($this->request->getParsedPath("api/core/v1/get/ldap/")[0]) {
            case 'apps':
                $this->getLDAPApps();
                break;
            default:
                throw new \JR\CORE\router\NotExistsException();
        }
    }

    public function getLDAPApps()
    {
        $util = new \JR\CORE\helpers\gateway\ldap\LDAP($this->dep);
        try {
            $this->data = $util->getApps();
        } catch (\Exception $ex) {
            throw new RedirectToError(500, $ex->getMessage());
        }
    }

}
