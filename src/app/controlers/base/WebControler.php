<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controler;

use JR\CORE\helpers\form\FormFactory;
use \JR\CORE\midleware\users\User;
use JR\CORE\router\RedirectError;
use JR\CORE\router\RedirectToError;

/**
 * Description of WebControler
 *
 * @author jakub
 */
abstract class WebControler extends Controler
{

    /**
     *
     * @var string view that will be displaied to user
     */
    public $view = null;
    /**
     * @var array of item for menu counters
     */
    protected $base_menu_counter = [];

    public function prepareSharedData()
    {
        $this->data['base']['langs'] = $this->dep->getTranslations()->getLangs();
        $this->data['base']['lang'] = $this->dep->getTranslations()->getSelectedLang();
        $this->data['base']['notifications'] = $this->dep->getNotifications()->getForUser($this->dep->getUser()->getInternalId());
        $this->data['base']['messages'] = $this->dep->getSession()->getMessages();
        $this->data['base']['user'] = $this->dep->getUser();
        $this->data['admin_levels'] = $this->data['base']['user']->admin_levels;
        $this->data['base']['admin'] = $this->dep->getAdmin();
        $this->data['base']['request'] = $this->request;
        $this->data['csrf'] = $this->dep->getSession()->getCSRFToken();
        $this->data['config'] = $this->config;
        $this->data['menu'] = new \JR\CORE\helpers\menu\Menu($this->request, $this->dep->getUser(), $this->dep, $this->base_menu_counter);
        $this->data['assets'] = new \JR\CORE\helpers\Assets($this->request->getBasePath(), $this->config->getVersion());
        $this->data['level_form'] = $this->createLevelForm($this->data['admin_levels'], $this->data['base']['user']);
    }

    protected function createLevelForm(array $adminLevels, User $user)
    {
        if ($user->getMainRole() < 4) {
            return null;
        }
        $form = new FormFactory('active_user_level', $this->getCSRF());
        $form->setForm_class('form form-inline');
        $rolesVissible = [];
        for ($i = 0; $i <= $user->getMainRole(); $i++) {
            $rolesVissible[$i] = $adminLevels[$i];
        }
        $use2FA = ($user->get2FASettings() === 'administrative_only' || $this->dep->getConfig()->getFeatureFlag('2fa_administrative_access_required') === '1');
        $form->createSelect('admin_user_active_level', '<strong>Current role: </strong>')
            ->setOptions($adminLevels)
            ->setOnChange(($use2FA ? 'App.tryChangeRole(this, ' . $user->getRole() . ')' : 'this.form.submit()'))
            ->Class('form-control mx-10')
            ->setVissible($rolesVissible)
            ->setRenderGroupStart()
            ->setRenderGroupEnd()
            ->value($user->getRole());
        if ($use2FA) {
            $form->createTextInput('two-fa-code', '<strong>2FA code: </strong>')
                ->setRenderGroupStart()
                ->setRenderGroupEnd()
                ->required()
                ->Class('form-control mx-10');
            $form->createButton('send', 'ChangeRole');
        }
        if ($form->isSend()) {
            $level = $form->getValue('admin_user_active_level');
            if ($user->getMainRole() < $level) {
                $level = $user->getMainRole();
            }
            if ($use2FA && $level > 3 && $user->getRole() < $level) {
                if (!$user->password->verify2FA($form->getValue('two-fa-code'))) {
                    $this->dep->getActionLog()->addLog('access-violation', 'change-active-role', ['role' => $level], 7);
                    throw new RedirectToError(403, '2 FA validation not pass!');
                }
            }
            $user->userSession->setLevel($level, $user->currentSessionId);
            $user->setRoleSession($level);
            $this->dep->getActionLog()->addLog('user', 'change-active-role', ['role' => $level]);
            throw new RedirectError($this->request->getPath());
        }
        return $form;
    }

    protected function getCSRF()
    {
        return $this->dep->getSession()->getCSRFToken();
    }

    protected function addCounter(string $category, string $item, int $count = 1, string $color = 'secondary')
    {
        $this->base_menu_counter[$category][$item] = ['count' => $count, 'color' => $color];
    }

    protected function makeLogAndLeave($vissibleText, $logTool, $logAction, $logData, $redirectPath, $log_view = 1, $color = "success")
    {
        if ($vissibleText) {
            $this->dep->getSession()->setMessages
            ($this->dep->getTranslations()
                ->str($vissibleText), $color);
        }
        $this->dep->getActionLog()->addLog($logTool, $logAction,
            json_encode($logData));
        throw new \JR\CORE\router\RedirectError($redirectPath);
    }

    protected function setMessage($message, $color = "success", $head = null, $url = null)
    {
        $this->dep->getSession()->setMessages
        ($this->dep->getTranslations()
            ->str($message), $color, $head, $url);
    }

}
