<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron\tasks\app;

/**
 * Description of RenewTokensTask
 *
 * @author jakub
 */
class RenewTokensTask extends \JR\CORE\controlers\cron\tasks\CronTask {

    public function renewTokens() {
        $api_tokens = new \JR\CORE\midlewares\apiTokens\ApiTokens($this->dep->getDB());
        $for_renew = $api_tokens->getForRenew();
        $msg = [];
        foreach ($for_renew as $u) {
            $api_tokens->renew($u['type'], $u['app_code'], $this->dep->createCURL());
            $msg[] = $u['app_code'] . " - " . $u['type'];
        }
        $this->message = "Renewed tokens - " . implode(", \n ", $msg);
    }

}
