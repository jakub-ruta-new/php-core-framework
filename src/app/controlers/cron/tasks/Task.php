<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron\tasks;

/**
 * Description of TaskDispatch
 *
 * @author jakub
 */
class Task extends CronTask
{

    /**
     *
     * @var \JR\CORE\midleware\cron\Cron
     */
    protected $utils;

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep)
    {
        parent::__construct($dep);
        $this->utils = new \JR\CORE\midleware\cron\Cron($this->dep->getDB());
    }

    function dispatch()
    {
        bdump(func_get_args());
        $tasks = $this->utils->getList();
        bdump($tasks, "all_tasks");
        $multiData = array();
        $multiData[] = array("task_id" => 1,
            "priority" => 0,
            "due" => date("Y-m-d H:i:s", time() + 60 * 60),
            "owner" => 9,
            "expire" => date("Y-m-d H:i:s", time() + 5184000));
        foreach ($tasks as $task) {
            if ($task['auto_repeat'] == 0)
                continue;
            if ($task['id'] === 1) continue;
            $diference = strtotime($task['auto_repeat']) - time();
            $last = $this->utils->getLastQueued($task['id']);
            $lastTime = isset($last['q_id']) ? strtotime($last['due']) : time();
            if ($lastTime < time())
                $lastTime = time();
            $i = 0;
            while (true) {
                $i++;
                if ($lastTime + ($diference * $i) > (60 * 60 * 48) + time() || $i > 60) {
                    break;
                }
                $multiData[] = array("task_id" => $task['id'],
                    "priority" => $task['default_priority'],
                    "due" => date("Y-m-d H:i:s", $lastTime + ($diference * $i)),
                    "owner" => 9,
                    "expire" => date("Y-m-d H:i:s", $lastTime + ($diference * $i) + $task['default_expiry']));
            }
        }
        if (is_array($multiData)) {
            $this->utils->abortUpccomingTaskDisptach();
            $this->utils->addMulti($multiData);
        }
    }

    function deleteOld()
    {
        $this->message = "Deleted " . $this->utils->deleteOld(60 * 60 * 24 * 30)
            . " tasks until " . date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 30);
    }

}
