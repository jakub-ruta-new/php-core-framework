<?php

namespace JR\CORE\controlers\cron\tasks\push;

use JR\CORE\controlers\cron\tasks\TaskException;
use JR\CORE\helpers\gateway\push\DispatchException;
use JR\CORE\helpers\gateway\push\ldap\LDAP;
use JR\CORE\helpers\gateway\push\Onesignal;
use JR\CORE\midleware\cron\Cron;
use JR\CORE\midleware\push\PushUtils;

class PushTask extends \JR\CORE\controlers\cron\tasks\CronTask
{
    /**
     *
     * @var PushUtils
     */
    protected $utils;
    /**
     * @var Onesignal
     */
    protected $gateway;

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep)
    {
        parent::__construct($dep);
        $this->utils = new PushUtils($this->dep->getDB());
        switch ($dep->getConfig()->getFeatureFlag('push_notifications')) {
            case 'onesignal':
                $this->gateway = new Onesignal($dep);
                break;
            case 'ldap':
                $this->gateway = new LDAP($dep);
                break;
            default:
                throw new TaskException('no remote service selected');

        }

    }

    function dispatch()
    {
        $notifications = $this->utils->getUnsendedPush();
        $count = 0;
        try {
            foreach ($notifications as $notification) {
                $notificationInPro = $notification;
                $this->gateway->post($notification, $this->utils);
                $count++;
            }
        } catch (\Exception $ex) {
            $this->message = 'Dispatched ' . $count . ' notifications to ' . $this->gateway::class . '. One with error ' . $ex->getMessage();
            $this->utils->setStatus($notificationInPro['id'], 'error');
            $cron = new Cron($this->db);
            $cron->addToQueue(Cron::$PUSH_TASK, 5, '0', 1);

            throw new DispatchException($this->message);
        }
        $this->message = 'Dispatched ' . $count . ' notifications to ' . $this->gateway::class;
    }
}