<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron;

/**
 * Description of CronDeamon
 *
 * @author jakub
 */
class CronDeamon
{

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $config;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    /**
     *
     * @var \JR\CORE\midleware\cron\Cron
     */
    protected $utils;

    /**
     * Time of start execution of cron
     * @var time
     */
    protected $time;

    function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep)
    {
        $this->request = $dep->getRequest();
        $this->db = $dep->getDB();
        $this->dep = $dep;
        $this->config = $dep->getConfig();
        $this->utils = new \JR\CORE\midleware\cron\Cron($this->db);
        $this->time = time() + (ini_get('max_execution_time') > 0 ? ini_get('max_execution_time') : 60);
    }

    function execute()
    {
        $options = $this->dep->getConfig();
        if ($options->getFeatureFlag('enable_cron')) {
            $priority = 20;
        } else {
            $priority = 2;
        }
        if ($options->getFeatureFlag('enable_cron_priority')) {
            $tasks = $this->utils->getNextTasks($priority, 200);
            foreach ($tasks as $task) {
                if ($this->IsTimeForAnotherTask()) {
                    $this->executeTask($task);
                } else {
                    return;
                }
            }
        } else {
            echo 'cron disabled!';
            exit();
        }

    }

    protected function IsTimeForAnotherTask()
    {
        if ($this->time - time() < 5) {
            return false;
        }
        return true;
    }

    public function executeTask($task)
    {
        $time = microtime(true);

        try {
            $this->utils->setRunning($task['q_id']);
        } catch (CronNotNewException $ex) {
            return;
        }

        if (strtotime($task['expire']) < time()) {
            $this->utils->setExpired($task['q_id']);
            return;
        }
        $path = explode("::", $task['path'], 3);
        try {
            $cron = new $path[0]($this->dep);
            $function = $path[1];
            $cron->$function(explode("/", $path[2]), $task);
            $this->utils->setDone($task['q_id'], $cron->getMessage(), $time);
        } catch (\Exception $ex) {
            $this->utils->setError($task['q_id'], $ex->getMessage(), $time);
        }
    }

}
