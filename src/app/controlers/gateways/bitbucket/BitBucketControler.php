<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\gateway\bitbucket;

/**
 * Description of BitBucketControler
 *
 * @author jakub
 */
class BitBucketControler extends \JR\CORE\controler\WebControler {

    public function execute() {
        echo('updating');

        bdump(file_get_contents($this->dep->getConfig()->get('DEPLOY_SCRIPT')), $this->dep->getConfig()->get('DEPLOY_SCRIPT'));
        $this->dep->getActionLog()->addLog("DEBUG", 'bitbucket',
                json_encode([
            'data' => file_get_contents('php://input'),
            'cron' => shell_exec($this->dep->getConfig()->get('DEPLOY_SCRIPT'))
        ]));
        echo('updated');

        exit();
    }

}
