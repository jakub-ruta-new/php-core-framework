<?php

namespace JR\CORE\gateway\onesignal;

class OneSignalControler extends \JR\CORE\controler\APIControler
{

    function execute()
    {
        $data = json_decode(file_get_contents('php://input'));
        $util = $this->dep->getPushUtils();
        $util->setLog($data->event, $data->id, $data->userId, $this->request->getUserIP());
    }
}