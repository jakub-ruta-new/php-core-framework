<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\gateway\ldap;

/**
 * Description of AdoptControler
 *
 * @author jakub
 */
class AdoptControler extends \JR\CORE\controler\WebControler
{

    public $view = 'core.gateway.ldap.adopt';

    //put your code here
    public function execute()
    {
        $this->data['form'] = $this->createAdoptForm();
    }

    protected function createAdoptForm()
    {
        $form = new \JR\CORE\helpers\form\FormFactory('app_adopt', $this->getCSRF());
        $form->createTextArea("token", "Token")
            ->required();
        $form->createButton("Submit", "Submit");
        if ($form->isSend()) {
            $this->formSended($form);
        }
        return $form;
    }

    public function formSended(\JR\CORE\helpers\form\FormFactory $form)
    {
        $ldap = new \JR\CORE\helpers\gateway\ldap\LDAP($this->dep);
        $ldap->adoptLDAP(trim($form->getValue('token')));
        $this->makeLogAndLeave("App adopted to LDAP",
            "LDAP", "adopt",
            json_encode(['token' => trim($form->getValue('token'))]),
            "login", 9);
    }

}
