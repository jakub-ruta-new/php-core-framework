<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\app;

/**
 * Description of app
 *
 * @author jakub
 */
class App
{

    /**
     *
     * @var \Dotenv\Dotenv
     */
    protected $dotenv;

    /**
     *
     * @var class JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var class \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    public function setENVFile($path)
    {
        $this->dotenv = \Dotenv\Dotenv::createImmutable($path, 'config.env');
        $this->dotenv->load();
        $this->dotenv->required(
            ['DB_HOST',
                'DB_NAME',
                'DB_USER',
                'DB_PASS',
                'APP_CODE',
                'max_reg_per_ip',
                'max_login_atemps',
            ]);
        $this->dotenv->required('ENVIRONMENT')->allowedValues(['stage', 'dev', 'prod', 'test']);
    }

    public function start()
    {
        if ($this->checkMaitenanceMode()) {
            return;
        }
        $this->prepareDependecies();
        $this->setTracy();
        if ($this->checkIPBan()) {
            return;
        }
        $router = new \JR\CORE\router\Main($this->dep);
        $router->execute();
    }

    /**
     * TODO enable devs login and using app
     * @return boolean true if maitenance file is created;
     */
    public function checkMaitenanceMode()
    {
        if (file_exists("../app/config/maitenance")) {
            header("HTTP/1.0 503 Service Unavailable");
            include '../app/static/maitenance.html';
            return true;
        }
        return false;
    }

    protected function prepareDependecies()
    {
        $this->createRequestObject();
        $this->dep = new \JR\CORE\helpers\dependencies\DependenciContainer($this->db, $this->request);
        if ($this->request->isGetRequest()) {
            $this->dep->getSession()->setLastPage($this->request->getPath());
        }
    }

    protected function createRequestObject()
    {
        $this->request = new \JR\CORE\request\request();
    }

    public function setTracy()
    {
        $user = $this->dep->getUser();
        $admin = $this->dep->getAdmin();
        if ((isset($user) && $user->getRole() > 9) ||
            (isset($admin) && $admin->getRole() > 9)) {
            \Tracy\Debugger::enable(\Tracy\Debugger::DEVELOPMENT);
            \Tracy\Debugger::dispatch();
        }
    }

    public function checkIPBan()
    {
        $ipTools = new \JR\CORE\helpers\ip_tools\IPTools();
        if ($ipTools->checkIPBan($this->dep->getRequest()->getUserIP())) {
            header("HTTP/1.0 403.6 - IP address rejected");
            include '../app/static/ip_ban.html';
            return true;
        }
        return false;
    }

}
