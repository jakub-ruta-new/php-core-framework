<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\views;

/**
 * Description of JSONView
 *
 * @author jakub
 */
class JSONView {

    protected $msg_error = null;
    protected $msg_warning = null;
    protected $msg_success = null;
    protected $data = null;
    protected $code = 200;

    public function __construct($msg_success, $msg_error = null, $msg_warning = null, $data = null, $code = 200) {
        $this->msg_error = $msg_error;
        $this->msg_warning = $msg_warning;
        $this->msg_success = $msg_success;
        $this->data = $data;
        $this->code = $code;
    }

    public function send() {
        $this->sendHeader();
        $data = array();
        if ($this->msg_error != null) {
            $data['msg_error'] = $this->msg_error;
        }
        if ($this->msg_warning != null) {
            $data['msg_warning'] = $this->msg_warning;
        }
        if ($this->msg_success != null) {
            $data['msg_success'] = $this->msg_success;
        }
        if ($this->data != null) {
            $data['data'] = $this->data;
        }
        header('Content-Type: application/json; charset=utf-8');
        echo(json_encode($data));
    }

    protected function sendHeader() {
        switch ($this->code) {
            case 200:
                header("HTTP/1.1 200 OK");
                break;
            case 404:
                header("HTTP/1.0 404 Not Found");
                break;
            case 403:
                header('HTTP/1.0 403 Forbidden');
                break;
            case 401:
                header("HTTP/1.1 401 Unauthorized");
                break;
        }
    }

}
