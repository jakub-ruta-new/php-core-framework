<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\views;

/**
 * Description of RawView
 *
 * @author jakub
 */
class RawView {

    protected $data;
    protected $view;

    public function __construct($data, $view) {
        $this->data = $data;
        $this->view = str_replace(".", "/", $view) . ".php";
    }

    public function renderToString() {
        ob_start();
        $data = $this->data;
        include __DIR__ . "/" . $this->view;
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

}
