<h1>CURL </h1>
<div class="tracy-inner">
    <div class="tracy-inner-container">
        <table>
            <?php foreach ($data as $key => $value): ?>
                <tr>
                    <th>
                        <?php echo ucfirst($key) ?>
                    </th>
                    <td>
                        <?php echo Tracy\Dumper::toHtml($value, [Tracy\Dumper::LIVE => true]) ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
        <a href="https://icons8.com/icons/set/url">CURL icon by Icons8</a>
    </div>
</div>