<span title="Translations">
    <img src="https://img.icons8.com/fluency/48/000000/translation.png"
         style="max-height: 18px"/>
    <span class="tracy-label"><?= $data['lang'] ?></span>
</span>
