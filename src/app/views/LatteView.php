<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\views;

use Latte;
use Tracy\Debugger;

/**
 * Description of View
 *
 * @author jakub
 */
class LatteView
{

    protected $data;
    protected $view;

    /**
     *
     * @var class Latte\Engine
     */
    protected $latte;

    public function __construct($data, $view)
    {

        $this->data = $data;
        $this->view = str_replace(".", "/", $view) . ".latte";
        $this->latte = new \Latte\Engine();
// REQUIRE nette/utils pack
        Debugger::getBar()->addPanel(new \Latte\Bridges\Tracy\LattePanel($this->latte));
        $this->latte->setTempDirectory('../app/temp/views');
        $this->latte->setLoader(new LatteLoader());
        $translator = function ($original, $vars = array()): string {
            return (string)\JR\CORE\helpers\langs\Lang::gI()->str($original, $vars);
        };
        $this->latte->addExtension(new Latte\Essential\TranslatorExtension($translator));
        $this->latte->addFilter('timeAgo', function ($time, $short = false) {
            return \JR\CORE\helpers\StringUtils::time_ago($time, $short);
        });
        $this->latte->addFilter('md5', function ($str) {
            return md5($str);
        });
        $this->latte->addFilter('anonimizeIP', function ($str) {
            if (str_contains($str, '.')) {
                $ip = explode('.', $str);
                $ip[3] = 0;
                return implode('.', $ip);
            } else if (str_contains($str, ':')) {
                $ip = explode(':', $str);
                return $ip[0] . ':' . $ip[1] . ':' . $ip[2] . ':' . $ip[3] . ':0';
                return 'unsupported';
            } else {
                return 'hidden';
            }
        });
    }

    public function render()
    {
        $this->latte->render($this->view, $this->data);
    }

    public function renderToString()
    {
        return $this->latte->renderToString($this->view, $this->data);
    }

}
